package com.always.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

import com.always.model.Caregiver;
import com.always.model.CaregiverDetail;
import com.always.model.OnlineData;
import com.always.model.User;
import com.always.model.UserHistory;
 

public class XMLPullParserHandler {
    List<OnlineData> onlineDataList;
    List<CaregiverDetail> caregiverDetailList;
    
    List <Caregiver> caregiverList;
    
    
    List<User> userList;
    List<UserHistory> userHistoryList;
    
    private OnlineData onlineData;
    private CaregiverDetail caregiverDetail;
    
    private String text;
    
    private User user;
    
    private UserHistory userHistory;
    
    public XMLPullParserHandler() {
    	onlineDataList = new ArrayList<OnlineData>();
    	userHistoryList=new ArrayList<UserHistory>();
    	caregiverDetailList=new ArrayList<CaregiverDetail>();
    	userList=new ArrayList<User>();
    }
 
    public List<OnlineData> getOnlineData() {
        return onlineDataList;
    }
 
    
    public List<OnlineData> parseJSON(String  strJson)
    {
    	  String OutputData = "";
          JSONObject jsonResponse;
                
          try {
                
               /****** Creates a new JSONObject with name/value mappings from the JSON string. ********/
               jsonResponse = new JSONObject(strJson);
                
               /***** Returns the value mapped by name if it exists and is a JSONArray. ***/
               /*******  Returns null otherwise.  *******/
               JSONArray jsonMainNode = jsonResponse.optJSONArray("user");
                
               /*********** Process each JSON Node ************/

               int lengthJsonArr = jsonMainNode.length();  

               for(int i=0; i < lengthJsonArr; i++) 
               {
                   /****** Get Object for each JSON node.***********/
                   JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    
                   /******* Fetch node values **********/
                             int  id = jsonChildNode.optInt("id");
                   String  fullName  = jsonChildNode.optString("fullName").toString();
                   String latitude   = jsonChildNode.optString("latitude").toString();
                   String longitude  = jsonChildNode.optString("longitude").toString();
                   String status=jsonChildNode.optString("status").toString();
                   String location=jsonChildNode.optString("location").toString();
                   String image =jsonChildNode.optString("image").toString();
                   String phone =jsonChildNode.optString("phone").toString();
                   
                   System.out.println("image url"+image);
                   
                   onlineData = new OnlineData();
                   
                   String str=" \"test";
                   System.out.println("fullName="+fullName);
                   System.out.println("status="+status);
                   System.out.println("location="+location);
                		   
                   onlineData.setId(id);
                   onlineData.setName(fullName.toString());
                   onlineData.setLatitude(latitude.toString());
                   onlineData.setLongitude(longitude.toString());
                   onlineData.setStatus(status.toString());
                   onlineData.setLocation(location.toString());
                   onlineData.setImage(image);
                   onlineData.setPhone(phone);
                   
                   onlineDataList.add(onlineData);
                   
                   
                   OutputData += "Node : \n\n     "+ id +" | "+ fullName +" | "
                                                   + latitude +" | "
                                                   + longitude +" | "
                                                   + status +" | "
                                                   + location +" \n\n ";
                   //Log.i("JSON parse", song_name);
              }
                
               /************ Show Output on screen/activity **********/

           //    output.setText( OutputData );
                
           } catch (JSONException e) {
    
               e.printStackTrace();
           }
    
          return onlineDataList;
    
    }
    
    
    
    public List<CaregiverDetail> caregiverDetail(String  strJson)
    {
    	  String OutputData = "";
          JSONObject jsonResponse;
                
          char alphabets[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

          try {
                
               /****** Creates a new JSONObject with name/value mappings from the JSON string. ********/
               jsonResponse = new JSONObject(strJson);
                
               JSONObject mStoryTags = jsonResponse.getJSONObject("user");
              
               if(mStoryTags != null){
            	   
                   Iterator<String> keys = mStoryTags.keys();
                   
                   while(keys.hasNext()){
                	   
                       String id = String.valueOf(keys.next()); // This value represent 19
                       JSONArray mTag = mStoryTags.getJSONArray(id);
                       
                       if(mTag != null){
                    	   
                           for(int i = 0; i < mTag.length();i++){
                        	   
                               JSONObject mElem = mTag.getJSONObject(i);
                               
                               System.out.println("mElemmElemmElem="+mElem);
                               
                               if(mElem != null){
                            	   
                                   int objectId = mElem.getInt("id"); // value: 101961456910
                                   String  fullName = mElem.getString("fullName"); // value: 101961456910
                                   String image = mElem.getString("image"); // value: 101961456910
                                   
                                   System.out.println("ADdCaregiver objectId"+objectId);
                                   System.out.println("ADdCaregiver fullName"+fullName);
                                   System.out.println("ADdCaregiver image"+image);
                                   
                                   caregiverDetail=new CaregiverDetail();
                                   
                                   caregiverDetail.setId(objectId);
                                   caregiverDetail.setFullName(fullName);
                                   caregiverDetail.setImage(image);
                                   
                                   caregiverDetailList.add(caregiverDetail);
                                   // and etc.
                               }
                           }
                       }
                   }
               }
                
           } catch (JSONException e) {
    
               e.printStackTrace();
           }
    
          return caregiverDetailList;
    
    }
    
    
    
    public List<User> parseHistory(String  strJson)
    {
    	  String OutputData = "";
          JSONObject jsonResponse;
                
          
          try {
                
               /****** Creates a new JSONObject with name/value mappings from the JSON string. ********/
               jsonResponse = new JSONObject(strJson);
                
               /***** Returns the value mapped by name if it exists and is a JSONArray. ***/
               /*******  Returns null otherwise.  *******/
               JSONArray jsonMainNode = jsonResponse.optJSONArray("inout");
                
               /*********** Process each JSON Node ************/
               System.out.println("jsonMainNode ==="+jsonMainNode);
               
                int lengthJParams = jsonMainNode.length();  
                
                System.out.println("lengthJParams==="+lengthJParams);
                
              //  int id =jsonResponse.getInt("id");
                String userName = jsonResponse.getString("userName");
                String image = jsonResponse.getString("image");
                String message = jsonResponse.getString("Message");
                String status= jsonResponse.getString("Status");
                
                //System.out.println("id===="+id);
                System.out.println("userName===="+userName);
                System.out.println("image="+image);
                System.out.println("Message="+message);
                System.out.println("Status="+status);
                

                user = new User();
               // user.setId(id);
                user.setUserName(userName);
                user.setImage(image);
                user.setMessage(message);
                user.setStatus(status);

                
                Log.d("lengthJParams===",String.valueOf(lengthJParams));
                
               for(int i=0; i < lengthJParams; i++) 
               {
                   
            	 /*   JSONObject jo = new JSONObject(jsonString);
            	   JSONObject joParams = jo.getJSONObject("params");
            	   String username = joParams.getString("username");*/
            	   
            	   Log.d("loop===","");
            	   
            	   /****** Get Object for each JSON node.***********/
                   JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    
                  
                   
                   /******* Fetch node values **********/
                   String  longitude   = jsonChildNode.optString("longitude").toString();
                   String latitude   = jsonChildNode.optString("latitude").toString();
                   String location = jsonChildNode.optString("location").toString();
                  // String imageHistory = jsonChildNode.optString("image").toString();
                   
                   String text1 = "Status :" ;
      	           if (jsonChildNode.optString("status").toString().trim().equalsIgnoreCase("online"))
      	        	 text1 = text1 +"<font color='green'>"+ jsonChildNode.optString("status").toString()+"</font>";
           	         else 
           	        	text1 = text1 +"<font color='red'>"+ jsonChildNode.optString("status").toString()+"</font>";
           	         
      	         text1 = text1 +"     Location : "+location;
           	         String status1 =  text1;
           	         
                   String hStatus=status1;
                   //String hStatus="Status :"+jsonChildNode.optString("status").toString()+"  Location :"+location;
                   String dtime=jsonChildNode.optString("dtime").toString();
                   
                   System.out.println("jsonChildNode===" + hStatus);
                   
                   userHistory = new UserHistory();
                   
                   userHistory.setDtime(dtime);
                   userHistory.setLatitude(latitude);
                   userHistory.setLongitude(longitude);
                   userHistory.setLocation(location);
                   userHistory.setStatus(hStatus);
                   userHistory.setImageListView(image);
                   
                   userHistoryList.add(userHistory);         
                  
                   
                   String str=" \"test";
                   System.out.println("longitude="+longitude);
                   System.out.println("latitude="+latitude);
                   System.out.println("location="+location);
                   System.out.println("status="+status);
                   System.out.println("dtime="+dtime);
                   
                  
                   OutputData += "Node second : \n\n     "+ dtime +" | "
                                                   + latitude +" | "
                                                   + longitude +" | "
                                                   + status +" | "
                                                   + location +" \n\n ";
                   //Log.i("JSON parse", song_name);
              }
                
               
               	user.setUserHistory(userHistoryList);
               
               	userList.add(user);
               
           //    output.setText( OutputData );
                
           } catch (JSONException e) {
    
               e.printStackTrace();
           }
    
          return userList;
    
    }
    
    
    
    
    public List<OnlineData> parse(InputStream is) {
        
    	XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();
 
            parser.setInput(is, null);
 
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagname.equalsIgnoreCase("item")) {
                        // create a new instance of employee
                        onlineData = new OnlineData();
                    }
                    break;
 
                case XmlPullParser.TEXT:
                    text = parser.getText();
                    break;
 
                case XmlPullParser.END_TAG:
                    if (tagname.equalsIgnoreCase("item")) {
                        // add employee object to list
                    	onlineDataList.add(onlineData);
                    } else if (tagname.equalsIgnoreCase("id")) {
                    	onlineData.setId(Integer.parseInt(text));
                    } else if (tagname.equalsIgnoreCase("name")) {
                    	onlineData.setName(text);
                    } else if (tagname.equalsIgnoreCase("description")) {
                    	onlineData.setLocation(text);
                    }
                    else if (tagname.equalsIgnoreCase("cost")) {
                    	onlineData.setStatus(text);
                     //  	onlineData.setLatitude(text);
                     //  	onlineData.setLatitude(text);
                    }
                   
                    
                    break;
                    
                default:
                    break;
                    /*} else if (tagname.equalsIgnoreCase("type")) {
                    	onlineData.setType(text);
                    }*/
                    
 
               
                }
                eventType = parser.next();
            }
 
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        return onlineDataList;
    }
    
    
  
  
}