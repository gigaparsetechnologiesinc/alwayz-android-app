package com.always.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLSocketFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.always.activity.SimpleSSLSocketFactory;

import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

public class Utility {
	
	
	 private static Pattern pattern;
	 private static Matcher matcher;
	    //Email Pattern
	 private static final String EMAIL_PATTERN = 
	            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	 String mainResult=""; 

     private static String ClientTime="04-04-2015 12:34:22";
     private static String APIKey="5987ac798eb9c2ed5801E2E6E2pkJ8";
   //  private static String URL="http://dheeru-bhadoria.netne.net/test/APICloud.php";
   private static String URL="https://gigaparse.com/clients/alwayz/API/APICloud.php";
     
	 
	 public String serviceCall(int methodName, String inputParameter)
	 {
		 String response = "";
		 
		 switch(methodName)
		 {
		 	case 111: //Used in mainActivity for getting user list of login caregiver 
		 	{		 		
		 		 JSONObject jsonObj = new JSONObject();

		          JSONObject jsonclient = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient.put("APIKey", APIKey);
			          jsonclient.put("Method", methodName);
			          
			          jsonAdd.put("caregiver_id",inputParameter );
				   	  
			          jsonObj.put("Data", jsonAdd);

			          jsonObj.put("ConnectionData", jsonclient);
			          
			          response = POST(jsonObj.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				  }
		          
		          return response.toString();
		    
		 	}
		 	case 105://gives list of all caregivers in AddCaregiver screen
		 	{		 		
		 		 JSONObject jsonObj = new JSONObject();

		          JSONObject jsonclient = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient.put("APIKey", APIKey);
			          jsonclient.put("Method", methodName);
			          
			          jsonAdd.put("userType",inputParameter );
				   	  
			          jsonObj.put("Data", jsonAdd);

			          jsonObj.put("ConnectionData", jsonclient);
			          
			          response = POST(jsonObj.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		 	}
		 	case 108:
		 	{
		 		JSONObject jsonObj = new JSONObject();

		          JSONObject jsonclient = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient.put("APIKey", APIKey);
			          jsonclient.put("Method", methodName);
			          
			          if (inputParameter.split("!")[0].equalsIgnoreCase("D"))//108 for display data
			          {
			        	  jsonAdd.put("id",inputParameter.split("!")[1] );
			          }
			         else if (inputParameter.split("!")[0].equalsIgnoreCase("S"))//108 for save data
			         {
			        	 jsonAdd.put("id",inputParameter.split("!")[1] );
			        	 jsonAdd.put("latitude",inputParameter.split("!")[2] );
			        	 jsonAdd.put("logitude",inputParameter.split("!")[3] );
			         }
			          
			          
				   	  
			          jsonObj.put("Data", jsonAdd);

			          jsonObj.put("ConnectionData", jsonclient);
			          
			          response = POST(jsonObj.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		 }
		 	case 100://for login
		 	{	
		 		
		 		 JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("Method", methodName);
			          
			          jsonAdd1.put("email", inputParameter.split("!")[0]);
				   	  jsonAdd1.put("password", inputParameter.split("!")[1]);
				   	  
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		    
		 	}
		 
			case 101://for signup
		 	{	
		 		
		 		 JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("Method", methodName);
			          
			          jsonAdd1.put("email", inputParameter.split("!")[0]);
				   	  jsonAdd1.put("password", inputParameter.split("!")[1]);
				   	  
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		    
		 	}
			case 112://for logout
		 	{	
		 		
		 		 JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("Method", methodName);
			          
			          jsonAdd1.put("id", inputParameter);
				   	  			   	  
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		    
		 	}
		 	
		 	case 113:
		 	{	
		 		
		 		 JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("Method", methodName);
			          
			          System.out.println("inputParameter.split(!)[0]=="+inputParameter.split("!")[0]);
			          System.out.println("inputParameter.split(!)[1]=="+inputParameter.split("!")[1]);
			          
			          jsonAdd1.put("id", inputParameter.split("!")[0]);
				   	  jsonAdd1.put("caregiver_id", inputParameter.split("!")[1]);
				   	  
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		    
		 	}
		 	
			case 118://used for add caregiver from phone list
		 	{	
		 		
		 		 JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("Method", methodName);
			          
			          System.out.println("inputParameter.split(!)[0]=="+inputParameter.split("!")[0]);
			          System.out.println("inputParameter.split(!)[1]=="+inputParameter.split("!")[1]);
			          
			          /*@"phone":phoneNumberString,
                      @"id":[de valueForKey:@"PayUser_ID"],
                      @"name":self.user_title,
                      @"email":self.user_email,
                      @"image":encodedString
                      
                      }; method====118*/
			          
			          
			          jsonAdd1.put("id", inputParameter.split("!")[0]);
				   	  jsonAdd1.put("name", inputParameter.split("!")[1]);
				   	  jsonAdd1.put("phone", inputParameter.split("!")[2]);
				   	  jsonAdd1.put("email", inputParameter.split("!")[3]);
				   	  jsonAdd1.put("image", inputParameter.split("!")[4]);
				   	  
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		    
		 	}
			case 119://used for delete users of caregiver in manage users form
		 	{	
		 		
		 		   JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          
		          System.out.println("TimeZone.getDefault()=  "+TimeZone.getDefault());
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("TimeZone", TimeZone.getDefault());
			          jsonclient1.put("Method", methodName);
			          
			          System.out.println("inputParameter.split(!)[0]=="+inputParameter.split("!")[0]);
			          System.out.println("inputParameter.split(!)[1]=="+inputParameter.split("!")[1]);
			          
			
			     	  jsonAdd1.put("id", inputParameter.split("!")[1]);
				   	  jsonAdd1.put("caregiver_id", inputParameter.split("!")[0]);
				  
			          
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			        
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				  }
		          
		          return response.toString();
		    
		 	}
			case 114://used for forgot password
		 	{	
		 		
		 		JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          
		          System.out.println("TimeZone.getDefault()=  "+TimeZone.getDefault());
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("TimeZone", TimeZone.getDefault());
			          jsonclient1.put("Method", methodName);
			          
			     	  jsonAdd1.put("email", inputParameter);
				   	  
			     	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			        
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				  }
		          
		          return response.toString();
		    
		 	}
		 	case 103:// This service is used for updating mailid and password of login caregiver
		 	{	
		 	
		 	//update email->method 103  send ==@“id":@"1",@"status":@"1",@"value":textField.text
		 		
		 		 JSONObject jsonObj1 = new JSONObject();

		          JSONObject jsonclient1 = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd1 = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient1.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient1.put("APIKey", APIKey);
			          jsonclient1.put("Method", methodName);
			          
			          System.out.println("inputParameter.split(!)[0]=="+inputParameter.split("!")[0]);
			          System.out.println("inputParameter.split(!)[1]=="+inputParameter.split("!")[1]);
			          
			          jsonAdd1.put("id", inputParameter.split("!")[0]);
				   	  jsonAdd1.put("status",inputParameter.split("!")[1]);
				   	  jsonAdd1.put("value",  inputParameter.split("!")[2]);
				   	  jsonObj1.put("Data", jsonAdd1);

			          jsonObj1.put("ConnectionData", jsonclient1);
			          
			          response = POST(jsonObj1.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		    
		 	}
		 	case 107:
		 	{
		 		JSONObject jsonObj = new JSONObject();

		          JSONObject jsonclient = new JSONObject(); // we need another object to store the address
		          
		          JSONObject jsonAdd = new JSONObject(); // we need another object to store the address
		          
		          try {
					
		        	  jsonclient.put("ClientTime", ClientTime); // Set the first name/pair 
			          jsonclient.put("APIKey", APIKey);
			          jsonclient.put("Method", methodName);
			          
			         jsonAdd.put("id",inputParameter.split("!")[0] );
			         jsonAdd.put("latitude",inputParameter.split("!")[1] );
			         jsonAdd.put("logitude",inputParameter.split("!")[2] );
			          
				   	  
			          jsonObj.put("Data", jsonAdd);

			          jsonObj.put("ConnectionData", jsonclient);
			          
			          response = POST(jsonObj.toString());
			          
			          
		          } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				  }
		          
		          // We add the object to the main object
		          
		          
		          // and finally we add the phone number
		          // In this case we need a json array to hold the java list
		       
		         
		          return response.toString();
		 }
		     default:
		 		break;
		 	
		 }
		 
		 return response; 
	 }
	 
	 public  String POST(String jsonstring){
	        InputStream inputStream = null;
	        String result = "";
	        
	       
	        try {
	 
	        	SimpleSSLSocketFactory sslFactory = new SimpleSSLSocketFactory(null);
	        	sslFactory.setHostnameVerifier(SimpleSSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	        	
	     /*   	 SchemeRegistry registry = new SchemeRegistry();
	             SSLSocketFactory socketFactory = SSLSocketFactory
	                     .getSocketFactory();
	             socketFactory
	                     .setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
	             registry.register(new Scheme("https", socketFactory, 443));
	             SingleClientConnManager mgr = new SingleClientConnManager(
	                     client.getParams(), registry);*/
	             
	            // 1. create HttpClient
	            HttpClient httpclient = new DefaultHttpClient();
	 
	            // 2. make POST request to the given URL
	            HttpPost httpPost = new HttpPost(URL);
	 
	            String json = jsonstring;
	 
	           // 5. set json to StringEntity
	            StringEntity se = new StringEntity(json);
	 
	            // 6. set httpPost Entity
	            httpPost.setEntity(se);
	 
	            // 7. Set some headers to inform server about the type of the content   
	            httpPost.setHeader("Accept", "application/json");
	            httpPost.setHeader("Content-type", "application/json");
	            httpPost.setHeader("Accept-Charset", "utf-8");
	            // 8. Execute POST request to the given URL
	            HttpResponse httpResponse = httpclient.execute(httpPost);
	 
	            // 9. receive response as inputStream
	            inputStream = httpResponse.getEntity().getContent();
	 
	            // 10. convert inputstream to string
	            if(inputStream != null)
	                result = convertStreamToString(inputStream);
	            else
	                result = "Did not work!";
	 
	          //  System.out.println("result======"+result);
	        } catch (Exception e) {
	            //Log.d("InputStream", e.getLocalizedMessage());
	            System.out.println("result======"+e.toString());
	        }
	 
	        
	        return result;
	    }  
	    
	 public String convertStreamToString(InputStream is) {
		  BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		  StringBuilder sb = new StringBuilder();
		  String line = null;
		 
		  try {
		    while ((line = reader.readLine()) != null) {
		      sb.append(line + "\n");
		    }
		  } catch (IOException e) {
		  } finally {
		    try {
		      is.close();
		    } catch (IOException e) {
		    }
		  }
		 
		  return sb.toString();
		}
	
	 

		class dheeraj extends AsyncTask<String,Void, String> {

		    private Exception exception;
		    	
		    @Override
			protected String doInBackground(String... params) {
				// TODO Auto-generated method stub
			
				mainResult= POST(params[0]);
				Log.d("Result", mainResult);
				return mainResult;
		        //Toast.makeText(MainActivity.this,result, 1).show();
			
			}
		}
		
	    /**
	     * Validate Email with regular expression
	     * 
	     * @param email
	     * @return true for Valid Email and false for Invalid Email
	     */
	    public static boolean validate(String email) {
	        pattern = Pattern.compile(EMAIL_PATTERN);
	        matcher = pattern.matcher(email);
	        return matcher.matches();
	 
	    }
	    /**
	     * Checks for Null String object
	     * 
	     * @param txt
	     * @return true for not null and false for null String object
	     */
	    public static boolean isNotNull(String txt){
	        return txt!=null && txt.trim().length()>0 ? true: false;
	    }
	    
	
	    
}
