package com.always.model;

public class CaregiverDetail {
	
	int id;
	String image ;
	String fullName;
	
	 public CaregiverDetail()
	 {
		 
	 }
	
	 public CaregiverDetail(int id, String image,String fullName) {
			super();
			
			this.setId(id);
			this.setImage(image);
			this.setFullName(fullName);
	        
	      //  this.setHidData(hidData);
		}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	
}
