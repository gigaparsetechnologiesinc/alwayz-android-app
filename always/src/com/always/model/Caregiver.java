package com.always.model;

import java.util.List;

public class Caregiver {

	private String index;
	private List<CaregiverDetail> caregiverDetail;
	
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public List<CaregiverDetail> getCaregiverDetail() {
		return caregiverDetail;
	}
	public void setCaregiverDetail(List<CaregiverDetail> caregiverDetail) {
		this.caregiverDetail = caregiverDetail;
	}
	
	
}
