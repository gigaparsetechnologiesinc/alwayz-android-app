package com.always.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.support.v4.view.GestureDetectorCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.always.activity.MyGestureListener;
import com.always.activity.SwipeListViewActivity;
import com.always.common.ImageDownloaderTask;
import com.always.common.ImageLoader;
import com.always.common.Imageloader1;
import com.always.model.ServiceData;
import com.always.model.UserData;

import com.example.always.R;
import com.squareup.picasso.Picasso;



public class DataArrayAdapter1 extends ArrayAdapter<ServiceData> {
	 static Context mcontext;
    private static final String TAG = "DataArrayyAdapter1";
    
	private List<ServiceData> dataList = new ArrayList<ServiceData>();

	private LruCache<String, Bitmap> mMemoryCache;
	 private Context ctx;
    ImageLoader imageLoader = new ImageLoader(this);
    Context mContext;
    private int action_down_x = 0;
    private int action_up_x = 0;
    private int difference = 0;
    
    public static class DataViewHolder {
        ImageView image;
        TextView name;
        TextView status;
     //   ImageView rightImg;
       TextView test;
		
       static Button btn_remove;
       GestureDetectorCompat mDetector;
    }

  
    
    public DataArrayAdapter1(Context context, int textViewResourceId) {
    	 super(context, textViewResourceId);
    	 
    	// Get memory class of this device, exceeding this amount will throw an
        // OutOfMemory exception.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
 
        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;
 
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
 
            @SuppressLint("NewApi") protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in bytes rather than number
                // of items.
                return bitmap.getByteCount();
            }
 
        };
        
       
    }

    
	@Override
	public void add(ServiceData object) {
		dataList.add(object);
		super.add(object);
	}
	
	
	
	/*public void add1(UserData object) {
		dataList.add1(object);
		super.add1(object);
	}*/

	@Override
	public int getCount() {
		return this.dataList.size();
	}

    @Override
	public ServiceData getItem(int index) {
		return this.dataList.get(index);
	}
    
    
   /* class MyTouchListener implements OnTouchListener
    {
    	
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			
			System.out.println("MyTouchListener");
			
			DataViewHolder holder = (DataViewHolder) v.getTag(R.layout.listview_main_map);
			int action = event.getAction();
			int position = (Integer) v.getTag();

			switch (action) {
			case MotionEvent.ACTION_DOWN:
				action_down_x = (int) event.getX();
				Log.d("action", "ACTION_DOWN - ");
				break;
			case MotionEvent.ACTION_MOVE:
				Log.d("action", "ACTION_MOVE - ");
				action_up_x = (int) event.getX();
				difference = action_down_x - action_up_x;
				break;
			case MotionEvent.ACTION_UP:
				Log.d("action", "ACTION_UP - ");
			//	calcuateDifference(holder, position);
				action_down_x = 0;
				action_up_x = 0;
				difference = 0;
				break;
			}
			return true;
		}
    }
    
    private void calcuateDifference(final DataViewHolder holder, final int position) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (difference == 0) {
					Toast.makeText(mContext, "difference 0", Toast.LENGTH_LONG).show();
				}
				if (difference > 75) {
					DataViewHolder.btn_remove.setVisibility(View.VISIBLE);
					//items.get(position).setVisible(true);
					//mAdapter.changeData(items);
					Toast.makeText(mContext, "Right to Left - "+position, Toast.LENGTH_LONG).show();
				}
				if (difference < -75) {
					DataViewHolder.btn_remove.setVisibility(View.GONE);
					//items.get(position).setVisible(false);
					//mAdapter.changeData(items);
					Toast.makeText(mContext, "Left to Right - "+position, Toast.LENGTH_LONG).show();
				}
			}
		});
	}*/
    
   /* private void runOnUiThread(Runnable runnable) {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        DataViewHolder viewHolder;
       // Activity cntx = new Activity();
        
		if (row == null) {
			
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
		
			row = inflater.inflate(R.layout.listview_main_map, parent, false);
			
            viewHolder = new DataViewHolder();
            
            
            viewHolder.image = (ImageView) row.findViewById(R.id.image);
            viewHolder.name = (TextView) row.findViewById(R.id.name);
            viewHolder.status = (TextView) row.findViewById(R.id.status);
        //    viewHolder.rightImg= (ImageView) row.findViewById(R.id.rightImage);
            viewHolder.test=(TextView) row.findViewById(R.id.test);
           // viewHolder.btn_remove=(Button) row.findViewById(R.id.btn_remove);
            
           /* viewHolder.mDetector = new GestureDetectorCompat(ctx,
                    new MyGestureListener(ctx, convertView));*/
            convertView.setTag(viewHolder);
          //  viewHolder.hidData=(TextView) row.findViewById(R.id.hidData);
            
            
            row.setTag(viewHolder);
            
            /*viewHolder.container.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                	viewHolder.mDetector.onTouchEvent(event);
                    return true;
                }
            });
            */
            
		} else {
			
            viewHolder = (DataViewHolder)row.getTag();
            
        }
		
		
		
        
		
		ServiceData serviceData = getItem(position);
		
		
	//	Bitmap bMap = BitmapFactory.decodeFile("http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
		// Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.baby);
		// viewHolder.fruitImg.setImageBitmap(bMap);
		 		
		 
		 
		 
    //    viewHolder.fruitImg.setImageResource(fruit.getFruitImg());
	 //   viewHolder.image.setImageResource(R.drawable.baby);
	//	ImageView imgflag = (ImageView) findViewById(R.id.baby);
	    
	//    viewHolder.rightImg.setImageResource(R.drawable.right_arrow1);
        
		//viewHolder.fruitImg.setImageBitmap(getRoundedShape(decodeFile(mcontext,R.drawable.baby),200));
		//viewHolder.fruitImg.setImageBitmap(getRoundedShape(decodeFile(cntx,R.drawable.baby),200));
		
		 //im.setImageBitmap(getRoundedShape(decodeFile(cntx, listview_images[position]),200));
        
        viewHolder.name.setText(serviceData.getName());
        viewHolder.status.setText(Html.fromHtml(serviceData.getStatus()));
        viewHolder.test.setText(serviceData.getTest());
        
        //AQuery aq = new AQuery(this);
        //aq.id(R.id.image1).image("http://www.vikispot.com/z/images/vikispot/android-w.png");
        
        System.out.println("serviceData.getImageListView() === "+serviceData.getImageListView()+" length " +serviceData.getImageListView().length());
        
        System.out.println("serviceData url"+serviceData.getImageListView());
        
        if( (serviceData.getImageListView()!= null) && (serviceData.getImageListView().length()>0))
        		{
        			Picasso.with(getContext()).load(serviceData.getImageListView()).into(viewHolder.image);	
        		}
        
  
      /*  Imageloader1 imageLoader = new Imageloader(getApplicationContext());
        
        imageLoader.DisplayImage(serviceData.getImageListView(),DataArrayAdapter.this,viewHolder.image );*/
        //Picasso.with(context).load("http://yourimage.jpg").into(yourImageView);
        
        /*if (viewHolder.image != null) {
        	//imageLoader.DisplayImage(serviceData.getImageListView(),  viewHolder.image);
			new ImageDownloaderTask(viewHolder.image).execute(serviceData.getImageListView());
		}
        */
        
       // viewHolder.hidData.setText(serviceData.getHidData());
        
       
        
		return row;
	}


    
 
		public static Bitmap decodeFile(Context context,int resId) {
						try {
							// decode image size
							mcontext=context;
							BitmapFactory.Options o = new BitmapFactory.Options();
							o.inJustDecodeBounds = true;
							BitmapFactory.decodeResource(mcontext.getResources(), resId, o);
							// Find the correct scale value. It should be the power of 2.
							final int REQUIRED_SIZE = 200;
							int width_tmp = o.outWidth, height_tmp = o.outHeight;
							int scale = 1;
							while (true)
							{
							 if (width_tmp / 2 < REQUIRED_SIZE
							 || height_tmp / 2 < REQUIRED_SIZE)
							 break;
							 width_tmp /= 2;
							 height_tmp /= 2;
							 scale++;
						}
						// decode with inSampleSize
						BitmapFactory.Options o2 = new BitmapFactory.Options();
						o2.inSampleSize = scale;
						return BitmapFactory.decodeResource(mcontext.getResources(), resId, o2);
						} catch (Exception e) {
				}
				return null;
		}



    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width) {
   	 // TODO Auto-generated method stub
   	 int targetWidth = width;
   	 int targetHeight = width;
   	 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
   	 targetHeight,Bitmap.Config.ARGB_8888);

   	 Canvas canvas = new Canvas(targetBitmap);
   	 Path path = new Path();
   	 path.addCircle(((float) targetWidth - 1) / 2,
   	 ((float) targetHeight - 1) / 2,
   	 (Math.min(((float) targetWidth),
   	 ((float) targetHeight)) / 2),
   	 Path.Direction.CCW);
   	 canvas.clipPath(path);
   	 Bitmap sourceBitmap = scaleBitmapImage;
   	 canvas.drawBitmap(sourceBitmap,
   	 new Rect(0, 0, sourceBitmap.getWidth(),
   	 sourceBitmap.getHeight()),
   	 new Rect(0, 0, targetWidth,
   	 targetHeight), null);
   	 return targetBitmap;
   	 }
   	
    

    public Bitmap decodeToBitmap(byte[] decodedByte) {
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
	}
}
