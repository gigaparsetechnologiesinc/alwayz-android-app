package com.always.adapter;

import java.util.ArrayList;
import java.util.List;

import com.always.model.ServiceData;
import com.example.always.R;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CaregiverArrayAdapter extends ArrayAdapter<ServiceData> {
	 static Context mcontext;
	    private static final String TAG = "DataArrayAdapter";
	    private String prevSeparator="0";
		private List<ServiceData> dataList = new ArrayList<ServiceData>();

	    static class DataViewHolder {
	        ImageView image;
	        TextView name;
	        TextView status;
	        ImageView rightImg;
	    }

	 /*   
	    static class DataViewHolderSeparator {
	        TextView separator;
		
	    }
*/
	    
	 
	    
    public CaregiverArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

		@Override
		public void add(ServiceData object) {
			dataList.add(object);
			super.add(object);
		}

	    @Override
		public int getCount() {
			return this.dataList.size();
		}

	    @Override
		public ServiceData getItem(int index) {
			return this.dataList.get(index);
		}

	    
	    @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
	        DataViewHolder viewHolder;
	     //   DataViewHolderSeparator viewHolderSeparator;
	       // Activity cntx = new Activity();
	        
	        
	        ServiceData serviceData = getItem(position);
	        
	    //   if (prevSeparator.equalsIgnoreCase(String.valueOf(Character.toUpperCase(serviceData.getName().charAt(0))) ))
	        {
					if (row == null) {
						
						LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						
					
						row = inflater.inflate(R.layout.list_row_layout_acaregiver, parent, false);
						
			            
						viewHolder = new DataViewHolder();
			            
			            viewHolder.image = (ImageView) row.findViewById(R.id.image);
			            viewHolder.name = (TextView) row.findViewById(R.id.name);
			            viewHolder.status = (TextView) row.findViewById(R.id.status);
			            /*viewHolder.rightImg= (ImageView) row.findViewById(R.id.rightImage);*/
			       //     viewHolder.test=(TextView) row.findViewById(R.id.test);
			            
			            
			            row.setTag(viewHolder);
					} else {
						
			            viewHolder = (DataViewHolder)row.getTag();
			            
			        }
					
					//ServiceData serviceData = getItem(position);
				    viewHolder.image.setImageResource(R.drawable.baby);
				    
				    
				   /* viewHolder.rightImg.setImageResource(R.drawable.right_arrow1);*/
			        
					viewHolder.name.setText(serviceData.getName());
			        viewHolder.status.setText(Html.fromHtml(serviceData.getTest()));
	       }
	     /*  else
	        {

	        	
		        
				if (row == null) {
					
					LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					
				
					row = inflater.inflate(R.layout.section_header, parent, false);
					
		          //  viewHolder = new DataViewHolder();
		            
					viewHolderSeparator = new DataViewHolderSeparator();
		            viewHolderSeparator.separator = (TextView) row.findViewById(R.id.separator);
		         
		            
		            row.setTag(viewHolderSeparator);
				} else {
					
		            viewHolderSeparator = (DataViewHolderSeparator)row.getTag();
		            
		        }
				
				   viewHolderSeparator.separator.setText(String.valueOf(Character.toUpperCase(serviceData.getName().charAt(0))) );
				   
	        }*/
	      
	        
	        prevSeparator= String.valueOf(Character.toUpperCase(serviceData.getName().charAt(0)));
	        		
			return row;
		}


			public static Bitmap decodeFile(Context context,int resId) {
							try {
								// decode image size
								mcontext=context;
								BitmapFactory.Options o = new BitmapFactory.Options();
								o.inJustDecodeBounds = true;
								BitmapFactory.decodeResource(mcontext.getResources(), resId, o);
								// Find the correct scale value. It should be the power of 2.
								final int REQUIRED_SIZE = 200;
								int width_tmp = o.outWidth, height_tmp = o.outHeight;
								int scale = 1;
								while (true)
								{
								 if (width_tmp / 2 < REQUIRED_SIZE
								 || height_tmp / 2 < REQUIRED_SIZE)
								 break;
								 width_tmp /= 2;
								 height_tmp /= 2;
								 scale++;
							}
							// decode with inSampleSize
							BitmapFactory.Options o2 = new BitmapFactory.Options();
							o2.inSampleSize = scale;
							return BitmapFactory.decodeResource(mcontext.getResources(), resId, o2);
							} catch (Exception e) {
					}
					return null;
			}



	    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width) {
	   	 // TODO Auto-generated method stub
	   	 int targetWidth = width;
	   	 int targetHeight = width;
	   	 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
	   	 targetHeight,Bitmap.Config.ARGB_8888);

	   	 Canvas canvas = new Canvas(targetBitmap);
	   	 Path path = new Path();
	   	 path.addCircle(((float) targetWidth - 1) / 2,
	   	 ((float) targetHeight - 1) / 2,
	   	 (Math.min(((float) targetWidth),
	   	 ((float) targetHeight)) / 2),
	   	 Path.Direction.CCW);
	   	 canvas.clipPath(path);
	   	 Bitmap sourceBitmap = scaleBitmapImage;
	   	 canvas.drawBitmap(sourceBitmap,
	   	 new Rect(0, 0, sourceBitmap.getWidth(),
	   	 sourceBitmap.getHeight()),
	   	 new Rect(0, 0, targetWidth,
	   	 targetHeight), null);
	   	 return targetBitmap;
	   	 }
	   	
	    

	    public Bitmap decodeToBitmap(byte[] decodedByte) {
			return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
		}
}
