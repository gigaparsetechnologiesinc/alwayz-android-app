package com.always.activity;


import com.example.always.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class WhoAreYouActivity extends Activity {

	ImageView imgCareGiver;
	ImageView imgUser;
	TextView txtCareGiver;
	TextView txtUser;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_who_are_you);
		
		imgCareGiver=(ImageView) findViewById(R.id.imgCareGiver);
		imgUser=(ImageView) findViewById(R.id.imgUser);
		txtUser=(TextView)findViewById(R.id.txtUser);
		txtCareGiver=(TextView) findViewById(R.id.txtCareGiver);
		
		imgCareGiver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent =new Intent();
				intent.setClass(getApplicationContext(), AccountSetUpSmallActivity.class);
				startActivity(intent);
			}
		});
		
		txtCareGiver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent =new Intent();
				intent.setClass(getApplicationContext(), AccountSetUpSmallActivity.class);
				startActivity(intent);
			}
		});
		
		
		imgUser.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent =new Intent();
				intent.setClass(getApplicationContext(), AccountSetUpLargeActivity.class);
				startActivity(intent);
			}
		});
		
		txtUser.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent =new Intent();
				intent.setClass(getApplicationContext(), AccountSetUpLargeActivity.class);
				startActivity(intent);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.who_are_you, menu);
		return true;
	}

	
}
