package com.always.activity;


import org.json.JSONException;
import org.json.JSONObject;

import com.always.common.Utility;
import com.example.always.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MyAccountActivity extends Activity {

	ImageView imgUEmail;
	ImageView imgUEmailArrow;
	TextView txtUEmail;
	
	ImageView imgCPass;
	TextView txtCPass;
	ImageView imgCPassArrow;
	
	ImageView imgLogout;
	TextView txtLogout;
	ImageView imgLogoutArrow;
	
	ImageView imgDAccount;
	ImageView  imgBackA;
	TextView txtBackA;
	final Context context = this;
	String userId="";
	
	public static final String MY_EMP_PREFS = "MySharedPref"; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_account);
		
		imgUEmail=(ImageView)findViewById(R.id.imgUEmail);
		txtUEmail=(TextView)findViewById(R.id.txtUEmail);
		imgUEmailArrow=(ImageView) findViewById(R.id.imgUEmailArrow);
	
		
		imgCPass=(ImageView)findViewById(R.id.imgCPass);
		txtCPass=(TextView) findViewById(R.id.txtCPass);
		imgCPassArrow=(ImageView)findViewById(R.id.imgCPassArrow);
		
		imgLogout=(ImageView)findViewById(R.id.imgLogout);
		txtLogout=(TextView) findViewById(R.id.txtLogout);
		imgLogoutArrow=(ImageView)findViewById(R.id.imgLogoutArrow);
		
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		
    	userId=  settings.getString("id", "0");
    	
    	
      imgLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				 // get prompts.xml view
				logout();
				
			}
		});
		
		txtLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				logout();
			}
		});
		
		imgLogoutArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				logout();
			}
		});
		
		
		
		imgUEmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				 // get prompts.xml view
				updateEmail();
				
			}
		});
		
		txtUEmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updateEmail();
			}
		});
		
		imgUEmailArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updateEmail();
			}
		});
		
		imgCPass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updatePassword();
			}
		});
		
		txtCPass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updatePassword();
			}
		});
		
		imgCPassArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updatePassword();
			}
		});
		
		
		txtBackA=(TextView) findViewById(R.id.txtBackA);
		txtBackA.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});
		
		

		
		imgBackA=(ImageView) findViewById(R.id.imgBackA);
		imgBackA.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			/*	Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});
		
		
	}
	
	private void logout()
	{
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		userId=  settings.getString("id", "0");
    	
		
		Utility utility=new Utility();
		utility.serviceCall(112, userId);
		
		
		context.getSharedPreferences(MY_EMP_PREFS, 0).edit().clear().commit();
		 
		Intent intent =new Intent();
		intent.setClass(getApplicationContext(), SplashActivity.class);
		startActivity(intent);
		
		finish();
	}

	private void updateEmail()
	{

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set prompts.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);
        final EditText input = (EditText) promptView.findViewById(R.id.userInput);
        
        alertDialogBuilder.setTitle("Update Email");
        
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                               // editTextMainScreen.setText(input.getText());
                            	Utility utility=new Utility();
                				String userData=utility.serviceCall(103,userId+"!"+"1"+"!"+input.getText());
                            	
                				System.out.println("103 result "+userData);
                				
                				try {
                			  		
                			  		String message="";
                					JSONObject jsonResponse = new JSONObject(userData);
                					
                					System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
                					message=jsonResponse.optString("Message");
                					
                					if (message.equalsIgnoreCase("Success"))
                					{
                						                						
                						Toast.makeText(getApplicationContext(), "Email Updated Successfully.", Toast.LENGTH_LONG).show();
                					}
                					else
                					{
                						
                						Toast.makeText(getApplicationContext(), "Email Not Updated", Toast.LENGTH_LONG).show();;
                					}
                					
                				} catch (JSONException e) {
                					// TODO Auto-generated catch block
                					e.printStackTrace();
                				}
                				
                            }
                        })
		                        .setNegativeButton("Cancel",

                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                            }

                        });

        // create an alert dialog

        AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();
	}
	
	
	private void updatePassword()
	{

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set prompts.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);
        final EditText input = (EditText) promptView.findViewById(R.id.userInput);
        
        alertDialogBuilder.setTitle("Update Password");
        
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                               // editTextMainScreen.setText(input.getText());
                            	Utility utility=new Utility();
                				String userData=utility.serviceCall(103,userId+"!"+"2"+"!"+input.getText());
                            	
                				System.out.println("103 result "+userData);
                				
                				try {
                			  		
	                			  		String message="";
	                					JSONObject jsonResponse = new JSONObject(userData);
	                					
	                					System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
	                					message=jsonResponse.optString("Message");
	                					
	                					if (message.equalsIgnoreCase("Success"))
	                					{
	                						                						
	                						Toast.makeText(getApplicationContext(), "Password Updated Successfully.", Toast.LENGTH_LONG).show();
	                					}
	                					else
	                					{
	                						
	                						Toast.makeText(getApplicationContext(), "Password Not Updated", Toast.LENGTH_LONG).show();;
	                					}
                					
                				} catch (JSONException e) {
                					// TODO Auto-generated catch block
                					e.printStackTrace();
                				}

                            }
                        })
		                        .setNegativeButton("Cancel",

                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                            }

                        });

        // create an alert dialog

        AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();
	}
	

}
