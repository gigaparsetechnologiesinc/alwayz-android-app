package com.always.activity;


import java.io.ByteArrayOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import com.always.common.Utility;
import com.example.always.R;
import com.loopj.android.http.RequestParams;

import android.annotation.SuppressLint;
import android.widget.RelativeLayout.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddConfirmActivity extends Activity {

	TextView txtConfirmNo; 
	TextView txtConfirmMsg1;
	TextView txtConfirmYes;
	String details;
	LoginActivity loginActivity;
	DisplayMetrics displaymetrics;
	 String name ;
	 ImageView image;
	 String imageEncoded ;
	public static final String MY_EMP_PREFS = "MySharedPref";  
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_confirm);
		
		 displaymetrics = new DisplayMetrics();
	  	 getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
	  	 int height = displaymetrics.heightPixels;
	  		int width = displaymetrics.widthPixels;
	  		
	  		Toast.makeText(getApplicationContext(), "Height "+height+"  Width "+width, Toast.LENGTH_LONG).show();
	  	//
	  		
	 // width=width/2;
	  //LayoutParams params = new LayoutParams(width,LayoutParams.FILL_PARENT);
	  
	  		
		txtConfirmNo=(TextView) findViewById(R.id.txtConfirmNo);
		
		txtConfirmMsg1=(TextView) findViewById(R.id.txtConfirmMsg1);
		txtConfirmYes=(TextView) findViewById(R.id.txtConfirmYes);
		
		image=(ImageView) findViewById(R.id.imgProfile1);
		//txtConfirmNo.setLayoutParams(params);
		
		//txtConfirmYes.setLayoutParams(params);
	
		Intent intent = getIntent();
        
         name = intent.getStringExtra("name");
        details = intent.getStringExtra("details");
        
        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
        image.setImageBitmap(bitmap);
        
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        
        
        Toast.makeText(getApplicationContext(), "details=="+details, Toast.LENGTH_LONG).show();
        
        int length=details.split("!").length;
        
        String phone;
        String email;
        
        if (length==1)
        {
            phone=details.split("!")[0];
        }
        else
        {
        	phone=details.split("!")[0];
            email=details.split("!")[1];
        }
        
        txtConfirmMsg1.setText(Character.toUpperCase(name.charAt(0)) + name.substring(1));
		
		txtConfirmNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 finish();
//				// TODO Auto-generated method stub
//				Intent intent = new Intent();
//		        intent.setClass(getApplicationContext(),MainActivity.class);
//				startActivity(intent);
			}
		});
		

		
		txtConfirmYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	
			
				SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
				String id=  settings.getString("id", "0");
				
				//Toast.makeText(getApplicationContext(), "details "+details.split("!")[0]+" id "+id, Toast.LENGTH_LONG).show();
				
			/*	@"phone":phoneNumberString,
                @"id":[de valueForKey:@"PayUser_ID"],
                @"name":self.user_title,
                @"email":self.user_email,
                @"image":encodedString
                
                }; method====118*/
                
                
				Utility utility=new Utility();
				String userData=utility.serviceCall(118,id+"!"+name+"!"+details+"!"+imageEncoded);
			  	System.out.println("reader111=="+userData);
			  	
			  	try {
			  		
			  		String message="";
					JSONObject jsonResponse = new JSONObject(userData);
					
					System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
					message=jsonResponse.optString("Message");
					
					if (message.equalsIgnoreCase("Success"))
					{
												
						SharedPreferences settingsEdit = getSharedPreferences(MY_EMP_PREFS,0); 
						//need an editor to edit and save values
						SharedPreferences.Editor editor = settingsEdit.edit();
						
						System.out.println("id update in shared =="+id);
					//	System.out.println("fullName update in shared=="+ details.split("!")[1]);
						
						editor.putString("caregiver_id", id);
						//editor.putString("caregiverName",details.split("!")[1]);
					//	editor.putString("caregiverImage", json_data.getString("image"));
						
						
						editor.commit();
						
						Toast.makeText(getApplicationContext(), "Caregiver successfully added.", Toast.LENGTH_LONG).show();
						
						
						SharedPreferences settings1 	= getSharedPreferences(MY_EMP_PREFS, 0);
						String caregiver_id=  settings.getString("caregiver_id", "0");
						
						System.out.println("caregiver_idcaregiver_id"+caregiver_id);
						System.out.println("caregiver_idcaregivername"+  settings.getString("fullName", "0"));
						
						
						Intent i =new Intent();
						i.setClass(getApplicationContext(), PatientHomeActivity.class);
						startActivity(i);
					
					}
					else
					{
						
						Toast.makeText(getApplicationContext(), "Caregiver not added.", Toast.LENGTH_LONG).show();;
/*						Intent i =new Intent();
						i.setClass(getApplicationContext(), AddCaregiver2Activity.class);
						startActivity(i);
*/					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  	
			  	
			  	/* loginActivity=new LoginActivity();
				
				  RequestParams params = new RequestParams();
				  String email= settings.getString("email", "Email no exist");
	                // Put Http parameter password with value of Password Edit Value control
	              String password= settings.getString("password", "Password no exist");
	              
	              System.out.println("settings.getString(email)=="+settings.getString("email", "Email no exist"));
	            		  System.out.println("settings.getString(password)=="+settings.getString("password", "Password no exist"));
	              
	             String  serviceResponse= loginActivity.invokeWS(email+"!"+password);
				
	             System.out.println("serviceResponse confirm=="+serviceResponse);
			*/
				
		    }
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_confirm, menu);
		return true;
	}

	
}
