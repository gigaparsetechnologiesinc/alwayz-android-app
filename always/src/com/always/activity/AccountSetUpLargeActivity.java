package com.always.activity;


import com.always.common.Utility;
import com.example.always.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class AccountSetUpLargeActivity extends Activity {

	ImageView imgProfile1;
	TextView txtNextL;
	EditText editTxtFullNameL;
	EditText editTxtEmailL;
	EditText editPhoneL;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_set_up_large);
		
		imgProfile1=(ImageView) findViewById(R.id.imgProfile1);
		imgProfile1.setImageResource(R.drawable.person);
		
		editTxtFullNameL=(EditText)findViewById(R.id.editTxtFullNameS);
		editTxtEmailL=(EditText) findViewById(R.id.editTxtEmailS);
		editPhoneL=(EditText) findViewById(R.id.editPhoneS);
		txtNextL=(TextView) findViewById(R.id.txtNextS);
		
	
			imgProfile1.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
			
			          Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

	                    startActivityForResult(intent, 2);
				}
			});
			
			
			txtNextL.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					if(Utility.isNotNull(editTxtEmailL.getText().toString()) &&  Utility.isNotNull(editTxtFullNameL.getText().toString()) 
							&&  Utility.isNotNull(editPhoneL.getText().toString())  )
					{
						
						
						Toast.makeText(getApplicationContext(), "editTxtEmailL="+editTxtEmailL.getText().toString(),Toast.LENGTH_LONG).show();
						
						if(Utility.validate(editTxtEmailL.getText().toString().trim())){
							
					
							
								SharedPreferences.Editor editor1 = getSharedPreferences("Session", MODE_PRIVATE).edit();
								editor1.putString("UserFullName", editTxtFullNameL.getText().toString());
								editor1.putString("UserEmail", editTxtEmailL.getText().toString());
								editor1.putString("UserPhone",editPhoneL.getText().toString());
								editor1.commit();
								
								Intent intent =new Intent();
								intent.setClass(getApplicationContext(), AddCaregiver2Activity.class);
								startActivity(intent);
								
						}
						 else{
				             Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
				         }
					  }
					else{
				          Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
				      }
					
						
					}
			});
			
		
	}
	
	
	
	 @Override

	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	        super.onActivityResult(requestCode, resultCode, data);

	        if (resultCode == RESULT_OK) {

	      if (requestCode == 2) {

	 

	                Uri selectedImage = data.getData();

	                String[] filePath = { MediaStore.Images.Media.DATA };

	                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

	                c.moveToFirst();

	                int columnIndex = c.getColumnIndex(filePath[0]);

	                String picturePath = c.getString(columnIndex);

	                c.close();

	                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

	                Log.w("path of image from gallery......******************......;...", picturePath+"");

	                ImageView viewImage;
	                viewImage=(ImageView) findViewById(R.id.imgProfile1);
					viewImage.setImageBitmap(thumbnail);

	            }

	        }
	 }
}
