package com.always.activity;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.always.adapter.UserArrayAdapter;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.example.always.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ManageUsersActivity extends Activity {

	
	 private static final String TAG = "ListViewActivity";

	    private UserArrayAdapter userArrayAdapter;
		private ListView listView;
		String uid;
		private static int colorIndex;
		
		// static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 
		 TextView txtBack;
		 ImageView  imgBack;	 
		 public static final String MY_EMP_PREFS = "MySharedPref";  
		 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_users);
		
		txtBack=(TextView) findViewById(R.id.txtBack);
		
		txtBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i =new Intent();
				i.setClass(getApplicationContext(), MainMapActivity.class);
				startActivity(i);
				
				 finish();
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});

		
		imgBack=(ImageView) findViewById(R.id.imgBack);
	
		
		imgBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i =new Intent();
				i.setClass(getApplicationContext(), MainMapActivity.class);
				startActivity(i);
				
				finish();
				
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});
		
		
		
		fillListView();
		
	}
	
	
private void fillListView()
{
	
	 colorIndex = 0;
		listView = (ListView) findViewById(R.id.listView);
		
	//CAll service to fill listview data
		
			Utility utility=new Utility();
			
			SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
			String cid=  settings.getString("id", "0");
	    	
			String userData=utility.serviceCall(111,cid);
		
		    List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	        
	         /*   XMLPullParserHandler parser = new XMLPullParserHandler();
	            
	            String xml = null;
	         	            
	            xml = getXmlFromUrl(urls);
	 
	           InputStream stream = new ByteArrayInputStream(xml.getBytes());
	         
	           onlineDataList = parser.parse(stream);
	 */           
		   XMLPullParserHandler parser = new XMLPullParserHandler();
  	   onlineDataList = parser.parseJSON(userData);
  	            
  	   
  	   
	           Toast.makeText(getApplicationContext(), " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
	         
		   	             
		   	userArrayAdapter = new UserArrayAdapter(getApplicationContext(), R.layout.list_row_layout_acaregiver);
		   	listView.setAdapter(userArrayAdapter);
		           
		   	  for (OnlineData onlineData:onlineDataList)
		  		{
		       	
		   		  	 int id=onlineData.getId();
		       	     String image = onlineData.getName();
		             String name = onlineData.getName();
		         	
		             String text = "Status :" ;
		             
		             if (onlineData.getStatus().equalsIgnoreCase("online"))
		            	 text = text +"<font color='green'>"+ onlineData.getStatus()+"</font>";
		             else 
		            	 text = text +"<font color='red'>"+ onlineData.getStatus()+"</font>";
		             
		        	 text = text +"     Location : "+onlineData.getLocation();
		             String status =  text;
		             //"Status : "+ onlineData.getStatus()+"     Location : "+onlineData.getLocation();
		             
		             String test =String.valueOf(onlineData.getId());// onlineData.getStatus()+"$"+onlineData.getStatus()+"$"+onlineData.getId();
		             String imageListView = onlineData.getImage();
		             
		             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
		             
		           ServiceData serviceData = new ServiceData(imgResId,name,status,test,imageListView );//,hidData);
		           
		         //   serviceData.setTest(String.valueOf(id));
		            userArrayAdapter.add(serviceData);
		             
		     	 //  array_sort.add(onlineData.getName());
		         // Toast.makeText(getApplicationContext(), " onlineData.getName() ="+ onlineData.getName(), Toast.LENGTH_LONG).show();
		     	   //array_sort1.add(onlineData.getLocation());
		  		}
		       
		   	  
		   	 listView.setOnItemClickListener(new OnItemClickListener() {
		     		
				  public void onItemClick(AdapterView<?> arg0,
				  View arg1, int position, long arg3)
				  {
					  

					    TextView tv_name= (TextView) arg1.findViewById(R.id.uid);
					     uid =tv_name.getText().toString();
					    
					  Toast.makeText(getApplicationContext(), "uid ="+uid,  Toast.LENGTH_SHORT).show();
				   
				   final CharSequence[] items = {
			                "Delete", "Details", "Cancel"
			        };

			      
				   AlertDialog.Builder builder3=new AlertDialog.Builder(ManageUsersActivity.this);
				   builder3.setTitle("Manage Users.").setItems(items, new DialogInterface.OnClickListener()
				   {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
					   // TODO Auto-generated method stub
						 //  Toast.makeText(getApplicationContext(), "U clicked "+which+ " " +items[which], Toast.LENGTH_LONG).show();
						   
						   if (which==0)//used tor delete
						   { 
							   
							   Toast.makeText(getApplicationContext(), "119  " +uid,Toast.LENGTH_LONG).show();
							   
								
					   			SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
					   			String id=  settings.getString("id", "0");
							   
							    Utility utility=new Utility();
					   			String userData=utility.serviceCall(119,id+"!"+uid);
					   			
					   			fillListView();
					   			
					   		//	startActivity(getIntent());
					   			//onActivityResult
					   			//startActivtyForResult


					   			//listView.remove(which);
								
							
								
							//	notifyDataSetChanged();


					   			
						   }
						   else  if (which==1)//used for details
						   {
							   
						   }
						   else  if (which==2)//used for cancel
						   {
							   finish();
						   }
					   }
					   
				   });
				   builder3.show();
				   				   
				  }
		 });
		   	  
}
	
private String getXmlFromUrl(String urlString) {

Toast.makeText(getApplicationContext(), "getXMLFRomURL", Toast.LENGTH_LONG);


StringBuffer output = new StringBuffer("");
    try {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        HttpURLConnection httpConnection = (HttpURLConnection) connection;
        httpConnection.setRequestMethod("GET");
        httpConnection.connect();

        if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            stream = httpConnection.getInputStream();

            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(stream));
            String s = "";
        while ((s = buffer.readLine()) != null)
            output.append(s);
    }
     
} catch (Exception ex) {
    ex.printStackTrace();
}
    
return output.toString();
 
/* ---Using Apache DefaultHttpClient for applications targeting 
 Froyo and previous versions --- */
/*String xml = null;

    try {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        xml = EntityUtils.toString(httpEntity);

    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    } catch (ClientProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return xml;*/
}  



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_users, menu);
		return true;
	}

	
	
	
	@Override
	public void onBackPressed() {
	// do something on back.
		Intent i =new Intent();
		i.setClass(getApplicationContext(), MainMapActivity.class);
		startActivity(i);
		return;
	}
	
	protected void onResume(Bundle savedInstanceState) {
		super.onResume();
		
	     
	}
	
	/*@Override
	protected void onResume() {

	   super.onResume();
	   this.onCreate(null);
	}*/
}
