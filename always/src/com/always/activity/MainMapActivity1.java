package com.always.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import com.always.adapter.DataArrayAdapter1;
import com.always.adapter.DataArrayAdapter1.DataViewHolder;
import com.always.common.ImageLoader;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.always.model.UserData;
import com.always.activity.SwipeListView;
import com.always.activity.SwipeListView.SwipeListViewCallback;


import com.example.always.R;
import com.example.always.R.id;
import com.example.always.R.layout;


import com.navdrawer.SimpleSideDrawer;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.support.v4.view.GestureDetectorCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;





import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;


public class MainMapActivity1 extends Activity implements SwipeListViewCallback  {

	public  List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	SimpleSideDrawer slide_me;
	public static final String MY_EMP_PREFS = "MySharedPref";  
	    WebView web;
	
	    TextView txtUsers;
	
		private static final String TAG = "ListViewActivity";
		
		private DataArrayAdapter dataArrayAdapter;
		private ListView listView;
		
		private static int colorIndex;
		
		static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 
		private final static String SERVICE_URI = "http://www.gigaparse.com/clients/alwayz/API/APICloud.php";
		int height;
		int width ;
		DisplayMetrics displaymetrics;
		
	 TextView txtMessage;
	 TextView txtInvite;
		
	 int cntUser=0;
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
        displaymetrics = new DisplayMetrics();
  		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
  		height = displaymetrics.heightPixels;
  		width = displaymetrics.widthPixels;
  		
  		
           // setActivity();
  		setContentView(R.layout.activity_main_map);
       	   
      		
      	slide_me = new SimpleSideDrawer(this);
      	//runSlider();
      	slide_me.setLeftBehindContentView(R.layout.left_menu);
      		
    		ImageView left_button; 
    	    left_button = (ImageView) findViewById(R.id.imgLeft);
       		
    	   	
       		left_button.setOnClickListener(new View.OnClickListener() {

       			@Override
       			public void onClick(View v) {
       				// TODO Auto-generated method stub
       				slide_me.toggleLeftDrawer();
       			
       			}
       		});
       		
        
       	//CAll service to fill listview data
		Utility utility=new Utility();
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		
    	String cid=  settings.getString("id", "0");
    	String imageProfile= settings.getString("image", "0");
    	
		String userData=utility.serviceCall(111,cid);
		
		System.out.println("Mainmap userData "+userData);
		
		System.out.println("caregiver_idcaregiver_idcaregiver_id"+cid);
		
		TextView txtCenter;
		
		txtCenter=(TextView)findViewById(R.id.txtCenter);
			 
		txtCenter.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (cntUser>0)
				{
					Toast.makeText(getApplicationContext(), "Click",Toast.LENGTH_LONG).show();
					Intent intent=new Intent();
					intent.setClass(getApplicationContext(), ManageUsersActivity.class);
					startActivity(intent);
				}
			}
		});
		  
	    	 web = (WebView)findViewById(R.id.webview);
	         web.getSettings().setJavaScriptEnabled(true);
	         web.getSettings().setBuiltInZoomControls(true);

	         web.requestFocusFromTouch();

	         web.setWebViewClient(new WebViewClient());
	         web.setWebChromeClient(new WebChromeClient());  
	        
	         web.loadUrl("file:///android_asset/jsonmap.html");
	      		
   		
	         //Open MAnage Users window
   	       colorIndex = 0;
           listView = (ListView) findViewById(R.id.list1);
      	             
           SwipeListView l = new SwipeListView(this, this);
   		   l.exec();
   		
           String message = "";
           JSONObject reader;
           
		try {
			 reader = new JSONObject(userData);
			 message  = reader.getString("Message");
			  
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		txtMessage=(TextView) findViewById(R.id.txtMessage);
		txtInvite=(TextView) findViewById(R.id.txtInvite);
		
		if ( message.equalsIgnoreCase("Success"))
		{
			listView.setVisibility(View.VISIBLE);
			txtMessage.setVisibility(View.GONE);
			txtInvite.setVisibility(View.GONE);
			
  	       XMLPullParserHandler parser = new XMLPullParserHandler();
      	   onlineDataList = parser.parseJSON(userData);
      	   
	      	dataArrayAdapter = new DataArrayAdapter(MainMapActivity1.this, R.layout.listview_row_layout);
	   		listView.setAdapter(dataArrayAdapter);
   		
		}
		else
		{
		   listView.setVisibility(View.GONE);	
		   txtMessage.setVisibility(View.VISIBLE);
		   txtInvite.setVisibility(View.VISIBLE);
		   
		    
				 txtInvite.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i =new Intent();
						i.setClass(getApplicationContext(), AddCaregiverContactsActivity.class);
						i.putExtra("comeFrom", "caregiver");
						startActivity(i);
						
					}
				});
				   
		}
      	            
      	   //Toast.makeText(MainActivity.this, " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
      		
      	  	  for (OnlineData onlineData:onlineDataList)
     		  {
      	  		  
      	  		int id=onlineData.getId();
          	    String image = onlineData.getName();
                String name = onlineData.getName();
                String text = "Status :" ;
	            
                cntUser=cntUser+1;
                
	             if (onlineData.getStatus().equalsIgnoreCase("online"))
	            	 text = text +"<font color='green'>"+ onlineData.getStatus()+"</font>";
	             else 
	            	 text = text +"<font color='red'>"+ onlineData.getStatus()+"</font>";
	             
	        	 text = text +"     Location : "+onlineData.getLocation();
	             String status =  text;
	             
             //   String status = "Status : "+ onlineData.getStatus()+"     Location : "+onlineData.getLocation();
                String  test =onlineData.getId()+"!"+onlineData.getLatitude()+"!"+onlineData.getLongitude();
                
                String imageListView=onlineData.getImage(); 
                
               int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
                
               ServiceData serviceData = new ServiceData(imgResId,name,status,test,imageListView);
           
               dataArrayAdapter.add(serviceData);
               
     		}
      	  	
      	  	TextView txtCircle;
      		txtCircle=(TextView)findViewById(R.id.txtCircle);
      		
      	  	txtCircle.setText(String.valueOf(cntUser));
      	 
        	String fullName=  settings.getString("fullName", "0");
        	TextView txtName=(TextView) findViewById(R.id.txtName);
        	TextView txtUser=(TextView) findViewById(R.id.txtUser);
        	
        	txtName.setText(fullName);
        	txtUser.setText("Caregiver for "+cntUser+" Users");
        	
        	ImageView imgPicture;
        	
        	imgPicture =(ImageView)findViewById(R.id.imgPicture);
        	
        			
        			   if( (imageProfile!= null) && (imageProfile.length()>0))
        	    		{
        	    			Picasso.with(getBaseContext()).load(imageProfile).into(imgPicture);	
        	    		}
        	    
        	
           	 listView.setOnItemClickListener(new OnItemClickListener() {
          		
     			  public void onItemClick(AdapterView<?> arg0,
     			  View arg1, int position, long arg3)
     			  { 
     				  DataViewHolder viewHolder;
     				  
     				  viewHolder=(DataViewHolder)arg1.getTag();
     				  
     				  
     				  String name = ((TextView)arg1.findViewById(R.id.name)).getText().toString();
     		          String test = ((TextView)arg1.findViewById(R.id.test)).getText().toString();

     		          Intent intent =new Intent();
     				  intent.setClass(getApplicationContext(),PatientDetailActivity.class);
     				  
     				  intent.putExtra("name",name);
     				  intent.putExtra("LatLog",test);
     				  
     				  startActivity(intent);
     			  }
     	 });

           	setClickEvent();
    }
     
  
    
    @Override
	public ListView getListView() {
		// TODO Auto-generated method stub
		return listView;
	}

	@Override
	public void onSwipeItem(boolean isRight, int position) {
		// TODO Auto-generated method stub
	//	Toast.makeText(getApplicationContext(), "OnitemSwipe "+isRight, Toast.LENGTH_LONG).show();
		dataArrayAdapter.onSwipeItem(isRight, position);
	}

	@Override
	public void onItemClickListener(ListAdapter adapter, int position) {
		// TODO Auto-generated method stub

	}

	
    private void setClickEvent()
    {
     	 
    	TextView  txtManageUsers;
       txtManageUsers=(TextView)slide_me.findViewById(R.id.txtUsers);
		txtManageUsers.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (cntUser>0)
				{
					Intent intent=new Intent();
					intent.setClass(getApplicationContext(), ManageUsersActivity.class);
					startActivity(intent);
				}
			}
		});
		  
		  //Open My Subscription window
		TextView  txtSub;
		txtSub=(TextView)slide_me.findViewById(R.id.txtSub);
		  
		 txtSub.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),SubscriptionActivity.class);
				startActivity(intent);
			}
		});
  		
		 
		 //open My Account window
		   TextView  txtAccount;
		  txtAccount=(TextView)slide_me.findViewById(R.id.txtAccount);
		  
		  txtAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),MyAccountActivity.class);
				startActivity(intent);
			}
		});
		  
		  
		
    
		txtUsers=(TextView) findViewById(R.id.txtUsers);
     	
     	txtUsers.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (cntUser>0)
				{
					Intent  intent=new Intent();
					intent.setClass(getApplicationContext(), ManageUsersActivity.class);
					startActivity(intent);
					
					
				}
				
			}
		});
     	  
    }


    
    
    public class DataArrayAdapter extends ArrayAdapter<ServiceData> {
   	 Context mcontext;
       private static final String TAG = "DataArrayyAdapter";
       private final int INVALID = -1;
		protected int DELETE_POS = -1;

   	private List<ServiceData> dataList = new ArrayList<ServiceData>();

   	private LruCache<String, Bitmap> mMemoryCache;
   	 private Context ctx;
      // ImageLoader imageLoader = new ImageLoader(this);
       Context mContext;
       private int action_down_x = 0;
       private int action_up_x = 0;
       private int difference = 0;
       
       public  class DataViewHolder {
           ImageView image;
           TextView name;
           TextView status;
        //   ImageView rightImg;
          TextView test;
   		
          ImageView btn_remove;
          
       }

     
       
       public DataArrayAdapter(Context context, int textViewResourceId) {
       	 super(context, textViewResourceId);
       	 
       	// Get memory class of this device, exceeding this amount will throw an
           // OutOfMemory exception.
           final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    
           // Use 1/8th of the available memory for this memory cache.
           final int cacheSize = maxMemory / 8;
    
           mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
    
               @SuppressLint("NewApi") protected int sizeOf(String key, Bitmap bitmap) {
                   // The cache size will be measured in bytes rather than number
                   // of items.
                   return bitmap.getByteCount();
               }
    
           };
           
          
       }

       
   	@Override
   	public void add(ServiceData object) {
   		dataList.add(object);
   		super.add(object);
   	}
   	
   	
   	
   	/*public void add1(UserData object) {
   		dataList.add1(object);
   		super.add1(object);
   	}*/

   	@Override
   	public int getCount() {
   		return this.dataList.size();
   	}

   	public void onSwipeItem(boolean isRight, int position) {
		// TODO Auto-generated method stub
   		
   		Toast.makeText(getApplicationContext(), "OnitemSwipe "+isRight, Toast.LENGTH_LONG).show();
   		
		if (isRight == false) {
			DELETE_POS = position;
		} else if (DELETE_POS == position) {
			DELETE_POS = INVALID;
		}
		//
		notifyDataSetChanged();
	
		Toast.makeText(getApplicationContext(), "DELETE_POS=== "+DELETE_POS , Toast.LENGTH_LONG).show();
		
		
	}
   	
   	
   	public void deleteItem(int pos) {
		//
		//m_List.remove(pos);
		DELETE_POS = INVALID;
		notifyDataSetChanged();
	}

   	
       @Override
   	public ServiceData getItem(int index) {
   		return this.dataList.get(index);
   	}
       
       
    
   	@Override
   	public View getView(final int position, View convertView, ViewGroup parent) {
   		View row = convertView;
           DataViewHolder viewHolder;
          // Activity cntx = new Activity();
           
           
        // TODO Auto-generated method stub
        			if (convertView == null) {
        				convertView = LayoutInflater.from(MainMapActivity1.this).inflate(
        						R.layout.listview_main_map, null);
        			}
        			
        			ImageView image = ViewHolderPattern.get(convertView, R.id.image);
        			TextView name = ViewHolderPattern.get(convertView, R.id.name);
        			TextView status = ViewHolderPattern.get(convertView, R.id.status);
        		/*	ImageView btn_remove = ViewHolderPattern.get(convertView, R.id.btn_remove);*/
        			
        			Toast.makeText(getApplicationContext(), " DELETE_POS=== "+DELETE_POS +" position ==="+position, Toast.LENGTH_LONG).show();
        			
        			/*if (DELETE_POS == position) {
        				//btn_remove.setVisibility(View.VISIBLE);
        			} else
        				//btn_remove.setVisibility(View.GONE);
*/        		/*	btn_remove.setOnClickListener(new View.OnClickListener() {

        				@Override
        				public void onClick(View v) {
        					// TODO Auto-generated method stub
        					deleteItem(position);
        				}
        			});
*/
        			ServiceData serviceData = getItem(position);
        			
        			name.setText(serviceData.getName());
        			//status.setText(serviceData.getStatus());

        			status.setText(Html.fromHtml(serviceData.getStatus()));
        			
        			if( (serviceData.getImageListView()!= null) && (serviceData.getImageListView().length()>0))
               		{
               			Picasso.with(getContext()).load(serviceData.getImageListView()).into(image);	
               		}
        			
        			return convertView;
        			
   	/*	if (row == null) {
   			
   			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
   			
   		
   			row = inflater.inflate(R.layout.listview_main_map, parent, false);
   			
               viewHolder = new DataViewHolder();
               
               
               viewHolder.image = (ImageView) row.findViewById(R.id.image);
               viewHolder.name = (TextView) row.findViewById(R.id.name);
               viewHolder.status = (TextView) row.findViewById(R.id.status);
         
               viewHolder.test=(TextView) row.findViewById(R.id.test);
               viewHolder.btn_remove=(ImageView) row.findViewById(R.id.btn_remove);
               
               Toast.makeText(getApplicationContext(), "DELETE_POS--"+DELETE_POS +" position = "+position, Toast.LENGTH_LONG).show();
               
             //  Button delete = ViewHolderPattern.get(convertView, R.id.delete);
   			if (DELETE_POS == position) {
   				viewHolder.btn_remove.setVisibility(View.VISIBLE);
   			} else
   				viewHolder.btn_remove.setVisibility(View.GONE);
   			
   			viewHolder.btn_remove.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//deleteItem(position);
					Toast.makeText(getApplicationContext(), "Delete click", Toast.LENGTH_LONG).show();
				}
			});
            
               row.setTag(viewHolder);
               
             
               
   		} else {
   			
               viewHolder = (DataViewHolder)row.getTag();
               
           }
   		
   		
   		
           
   		
   		ServiceData serviceData = getItem(position);
   		
   		
   	
           
           viewHolder.name.setText(serviceData.getName());
           viewHolder.status.setText(Html.fromHtml(serviceData.getStatus()));
           viewHolder.test.setText(serviceData.getTest());
           
           
           System.out.println("serviceData.getImageListView() === "+serviceData.getImageListView()+" length " +serviceData.getImageListView().length());
           
           System.out.println("serviceData url"+serviceData.getImageListView());
           
           if( (serviceData.getImageListView()!= null) && (serviceData.getImageListView().length()>0))
           		{
           			Picasso.with(getContext()).load(serviceData.getImageListView()).into(viewHolder.image);	
           		}
           
         
           
   		return row;*/
   	}

 
    
   		public Bitmap decodeFile(Context context,int resId) {
   						try {
   							// decode image size
   							mcontext=context;
   							BitmapFactory.Options o = new BitmapFactory.Options();
   							o.inJustDecodeBounds = true;
   							BitmapFactory.decodeResource(mcontext.getResources(), resId, o);
   							// Find the correct scale value. It should be the power of 2.
   							final int REQUIRED_SIZE = 200;
   							int width_tmp = o.outWidth, height_tmp = o.outHeight;
   							int scale = 1;
   							while (true)
   							{
   							 if (width_tmp / 2 < REQUIRED_SIZE
   							 || height_tmp / 2 < REQUIRED_SIZE)
   							 break;
   							 width_tmp /= 2;
   							 height_tmp /= 2;
   							 scale++;
   						}
   						// decode with inSampleSize
   						BitmapFactory.Options o2 = new BitmapFactory.Options();
   						o2.inSampleSize = scale;
   						return BitmapFactory.decodeResource(mcontext.getResources(), resId, o2);
   						} catch (Exception e) {
   				}
   				return null;
   		}



       public Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width) {
      	 // TODO Auto-generated method stub
      	 int targetWidth = width;
      	 int targetHeight = width;
      	 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
      	 targetHeight,Bitmap.Config.ARGB_8888);

      	 Canvas canvas = new Canvas(targetBitmap);
      	 Path path = new Path();
      	 path.addCircle(((float) targetWidth - 1) / 2,
      	 ((float) targetHeight - 1) / 2,
      	 (Math.min(((float) targetWidth),
      	 ((float) targetHeight)) / 2),
      	 Path.Direction.CCW);
      	 canvas.clipPath(path);
      	 Bitmap sourceBitmap = scaleBitmapImage;
      	 canvas.drawBitmap(sourceBitmap,
      	 new Rect(0, 0, sourceBitmap.getWidth(),
      	 sourceBitmap.getHeight()),
      	 new Rect(0, 0, targetWidth,
      	 targetHeight), null);
      	 return targetBitmap;
      	 }
      	
       

       public Bitmap decodeToBitmap(byte[] decodedByte) {
   		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
   	}
   }
  
  	public static class ViewHolderPattern {
		// I added a generic return type to reduce the casting noise in client
		// code
		@SuppressWarnings("unchecked")
		public static <T extends View> T get(View view, int id) {
			SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
			if (viewHolder == null) {
				viewHolder = new SparseArray<View>();
				view.setTag(viewHolder);
			}
			View childView = viewHolder.get(id);
			if (childView == null) {
				childView = view.findViewById(id);
				viewHolder.put(id, childView);
			}
			return (T) childView;
		}
	}
       
 
}
