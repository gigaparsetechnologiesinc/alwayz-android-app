package com.always.activity;

import com.example.always.R;
import com.example.always.R.id;
import com.example.always.R.layout;
import com.example.always.R.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		
				  /****** Create Thread that will sleep for 5 seconds *************/        
		        Thread background = new Thread() {
		            public void run() {
		                 
		                try {
		                    // Thread will sleep for 5 seconds
		                    sleep(5*1000);
		                     
		                    // After 5 seconds redirect to another intent
		                    Intent i=new Intent(getBaseContext(),LoginActivity.class);
		                    startActivity(i);
		                     
		                    //Remove activity
		                    finish();
		                     
		                } catch (Exception e) {
		                 
		                }
		            }
		        };
		         
		        // start thread
		        background.start();
		         
		//METHOD 2  
		         
		        /*
		        new Handler().postDelayed(new Runnable() {
		              
		            // Using handler with postDelayed called runnable run method
		  
		            @Override
		            public void run() {
		                Intent i = new Intent(MainSplashScreen.this, FirstScreen.class);
		                startActivity(i);
		  
		                // close this activity
		                finish();
		            }
		        }, 5*1000); // wait for 5 seconds
		        */
		
	}
	
		    @Override
		    protected void onDestroy() {
		         
		        super.onDestroy();
		         
		    }
		  
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}


}
