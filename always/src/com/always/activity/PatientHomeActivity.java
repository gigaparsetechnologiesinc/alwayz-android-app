package com.always.activity;


import org.json.JSONException;
import org.json.JSONObject;

import com.always.common.Utility;
import com.example.always.R;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.content.Context;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class PatientHomeActivity extends Activity  {

	TextView txtCall;
	ImageView imgProfile;
	TextView txtConfirmMsg1;
	ImageView imgCall;
	TextView txtAdd;
	TextView txtLogout;
	 Double latitude;
	 Double longitude;
	 
	 int setLocationFlag=0;
	
	private int FIVE_SECONDS = 5000;
	private int TEN_SECONDS=10000;
	Handler handler = new Handler();
	
	private Handler myHandler;
	private Runnable myRunnable ;
	
	private static final String TAG = PatientHomeActivity.class.getSimpleName();
	
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
	
	private Location mLastLocation;
	//private GoogleApiClient mGoogleApiClient;
	private boolean mRequestingLocationUpdates = false;
	//private LocationRequest mLocationRequest;
	private static int UPDATE_INTERVAL = 10000; // 10 sec
	private static int FATEST_INTERVAL = 5000; // 5 sec
	private static int DISPLACEMENT = 10; // 10 meters
	
	String location="";
	
	 TextView marqueeText;
	public static final String MY_EMP_PREFS = "MySharedPref";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_home);
		
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		final SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
	
		txtLogout=(TextView)findViewById(R.id.txtLogout);
		
		
		txtCall=(TextView) findViewById(R.id.txtCall);
		imgProfile =(ImageView) findViewById(R.id.imgProfile1);
		txtConfirmMsg1=(TextView) findViewById(R.id.txtConfirmMsg1);
		imgCall=(ImageView)findViewById(R.id.imgCall);
		
		txtAdd=(TextView)findViewById(R.id.txtAdd);	
		
		txtConfirmMsg1.setText(settings.getString("caregiverName", ""));
		
		txtAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), AddCaregiver2Activity.class);
				startActivity(intent);*/
				
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(), AddCaregiverContactsActivity.class);
				intent.putExtra("comeFrom", "user");
				startActivity(intent);
				
				
			}
		});
		
		final String phoneNo= settings.getString("phoneNo", "");
		
		System.out.println("Uri.parse(phoneNo)=="+phoneNo);
		
		 final AlertDialog.Builder builderMain = new AlertDialog.Builder(this);
		 
		txtCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String phoneNo= settings.getString("phoneNo", "");
			
				builderMain.setMessage("Are you sure you want to call "+settings.getString("caregiverName", "")+" ? ");

				builderMain.setPositiveButton("YES", new DialogInterface.OnClickListener() {

			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing but close the dialog
			        	Intent callIntent = new Intent(Intent.ACTION_CALL);
	                	callIntent.setData(Uri.parse(phoneNo));
	                	startActivity(callIntent);
			            
			        }

			    });

				builderMain.setNegativeButton("NO", new DialogInterface.OnClickListener() {

			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing
			            dialog.dismiss();
			        }
			    });

			    AlertDialog alert = builderMain.create();
			    alert.show();
				  
				
			}
		});
		
		
		
		
		imgCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				builderMain.setMessage("Are you sure you want to call "+settings.getString("caregiverName", "")+" ? ");

				builderMain.setPositiveButton("YES", new DialogInterface.OnClickListener() {

			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing but close the dialog
			        	Intent callIntent = new Intent(Intent.ACTION_CALL);
	                	callIntent.setData(Uri.parse(phoneNo));
	                	startActivity(callIntent);
			            
			        }

			    });

				builderMain.setNegativeButton("NO", new DialogInterface.OnClickListener() {

			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing
			            dialog.dismiss();
			        }
			    });

			    AlertDialog alert = builderMain.create();
			    alert.show();
				  
				
				
			}
		});
		

		final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		txtLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				builder1.setTitle("Logout");
				builder1.setMessage("Do you want to logout?");

				builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			        public void onClick(DialogInterface dialog, int which) {
			           
			        	// handler.removeCallbacksAndMessages(null);
			        	 logout();
			        }

			    });

				builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing
			        	dialog.dismiss();			        }
			    });

			    AlertDialog alert = builder1.create();
			    alert.show();
			    
			
				   /*final CharSequence[] items = {
			                 "Logout", "Cancel"
			        };

			      
				   AlertDialog.Builder builder3=new AlertDialog.Builder(PatientHomeActivity.this);
				   builder3.setTitle("Logout").setItems(items, new DialogInterface.OnClickListener()
				   {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
					   // TODO Auto-generated method stub
					   Toast.makeText(getApplicationContext(), "U clicked "+items[which], Toast.LENGTH_LONG).show();
					   
					   String options=items[which].toString(); 
					   
							   if (options.equalsIgnoreCase("Logout"))
							   {
								   logout();
							   
							   }
							   else if (options.equalsIgnoreCase("Cancel"))
							   {
								   dialog.cancel();
							   }
					   
					   
						   
					   }
				   });
				   builder3.show();*/
			}
		});
		
		
		
	/*	
		if (checkPlayServices()) {

		    buildGoogleApiClient();
		}
		
		scheduleSendLocation();*/
		
	
	        
	      /*  marqueeText=(TextView)findViewById(R.id.marquee_text);
	        marqueeText.setText("Set Location");
	        marqueeText.setSelected(true);
	        marqueeText.setTypeface(null, Typeface.BOLD);
	        marqueeText.setSingleLine();
	        marqueeText.setEllipsize(TruncateAt.MARQUEE);
	        marqueeText.setHorizontallyScrolling(true);*/
		 
		    marqueeText=(TextView)findViewById(R.id.marquee_text);
		    
		    if (setLocationFlag==0)
		    	marqueeText.setText("Set Location................................");
		    else
		    	marqueeText.setText("Update Location................................");
		    
		    
	        marqueeText.setSelected(true);
	        marqueeText.setTypeface(null, Typeface.BOLD);
	        marqueeText.setSingleLine();
	        marqueeText.setEllipsize(TruncateAt.MARQUEE);
	        marqueeText.setHorizontallyScrolling(true);
	        
	        
	        marqueeText.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setLocationFlag=setLocationFlag+1;
					displayLocation("onDemand");
					//saveLatLon("onDemand", latitude, longitude)
					
					 if (setLocationFlag==0)
					    	marqueeText.setText("Set Location................................");
					    else
					    	marqueeText.setText("Update Location................................");
					    
				}
			});
	        
	        
	     //  uncomment when used with location
	       
	        
	        handler.postDelayed(new Runnable() {
	            public void run() {
	                displayLocation("continue");          // this method will contain your almost-finished HTTP calls
	                handler.postDelayed(this, TEN_SECONDS);
	            }
	        }, TEN_SECONDS);
	     
		     
            //if (gpsLocation != null) {
            	// latitude = 37.422005;
            /*     //longitude = -122.084095;
            	LocationAddress locationAddress = new LocationAddress();
            	//Toast.makeText(getApplicationContext(),"Location \nLatitude: " + latitude+ "\nLongitude: " + longitude,Toast.LENGTH_LONG).show();
                locationAddress.getAddressFromLocation(latitude, longitude,getApplicationContext(), new GeocoderHandler());*/
                
                
                
}

	
	public void displayLocation(String setLatLon)
	{
            AppLocationService appLocationService;
			appLocationService = new AppLocationService(PatientHomeActivity.this);
			
			Location gpsLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);

			if (gpsLocation != null) {
				 latitude = gpsLocation.getLatitude();
				 longitude = gpsLocation.getLongitude();
				Toast.makeText(getApplicationContext(),"Mobile Location (NETWORK_PROVIDER): \nLatitude: " + latitude+ "\nLongitude: " + longitude,Toast.LENGTH_LONG).show();
				
				saveLatLon(setLatLon,latitude, longitude);
				
			} else {
				showSettingsAlert("NETWORK");
			}
			
	}
	
	private void saveLatLon(String setLatLon, double latitude,double longitude)
	{
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		final String	userId=  settings.getString("id", "0");
		
		
		Utility utility=new Utility();
		String userData="";
		
		if (setLatLon.equalsIgnoreCase("continue"))
			{
				userData=utility.serviceCall(108,"S"+"!"+userId+"!"+latitude+"!"+longitude);
			}
		else if (setLatLon.equalsIgnoreCase("onDemand"))
		{
			userData=utility.serviceCall(107,userId+"!"+latitude+"!"+longitude);
		}
		
		System.out.println("108 result "+ userData);
		
		try {
	  		
		  		String message="";
				JSONObject jsonResponse = new JSONObject(userData);
				
				System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
				message=jsonResponse.optString("Message");
				
				/*if (message.equalsIgnoreCase("Success"))
				{
					                						
					Toast.makeText(getApplicationContext(), "Password changed Successfully.", Toast.LENGTH_LONG).show();
				}
				else
				{
					
					Toast.makeText(getApplicationContext(), "Password Not changed", Toast.LENGTH_LONG).show();;
				}*/
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void showSettingsAlert(String provider) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				PatientHomeActivity.this);

		alertDialog.setTitle(provider + " SETTINGS");

		alertDialog
				.setMessage(provider + " is not enabled! Want to go to settings menu?");

		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						PatientHomeActivity.this.startActivity(intent);
					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}
	
	
private void logout()
	{
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		String 	userId=  settings.getString("id", "0");
    	
		
		handler.removeCallbacksAndMessages(null);
		
	 /*   handler.removeCallbacks(runnable);
		handler.interrupt();
		handler = null;*/
		
		
		Utility utility=new Utility();
		utility.serviceCall(112, userId);
		
		Intent intent =new Intent();
		intent.setClass(getApplicationContext(), SplashActivity.class);
		startActivity(intent);
	}





/**
* Method to display the location on UI
* *//*
private void displayLocation() {

	//Toast.makeText(getApplication(), "displayLocation", Toast.LENGTH_LONG).show();
	
mLastLocation = LocationServices.FusedLocationApi
        .getLastLocation(mGoogleApiClient);

	if (mLastLocation != null) {
	    double latitude = mLastLocation.getLatitude();
	    double longitude = mLastLocation.getLongitude();
	
	    location=location+"  "+latitude + ", " + longitude;
	    
	    Toast.makeText(getApplication()," location  "+location, Toast.LENGTH_LONG).show();
	    
	    
	    Utility utility=new Utility();
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		
    	String cid=  settings.getString("id", "0");
    	
		String userData=utility.serviceCall(108,cid+"!"+latitude+"!"+longitude);
		
	    //Toast.makeText(getApplication()," location  "+latitude + ",longitude " + longitude, Toast.LENGTH_LONG).show();
	  //  lblLocation.setText(location);
	
	} else {
	
	   // lblLocation.setText("(Couldn't get the location. Make sure location is enabled on the device)");
	}
}

*//**
* Creating google api client object
* *//*
protected synchronized void buildGoogleApiClient() {
mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API).build();
}

*//**
* Method to verify google play services on the device
* *//*
private boolean checkPlayServices() {
int resultCode = GooglePlayServicesUtil
        .isGooglePlayServicesAvailable(this);
if (resultCode != ConnectionResult .SUCCESS) {
    if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
        GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                PLAY_SERVICES_RESOLUTION_REQUEST).show();
    } else {
        Toast.makeText(getApplicationContext(),
                "This device is not supported.", Toast.LENGTH_LONG)
                .show();
        finish();
    }
    return false;
}
return true;
}

@Override
protected void onStart() {
super.onStart();
if (mGoogleApiClient != null) {
    mGoogleApiClient.connect();
}
}

@Override
protected void onResume() {
super.onResume();

checkPlayServices();
}

*//**
* Google api callback methods
*//*
@Override
public void onConnectionFailed(ConnectionResult result) {
Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
        + result.getErrorCode());
}

@Override
public void onConnected(Bundle arg0) {

// Once connected with google api, get the location
displayLocation();
}

@Override
public void onConnectionSuspended(int arg0) {
mGoogleApiClient.connect();
}



public void scheduleSendLocation() {
    handler.postDelayed(new Runnable() {
        public void run() {
            displayLocation();          // this method will contain your almost-finished HTTP calls
            handler.postDelayed(this, FIVE_SECONDS);
        }
    }, FIVE_SECONDS);

}

@Override
protected void onStop() {
	// TODO Auto-generated method stub
	super.onStop();
	
}


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	*/
	
	
}
