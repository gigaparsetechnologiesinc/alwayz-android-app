package com.always.activity;


import com.always.common.Utility;
import com.example.always.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AccountSetUpSmallActivity extends Activity {

	ImageView imgProfile1;
	TextView txtNextS;
	EditText editTxtFullNameS;
	EditText editTxtEmailS;
	EditText editPhoneS;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_set_up_small);
		
		imgProfile1=(ImageView) findViewById(R.id.imgProfile1);
		imgProfile1.setImageResource(R.drawable.person);
		
		
		editTxtFullNameS=(EditText)findViewById(R.id.editTxtFullNameS);
		editTxtEmailS=(EditText) findViewById(R.id.editTxtEmailS);
		editPhoneS=(EditText) findViewById(R.id.editPhoneS);
		txtNextS=(TextView) findViewById(R.id.txtNextS);
		
		imgProfile1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
		
		          Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);
			}
		});
		

				
				txtNextS.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					
						
						if(Utility.isNotNull(editTxtEmailS.getText().toString()) &&  Utility.isNotNull(editTxtFullNameS.getText().toString()) 
								&&  Utility.isNotNull(editPhoneS.getText().toString())  )
						{
							
							
							
							if(Utility.validate(editTxtEmailS.getText().toString().trim())){
								
						
							SharedPreferences.Editor editor1 = getSharedPreferences("Session", MODE_PRIVATE).edit();
							editor1.putString("CaregiverFullName", editTxtFullNameS.getText().toString());
							editor1.putString("CaregiverUserEmail", editTxtEmailS.getText().toString());
							editor1.putString("CaregiverUserPhone",editPhoneS.getText().toString());
							editor1.commit();
							
	
							Intent intent =new Intent();
							intent.setClass(getApplicationContext(), MainActivity.class);
							startActivity(intent);
							}
							 else{
					             Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
					         }
						  }
						else{
					          Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
					      }
						
						
					}
				});
				
				
	
		
			
		}
		
	 @Override

	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	        super.onActivityResult(requestCode, resultCode, data);

	        if (resultCode == RESULT_OK) {

	      if (requestCode == 2) {

	 

	                Uri selectedImage = data.getData();

	                String[] filePath = { MediaStore.Images.Media.DATA };

	                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

	                c.moveToFirst();

	                int columnIndex = c.getColumnIndex(filePath[0]);

	                String picturePath = c.getString(columnIndex);

	                c.close();

	                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

	                Log.w("path of image from gallery......******************......;...", picturePath+"");

	                ImageView viewImage;
	                viewImage=(ImageView) findViewById(R.id.imgProfile1);
					viewImage.setImageBitmap(thumbnail);

	            }

	        }
	 }

}
