package com.always.activity;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.always.adapter.PatientArrayAdapter;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.example.always.R;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class PatientHomeAltActivity extends Activity {
	private int TEN_SECONDS=10000;
	Handler handler = new Handler();
	 Double latitude;
	 Double longitude;
	public  List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	 private static final String TAG = "ListViewActivity";

	    private PatientArrayAdapter patientArrayAdapter;
		private ListView listView;

		private static int colorIndex;
		
		 static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 public static final String MY_EMP_PREFS = "MySharedPref";
		 final Context context = this;
		//   List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_home_alt);
		
		
		colorIndex = 0;
  		listView = (ListView) findViewById(R.id.listViewPatient);
  		
  		
  		ImageView imgAddCaregivers=(ImageView) findViewById(R.id.imgAddCaregivers);
  		
  		imgAddCaregivers.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(), AddCaregiverContactsActivity.class);
				intent.putExtra("comeFrom", "user");
				startActivity(intent);
				
			}
		});
  		
  		
  		 //final PatientHomeActivity patientHomeActivity=new PatientHomeActivity ();
  		 
  		 	handler.postDelayed(new Runnable() {
            public void run() {
            	
	            	displayLocation("continue");          // this method will contain your almost-finished HTTP calls
	            	handler.postDelayed(this, TEN_SECONDS);
	            	
	            }
	        }, TEN_SECONDS);
     
        
  		
  		TextView txtOptions;
  		
  		txtOptions=(TextView)findViewById(R.id.txtOptions);
  		
  		txtOptions.setOnClickListener(new OnClickListener() {
			
  		   
			   final CharSequence[] items = {
		                "Logout", "Set Home Location", "Change Password","Cancel"
		        };

			   
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				   AlertDialog.Builder builder3=new AlertDialog.Builder(PatientHomeAltActivity.this);
				   builder3.setTitle("Options").setItems(items, new DialogInterface.OnClickListener()
				   {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
					   // TODO Auto-generated method stub
						 //  Toast.makeText(getApplicationContext(), "U clicked "+which+ " " +items[which], Toast.LENGTH_LONG).show();
						   
						   if (which==0)//used tor logout
						   { 
							  logout();   
						    }
						   else  if (which==1)//used for set home location
						   {
							   setHomeLocation();
						   }
						   else  if (which==2)//used for change password
						   {
							   updatePassword();
						   }
						   else  if (which==2)//used for change password
						   {
							   finish();
						   }
					   }
					   
				   });
				   builder3.show();
			}
		});
  		
  		/*final SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
  	           
  		int size = settings.getInt("caregiverArr" + "_size", 0);  
  	
  		Toast.makeText(getApplicationContext(), "email "+settings.getString("email", "") , Toast.LENGTH_LONG).show();
  		Toast.makeText(getApplicationContext(), "password "+settings.getString("password", "") , Toast.LENGTH_LONG).show();
  	   */
  	 
  		final SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
  		
    	Utility utility=new Utility();
    	
		String userData=utility.serviceCall(100,settings.getString("email", "")+"!"+settings.getString("password", ""));
	  	System.out.println("reader111=="+userData);
		JSONObject reader;
		try {
			reader = new JSONObject(userData);
		
		String message  = reader.getString("Message");	
		
		
		 if (message.equals("Success"))
            {
			
			   JSONObject user  = reader.getJSONObject("user");
				String userType = user.getString("userType");
				
			    System.out.println("reader.getString(cargiver) "+reader.getString("cargiver"));
	            
	            if ((reader.getString("cargiver") !=null) && (reader.getString("cargiver") !="null") && (reader.getString("cargiver") !=""))
	            {
		            JSONArray caregiverArr  = reader.getJSONArray("cargiver");
		             for(int i=0; i<caregiverArr.length(); i++){
		                JSONObject json_data = caregiverArr.getJSONObject(i);
		                
		                OnlineData  onlineData=new OnlineData();
		                
		                //Toast.makeText(getApplicationContext(), "loop " +json_data.getString("fullName"), Toast.LENGTH_LONG).show();
		                
		                onlineData.setName( json_data.getString("fullName"));
		                onlineData.setImage( json_data.getString("image"));
		                onlineData.setPhone( json_data.getString("phone"));
		                
		                onlineDataList.add(onlineData);
		                
		               
		            }
		            
		       
	            }
	            
	        	
            }
		 
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  
		//Toast.makeText(getApplicationContext(), "onlineDataList size "+onlineDataList.size(),Toast.LENGTH_LONG).show();
		
  	    //  List<OnlineData> onlineDataList = new ArrayList<OnlineData>(); 
  	  
		  	  /*try {
		  		  
		  		onlineDataList = (ArrayList<OnlineData>) ObjectSerializer.deserialize(settings.getString("caregiverList",
		  				ObjectSerializer.serialize(new ArrayList<OnlineData>()))); 
		  		
		  		//onlineDataList = (ArrayList<OnlineData>) ObjectSerializer.deserialize(settings.getString(onlineDataList,
		        	//	  ObjectSerializer.serialize(new ArrayList<OnlineData>())));
		          
		      } catch (IOException e) {
		          e.printStackTrace();
		      }
  	  
		  	  
		  	Toast.makeText(getApplicationContext(), "sizeeee "+onlineDataList.size(), Toast.LENGTH_LONG).show();
  	    */
  	    patientArrayAdapter = new PatientArrayAdapter(getApplicationContext(), R.layout.list_row_patient_alt);
  		listView.setAdapter(patientArrayAdapter);
          
  	  for (OnlineData onlineData:onlineDataList)
 		{
      	
      	    String image = onlineData.getImage();
            String name = onlineData.getName();
            String phone =onlineData.getPhone();
            
            //Toast.makeText(getApplicationContext(), "image "+image, Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), "name "+name, Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), "phone "+phone, Toast.LENGTH_LONG).show();
            
            String status = onlineData.getStatus();
         //   String hidData=onlineData.getLatitude()+"$"+onlineData.getLongitude();
            
            int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
            
            
         /*   if( (onlineData.getImage()!= null) && (onlineData.getImage().length()>0))
    		{
    			Picasso.with(getContext()).load(onlineData.getImage()).into(viewHolder.image);	
    		}
    */
            
           ServiceData serviceData = new ServiceData(imgResId,name,status,status);//, hidData);
           patientArrayAdapter.add(serviceData);
            
    	 //  array_sort.add(onlineData.getName());
        // Toast.makeText(getApplicationContext(), " onlineData.getName() ="+ onlineData.getName(), Toast.LENGTH_LONG).show();
    	   //array_sort1.add(onlineData.getLocation());
 		}
      
  	final AlertDialog.Builder builderMain = new AlertDialog.Builder(this);
  	
		      
		  	 listView.setOnItemClickListener(new OnItemClickListener() {
		 		
				  public void onItemClick(AdapterView<?> arg0,
				  View arg1, int position, long arg3)
				  {
					 // Toast.makeText(getApplicationContext(), "click "+position, Toast.LENGTH_LONG).show();
				
					  final OnlineData item = onlineDataList.get(position);
					  
						builderMain.setTitle("Call Caregiver");
        				builderMain.setMessage("Would you like to call "+item.getName()+" ?");

        				builderMain.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

        			        public void onClick(DialogInterface dialog, int which) {
        			            // Do nothing but close the dialog
        			        	
        			        	String phone="tel:+"+item.getPhone();
        			        	
        			        	Intent callIntent = new Intent(Intent.ACTION_CALL);
        	                	callIntent.setData(Uri.parse(phone));
        	                	startActivity(callIntent);
        			            
        			        }

        			    });

        				builderMain.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {

        			        @Override
        			        public void onClick(DialogInterface dialog, int which) {
        			            // Do nothing
        			            dialog.dismiss();
        			        }
        			    });

        			    AlertDialog alert = builderMain.create();
        			    alert.show();
        			    
        			    
			/*		  Intent intent =new Intent();
					  intent.setClass(getApplicationContext(),PatientDetailActivity.class);
					  startActivity(intent);*/
				  }
		 });
		  	 
  	 
	}

	private void setHomeLocation()
	{
		
	//PatientHomeActivity patientHomeActivity =new PatientHomeActivity();
		displayLocation("onDemand");
		
/*		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		String  userId=  settings.getString("id", "0");
    	
		
		Utility utility=new Utility();
		utility.serviceCall(107, userId);*/
	}

	
	
	public void displayLocation(String setLatLon)
	{
            AppLocationService appLocationService;
			appLocationService = new AppLocationService(PatientHomeAltActivity.this);
			
			Location gpsLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);

			if (gpsLocation != null) {
				 latitude = gpsLocation.getLatitude();
				 longitude = gpsLocation.getLongitude();
				Toast.makeText(getApplicationContext(),"Mobile Location (NETWORK_PROVIDER): \nLatitude: " + latitude+ "\nLongitude: " + longitude,Toast.LENGTH_LONG).show();
				
				saveLatLon(setLatLon,latitude, longitude);
				
			} else {
				showSettingsAlert("NETWORK");
			}
			
	}
	
	
	public void showSettingsAlert(String provider) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				PatientHomeAltActivity.this);

		alertDialog.setTitle(provider + " SETTINGS");

		alertDialog
				.setMessage(provider + " is not enabled! Want to go to settings menu?");

		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						PatientHomeAltActivity.this.startActivity(intent);
					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}
	
	
	
	private void saveLatLon(String setLatLon, double latitude,double longitude)
	{
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		final String	userId=  settings.getString("id", "0");
		
		
		Utility utility=new Utility();
		String userData="";
		
		if (setLatLon.equalsIgnoreCase("continue"))
			{
				userData=utility.serviceCall(108,"S"+"!"+userId+"!"+latitude+"!"+longitude);
			}
		else if (setLatLon.equalsIgnoreCase("onDemand"))
		{
			userData=utility.serviceCall(107,userId+"!"+latitude+"!"+longitude);
		}
		
		System.out.println("108 result "+ userData);
		
		try {
	  		
		  		String message="";
				JSONObject jsonResponse = new JSONObject(userData);
				
				System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
				message=jsonResponse.optString("Message");
				
				/*if (message.equalsIgnoreCase("Success"))
				{
					                						
					Toast.makeText(getApplicationContext(), "Password changed Successfully.", Toast.LENGTH_LONG).show();
				}
				else
				{
					
					Toast.makeText(getApplicationContext(), "Password Not changed", Toast.LENGTH_LONG).show();;
				}*/
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void logout()
	{
		handler.removeCallbacksAndMessages(null);
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		String	userId=  settings.getString("id", "0");
    		
		Utility utility=new Utility();
		utility.serviceCall(112, userId);
		
		Intent intent =new Intent();
		intent.setClass(getApplicationContext(), SplashActivity.class);
		startActivity(intent);
	}

	
	
	private void updatePassword()
	{

		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		final String	userId=  settings.getString("id", "0");
		
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set prompts.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);
        final EditText input = (EditText) promptView.findViewById(R.id.userInput);
        
        alertDialogBuilder.setTitle("Change Password");
        
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                               // editTextMainScreen.setText(input.getText());
                            	Utility utility=new Utility();
                				String userData=utility.serviceCall(103,userId+"!"+"2"+"!"+input.getText());
                            	
                				System.out.println("103 result "+userData);
                				
                				try {
                			  		
	                			  		String message="";
	                					JSONObject jsonResponse = new JSONObject(userData);
	                					
	                					System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
	                					message=jsonResponse.optString("Message");
	                					
	                					if (message.equalsIgnoreCase("Success"))
	                					{
	                						                						
	                						Toast.makeText(getApplicationContext(), "Password changed Successfully.", Toast.LENGTH_LONG).show();
	                					}
	                					else
	                					{
	                						
	                						Toast.makeText(getApplicationContext(), "Password Not changed", Toast.LENGTH_LONG).show();;
	                					}
                					
                				} catch (JSONException e) {
                					// TODO Auto-generated catch block
                					e.printStackTrace();
                				}

                            }
                        })
		                        .setNegativeButton("Cancel",

                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                            }

                        });

        // create an alert dialog

        AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();
	}


	
}
