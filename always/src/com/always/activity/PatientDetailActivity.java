package com.always.activity;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.always.adapter.UserHistoryArrayAdapter;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.User;
import com.always.model.UserHistory;
import com.example.always.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PatientDetailActivity extends Activity {
	WebView web;

	
    private UserHistoryArrayAdapter arrayAdapter;
	private ListView listView;
	private static int colorIndex;
	
	 static final String urls = "http://api.androidhive.info/pizza/?format=xml";
	 
	 ImageView imgLeftH;
	  
	 
	 	private String userName;
		private String image;
		private String message;
		private String status;
		private   List<User> userList = new ArrayList<User>();
		
		private List<UserHistory> userHistoryList=new ArrayList<UserHistory>();
		public static final String MY_EMP_PREFS = "MySharedPref";
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_detail);
         
		Intent intent = getIntent();
        
        final String latLog = intent.getStringExtra("LatLog");
        final String getName = intent.getStringExtra("name");
        
		ImageView imgCallH;
		
		imgCallH=(ImageView)findViewById(R.id.imgCallH);
		
		final AlertDialog.Builder builderMain = new AlertDialog.Builder(this);
		imgCallH.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

        		builderMain.setTitle("Call");
				builderMain.setMessage("Would you like to call "+getName+" ?");

				builderMain.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing but close the dialog
			        	
			        	String phone="tel:+"+latLog.split("!")[3];
			        	
			        	Intent callIntent = new Intent(Intent.ACTION_CALL);
	                	callIntent.setData(Uri.parse(phone));
	                	startActivity(callIntent);
			            
			        }

			    });

				builderMain.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {

			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing
			            dialog.dismiss();
			        }
			    });

			    AlertDialog alert = builderMain.create();
			    alert.show();
			    
			}
		});
		
		
		
		
		
       // Toast.makeText(getApplicationContext(), "latlog===="+latLog, Toast.LENGTH_LONG).show();
        TextView txtCenterH=(TextView)findViewById(R.id.txtCenterH);
        txtCenterH.setText(getName);
        
       
		imgLeftH=(ImageView) findViewById(R.id.imgLeftH);
		
		imgLeftH.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		 web = (WebView)findViewById(R.id.webviewH);
	     web.getSettings().setJavaScriptEnabled(true);
	     web.getSettings().setBuiltInZoomControls(true);

	     web.requestFocusFromTouch();

	     web.setWebViewClient(new WebViewClient());
	     web.setWebChromeClient(new WebChromeClient());  
	     
	     web.loadUrl("file:///android_asset/patient_map.html?latLog="+latLog);
	    
	     
	     colorIndex = 0;
   		 listView = (ListView) findViewById(R.id.lstHistory);
   		
   		 XMLPullParserHandler parser = new XMLPullParserHandler();
   	            
   	
   	 		Utility utility=new Utility();

   			/*SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
   			
   	    	String id=  settings.getString("id", "0");*/
   	    	
   			String userData=utility.serviceCall(108,"D"+"!"+latLog.split("!")[0]);//For user detail
   			
   		   userList = parser.parseHistory(userData);
   	            
   	  
   	    arrayAdapter = new UserHistoryArrayAdapter(getApplicationContext(), R.layout.listview_row_layout);
   		listView.setAdapter(arrayAdapter);
        
   		
   		for(User user:userList)
   		{
   			userName=user.getUserName();
   			image=user.getImage();
   			
   			status=user.getStatus();
   			
   			message=user.getMessage();
   			userHistoryList=user.getUserHistory();
   			
   			UserHistory userHistory1=new UserHistory();
   			
   			userHistory1.setDtime("Now");
   			userHistory1.setLatitude("0");
   			userHistory1.setLongitude("0");
   			
   		 String text = "Status :" ;
         
         if (status.equalsIgnoreCase("online"))
        	 text = text +"<font color='green'>"+ status+"</font>";
         else 
        	 text = text +"<font color='red'>"+ status+"</font>";
         
    	 text = text +"     Location : "+message;
         String status =  text;
         
   			//userHistory1.setStatus("Status : "+status+" Location : "+message);
         	userHistory1.setStatus(status);
   			userHistory1.setLocation(message);
   			
   			arrayAdapter.add(userHistory1);
	             
   			
   		UserHistoryAdapter userHistoryAdapter1 = new UserHistoryAdapter( "Now",  "0","0",status,  message);
   			//arrayAdapter.add(userHistory);
   			
   	   	  for (UserHistory userHistory:userHistoryList)
   	  		{
   	       	
   	       	     String dtime = userHistory.getDtime();
   	             String latitude= userHistory.getLatitude();
   	             String longitude = userHistory.getLongitude();
   	             
   	          String text1 = "Status :" ;
   	         
   	       //   Toast.makeText(getApplicationContext(), "userHistory.getStatus()="+userHistory.getStatus(), Toast.LENGTH_LONG).show();
   	          
	   	         if (userHistory.getStatus().trim().equalsIgnoreCase("online"))
	   	        	text1 = text1 +"<font color='green'>"+ userHistory.getStatus()+"</font>";
	   	         else 
	   	        	text1 = text1 +"<font color='red'>"+ userHistory.getStatus()+"</font>";
	   	         
	   	         text1 = text1 +"     Location : "+userHistory.getLocation();
	   	         String status1 =  text1;
   	         
   	           //  String status1 =userHistory.getStatus();
   	            // String status = "Status : "+userHistory.getStatus()+" Location : "+message;
   	             String location=userHistory.getLocation();
   	             
   	              
   	             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
   	             
   	             UserHistoryAdapter userHistoryAdapter = new UserHistoryAdapter( dtime,  latitude,longitude,status1,  location);
   	             arrayAdapter.add(userHistory);
   	             
   	  		}
   	   	  
   		}
   	}

	

}
