package com.always.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.always.adapter.DataArrayAdapter1.DataViewHolder;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.example.always.R;
import com.example.always.R.id;
import com.example.always.R.layout;



import com.navdrawer.SimpleSideDrawer;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Listener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


import com.always.activity.SwipeMenu;
import com.always.activity.SwipeMenuCreator;
import com.always.activity.SwipeMenuItem;
import com.always.activity.SwipeMenuListView;
import com.always.activity.MainMapActivity.ViewHolderPattern;


public class MainListActivity extends Activity  {

	public  List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	SimpleSideDrawer slide_me;
    private SwipeMenuListView mListView;
    
    
	public static final String MY_EMP_PREFS = "MySharedPref";  
	    WebView web;
	
	    TextView txtUsers;
	
		private static final String TAG = "ListViewActivity";
		
		private DataArrayAdapter dataArrayAdapter;
		/*private ListView listView;*/
		
		private static int colorIndex;
		
		static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 
		private final static String SERVICE_URI = "http://www.gigaparse.com/clients/alwayz/API/APICloud.php";
		int height;
		int width ;
		DisplayMetrics displaymetrics;
		
	 TextView txtMessage;
	 TextView txtInvite;
		
	 int cntUser=0;
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
        displaymetrics = new DisplayMetrics();
  		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
  		height = displaymetrics.heightPixels;
  		width = displaymetrics.widthPixels;
  		
  		
           // setActivity();
  		setContentView(R.layout.activity_main_list);
       	   
      		
      	slide_me = new SimpleSideDrawer(this);
      	//runSlider();
      	slide_me.setLeftBehindContentView(R.layout.left_menu);
      		
    		ImageView left_button; 
    	    left_button = (ImageView) findViewById(R.id.imgLeft);
       		
    	   	
       		left_button.setOnClickListener(new View.OnClickListener() {

       			@Override
       			public void onClick(View v) {
       				// TODO Auto-generated method stub
       				slide_me.toggleLeftDrawer();
       			
       			}
       		});
	        mListView = (SwipeMenuListView)findViewById(R.id.list1);
		     
		    
        
       	//CAll service to fill listview data
		Utility utility=new Utility();
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		
    	String cid=  settings.getString("id", "0");
    	String imageProfile= settings.getString("image", "0");
    	
		String userData=utility.serviceCall(111,cid);
		
		System.out.println("Mainmap userData "+userData);
		
		System.out.println("caregiver_idcaregiver_idcaregiver_id"+cid);
		
		TextView txtCenter;
		
		txtCenter=(TextView)findViewById(R.id.txtCenter);
			 
		txtCenter.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (cntUser>0)
				{
					Toast.makeText(getApplicationContext(), "Click",Toast.LENGTH_LONG).show();
					Intent intent=new Intent();
					intent.setClass(getApplicationContext(), ManageUsersActivity.class);
					startActivity(intent);
				}
			}
		});
		  
	    	 web = (WebView)findViewById(R.id.webview);
	         web.getSettings().setJavaScriptEnabled(true);
	         web.getSettings().setBuiltInZoomControls(true);

	         web.requestFocusFromTouch();

	         web.setWebViewClient(new WebViewClient());
	         web.setWebChromeClient(new WebChromeClient());  
	        
	         web.loadUrl("file:///android_asset/jsonmap.html");
	      		
   		
	         //Open MAnage Users window
   	       colorIndex = 0;
   	       
                        
        
           String message = "";
           JSONObject reader;
           
		try {
			 reader = new JSONObject(userData);
			 message  = reader.getString("Message");
			  
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		txtMessage=(TextView) findViewById(R.id.txtMessage);
		txtInvite=(TextView) findViewById(R.id.txtInvite);
		
		if ( message.equalsIgnoreCase("Success"))
		{
			mListView.setVisibility(View.VISIBLE);
			txtMessage.setVisibility(View.GONE);
			txtInvite.setVisibility(View.GONE);
			
  	       XMLPullParserHandler parser = new XMLPullParserHandler();
      	   onlineDataList = parser.parseJSON(userData);
      	   
	      	dataArrayAdapter = new DataArrayAdapter(MainListActivity.this, R.layout.listview_row_layout);
	      	mListView.setAdapter(dataArrayAdapter);
   		
		}
		else
		{
			mListView.setVisibility(View.GONE);	
		   txtMessage.setVisibility(View.VISIBLE);
		   txtInvite.setVisibility(View.VISIBLE);
		   
		    
				 txtInvite.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i =new Intent();
						i.setClass(getApplicationContext(), AddCaregiverContactsActivity.class);
						i.putExtra("comeFrom", "caregiver");
						startActivity(i);
						
					}
				});
				   
		}
      	            
      	   //Toast.makeText(MainActivity.this, " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
      		
      	  	  for (OnlineData onlineData:onlineDataList)
     		  {
      	  		  
      	  		int id=onlineData.getId();
          	    String image = onlineData.getName();
                String name = onlineData.getName();
                String text = "Status :" ;
	            
                cntUser=cntUser+1;
                
	             if (onlineData.getStatus().equalsIgnoreCase("online"))
	            	 text = text +"<font color='green'>"+ onlineData.getStatus()+"</font>";
	             else 
	            	 text = text +"<font color='red'>"+ onlineData.getStatus()+"</font>";
	             
	        	 text = text +"     Location : "+onlineData.getLocation();
	             String status =  text;
	             
             //   String status = "Status : "+ onlineData.getStatus()+"     Location : "+onlineData.getLocation();
                String  test =onlineData.getId()+"!"+onlineData.getLatitude()+"!"+onlineData.getLongitude();
                
                String imageListView=onlineData.getImage(); 
                
               int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
                
               ServiceData serviceData = new ServiceData(imgResId,name,status,test,imageListView);
           
               dataArrayAdapter.add(serviceData);
               
     		}
      	  	
      	  	TextView txtCircle;
      		txtCircle=(TextView)findViewById(R.id.txtCircle);
      		
      	  	txtCircle.setText(String.valueOf(cntUser));
      	 
        	String fullName=  settings.getString("fullName", "0");
        	TextView txtName=(TextView) findViewById(R.id.txtName);
        	TextView txtUser=(TextView) findViewById(R.id.txtUser);
        	
        	txtName.setText(fullName);
        	txtUser.setText("Caregiver for "+cntUser+" Users");
        	
        	ImageView imgPicture;
        	
        	imgPicture =(ImageView)findViewById(R.id.imgPicture);
        	
        		Toast.makeText(getApplicationContext(), "imageProfile "+imageProfile , Toast.LENGTH_LONG).show();
        		
        			   if( (imageProfile!= null) && (imageProfile.length()>0))
        	    		{
        				   //http://stackoverflow.com/questions/23562794/doesnt-picasso-support-to-download-images-which-uses-https-protocol
        				   //https://github.com/square/picasso/issues/500
        	    			Picasso.with(getBaseContext()).load(imageProfile).into(imgPicture);
        				   
        				 /*  PicassoTrustAll.getInstance(context)
        	                .load(url)
        	                .into(imageView);*/
        				   
        				/*   OkHttpClient client = new OkHttpClient();
        				   client.setProtocols(Arrays.asList(Protocol.HTTP_11));
        				   Picasso picasso = new Picasso.Builder(context)
        				       .downloader(new OkDownloader(client))
        				       .build();*/
        				   
        	    		}
        	    
        			   
			   // step 1. create a MenuCreator
		        SwipeMenuCreator creator = new SwipeMenuCreator() {

		            @Override
		            public void create(SwipeMenu menu) {
		                // create "open" item
		            	
		            	
		            	
		               /* SwipeMenuItem deleteItem = new SwipeMenuItem(
		                        getApplicationContext());
		                // set item background
		                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
		                        0x3F, 0x25)));
		                // set item width
		                deleteItem.setWidth(dp2px(90));
		                // set a icon
		                deleteItem.setIcon(R.drawable.ic_delete);
		                // add to menu
		                menu.addMenuItem(deleteItem);
		                
		                
		                SwipeMenuItem deleteItem1 = new SwipeMenuItem(
		                        getApplicationContext());
		                // set item background
		                deleteItem1.setBackground(new ColorDrawable(Color.rgb(0xF9,
		                        0x3F, 0x25)));
		                // set item width
		                deleteItem1.setWidth(dp2px(90));
		                // set a icon
		                deleteItem1.setIcon(R.drawable.ic_delete);
		                // add to menu
		                menu.addMenuItem(deleteItem1);
		                */
		            	SwipeMenuItem openItem = new SwipeMenuItem(
		                        getApplicationContext());
		                // set item background
		                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
		                        0xCE)));
		                // set item width
		                openItem.setWidth(dp2px(90));
		                // set item title
		                openItem.setTitle("Call");
		                // set item title fontsize
		                openItem.setTitleSize(18);
		                // set item title font color
		                openItem.setTitleColor(Color.WHITE);
		                // add to menu
		                menu.addMenuItem(openItem);

		            	SwipeMenuItem HistoryItem = new SwipeMenuItem(
		                        getApplicationContext());
		                // set item background
		            	HistoryItem.setBackground(new ColorDrawable(Color.rgb(0x30, 0xB1,
		                        0xF5)));
		                // set item width
		            	HistoryItem.setWidth(dp2px(90));
		                // set item title
		            	HistoryItem.setTitle("History");
		                // set item title fontsize
		            	HistoryItem.setTitleSize(18);
		                // set item title font color
		            	HistoryItem.setTitleColor(Color.WHITE);
		                // add to menu
		                menu.addMenuItem(HistoryItem);
		             	
		            }
		        };
		        
		        final AlertDialog.Builder builderMain = new AlertDialog.Builder(this);
		        // set creator
		        mListView.setMenuCreator(creator);

		        // step 2. listener item click event
		        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
		            @Override
		            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
		            	final OnlineData item = onlineDataList.get(position);
		                switch (index) {
		                    case 0:
		                        // open
		                       // open(item);
		                    	Toast.makeText(getApplicationContext(), "Call", Toast.LENGTH_LONG).show();
		                    	 
		                    	 
		                    		builderMain.setTitle("Call");
		            				builderMain.setMessage("Would you like to call "+item.getName()+" ?");

		            				builderMain.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

		            			        public void onClick(DialogInterface dialog, int which) {
		            			            // Do nothing but close the dialog
		            			        	
		            			        	String phone="tel:+"+item.getPhone();
		            			        	
		            			        	Intent callIntent = new Intent(Intent.ACTION_CALL);
		            	                	callIntent.setData(Uri.parse(phone));
		            	                	startActivity(callIntent);
		            			            
		            			        }

		            			    });

		            				builderMain.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {

		            			        @Override
		            			        public void onClick(DialogInterface dialog, int which) {
		            			            // Do nothing
		            			            dialog.dismiss();
		            			        }
		            			    });

		            			    AlertDialog alert = builderMain.create();
		            			    alert.show();
		            			    
		            			    
		                        break;
		                    case 1:
		                        // delete
		                    	Toast.makeText(getApplicationContext(), "phone "+ item.getPhone(), Toast.LENGTH_LONG).show();
		                    	/*Toast.makeText(getApplicationContext(), item.getName(), Toast.LENGTH_LONG).show();
		                    	Toast.makeText(getApplicationContext(), item.getLatitude(), Toast.LENGTH_LONG).show();
		                    	Toast.makeText(getApplicationContext(), item.getLongitude(), Toast.LENGTH_LONG).show();
		                    	Toast.makeText(getApplicationContext(), item.getTest(), Toast.LENGTH_LONG).show();*/
		                    	
		                    	Intent intent =new Intent();
		        				  intent.setClass(getApplicationContext(),PatientDetailActivity.class);
		        				  
		        				  intent.putExtra("name",item.getName());
		        				  intent.putExtra("LatLog",item.getId()+"!"+item.getLatitude()+"!"+item.getLongitude()+"!"+item.getPhone());
		        				  
		        				  startActivity(intent);
		        				  

		                    	/* DataViewHolder viewHolder;
		        				  
		        				  viewHolder=(DataViewHolder)arg1.getTag();
		        				  
		        				  
		        				  String name = ((TextView)arg1.findViewById(R.id.name)).getText().toString();
		        		          String test = ((TextView)arg1.findViewById(R.id.test)).getText().toString();

		        		          
		                    	/*onlineDataList.remove(position);
		                    	onlineDataList.notifyDataSetChanged();*/
		                        break;
		                }
		                return false;
		            }
		        });
		        
		        // set SwipeListener
		        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

		            @Override
		            public void onSwipeStart(int position) {
		                // swipe start
		            }

		            @Override
		            public void onSwipeEnd(int position) {
		                // swipe end
		            }
		        });

		        // test item long click
		        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

		            @Override
		            public boolean onItemLongClick(AdapterView<?> parent, View view,
		                                           int position, long id) {
		                Toast.makeText(getApplicationContext(), position + " long click", Toast.LENGTH_SHORT).show();
		                return false;
		            }
		        });
        	
		        
		        
		        mListView.setOnItemClickListener(new OnItemClickListener() {
		          		
	     			  public void onItemClick(AdapterView<?> arg0,
	     			  View arg1, int position, long arg3)
	     			  { 
	     				  DataViewHolder viewHolder;
	     				  
	     				  viewHolder=(DataViewHolder)arg1.getTag();
	     				  
	     				 final OnlineData item = onlineDataList.get(position);
	     				  
	     				  String name = ((TextView)arg1.findViewById(R.id.name)).getText().toString();
	     		         // String test = ((TextView)arg1.findViewById(R.id.test)).getText().toString();

	     		          Toast.makeText(getApplicationContext(), "name " +name, Toast.LENGTH_LONG).show();
	     		          Toast.makeText(getApplicationContext(), "test " +item.getId()+"!"+item.getLatitude()+"!"+item.getLongitude()+"!"+item.getPhone(), Toast.LENGTH_LONG).show();
	     		         
	     		          
	     		          Intent intent =new Intent();
	     				  intent.setClass(getApplicationContext(),PatientDetailActivity.class);
	     				  
	     				  intent.putExtra("name",name);
	     				  intent.putExtra("LatLog",item.getId()+"!"+item.getLatitude()+"!"+item.getLongitude());
	     				  
	     				  startActivity(intent);
	     			  }
	     	 });

		     	 
           	setClickEvent();
    }
     
  
    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
  

	
    private void setClickEvent()
    {
     	 
    	TextView  txtManageUsers;
       txtManageUsers=(TextView)slide_me.findViewById(R.id.txtUsers);
		txtManageUsers.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (cntUser>0)
				{
					Intent intent=new Intent();
					intent.setClass(getApplicationContext(), ManageUsersActivity.class);
					startActivity(intent);
				}
			}
		});
		  
		  //Open My Subscription window
		TextView  txtSub;
		txtSub=(TextView)slide_me.findViewById(R.id.txtSub);
		  
		 txtSub.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),SubscriptionActivity.class);
				startActivity(intent);
			}
		});
  		
		 
		 //open My Account window
		   TextView  txtAccount;
		  txtAccount=(TextView)slide_me.findViewById(R.id.txtAccount);
		  
		  txtAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),MyAccountActivity.class);
				startActivity(intent);
			}
		});
		  
		  
		
    
		txtUsers=(TextView) findViewById(R.id.txtUsers);
     	
     	txtUsers.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (cntUser>0)
				{
					Intent  intent=new Intent();
					intent.setClass(getApplicationContext(), ManageUsersActivity.class);
					startActivity(intent);
					
					
				}
				
			}
		});
     	  
    }


    private class SSLTolerentListView extends ListView {

        public SSLTolerentListView(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}

		
        public void onReceivedSslError(ListView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }

     }
    
public class DataArrayAdapter extends ArrayAdapter<ServiceData> {
   	   Context mcontext;
       private static final String TAG = "DataArrayyAdapter";
       private final int INVALID = -1;
	   protected int DELETE_POS = -1;

    	private List<ServiceData> dataList = new ArrayList<ServiceData>();

    	private LruCache<String, Bitmap> mMemoryCache;
   	    private Context ctx;
      // ImageLoader imageLoader = new ImageLoader(this);
       Context mContext;
       private int action_down_x = 0;
       private int action_up_x = 0;
       private int difference = 0;
       
       public  class DataViewHolder {
           ImageView image;
           TextView name;
           TextView status;
        //   ImageView rightImg;
          TextView test;
   		
          TextView txtHistory;
          TextView txtCall;
          
       }

       
   
       
       
  /*     private class SSLTolerentWebViewClient extends WebViewClient {

           @Override
           public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
               handler.proceed(); // Ignore SSL certificate errors
           }

        }
       
       mWebView.setWebViewClient(
               new SSLTolerentWebViewClient()
       );
     */
       
       //http://javatechig.com/android/loading-image-asynchronously-in-android-listview
       public void onReceivedSslError(ListView view, SslErrorHandler handler,
               SslError error) {
           handler.cancel();
       }
       
       
       public DataArrayAdapter(Context context, int textViewResourceId) {
       	 super(context, textViewResourceId);
       	 
       	// Get memory class of this device, exceeding this amount will throw an
           // OutOfMemory exception.
           final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    
           // Use 1/8th of the available memory for this memory cache.
           final int cacheSize = maxMemory / 8;
    
           mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
    
               @SuppressLint("NewApi") protected int sizeOf(String key, Bitmap bitmap) {
                   // The cache size will be measured in bytes rather than number
                   // of items.
                   return bitmap.getByteCount();
               }
    
           };
           
          
       }

       
   	@Override
   	public void add(ServiceData object) {
   		dataList.add(object);
   		super.add(object);
   	}
   	
   	
   	
   	@Override
   	public int getCount() {
   		return this.dataList.size();
   	}


   	
       @Override
   	public ServiceData getItem(int index) {
   		return this.dataList.get(index);
   	}
       
      

       public boolean onCreateOptionsMenu(Menu menu) {
           getMenuInflater().inflate(R.menu.menu_main, menu);
           return true;
       }
    
       
       class ViewHolder {
           ImageView image;
           TextView name;
           TextView status;
           
           public ViewHolder(View view) {
               image = (ImageView) view.findViewById(R.id.image);
               name = (TextView) view.findViewById(R.id.name);
               status= (TextView) view.findViewById(R.id.status);
               view.setTag(this);
           }
       }
       
   	@Override
   	public View getView(final int position, View convertView, ViewGroup parent) {
   		
   		View row = convertView;
        DataViewHolder viewHolder;
       // Activity cntx = new Activity();
        
        
     // TODO Auto-generated method stub
     			if (convertView == null) {
     				convertView = LayoutInflater.from(MainListActivity.this).inflate(
     						R.layout.listview_main_map, null);
     			}
     			
     			ImageView image = ViewHolderPattern.get(convertView, R.id.image);
     			TextView name = ViewHolderPattern.get(convertView, R.id.name);
     			TextView status = ViewHolderPattern.get(convertView, R.id.status);
     			TextView txtHistory = ViewHolderPattern.get(convertView, R.id.txtHistory);
     			TextView txtCall = ViewHolderPattern.get(convertView, R.id.txtCall);
     			
     			
     			
     			/*Toast.makeText(getApplicationContext(), " DELETE_POS=== "+DELETE_POS +" position ==="+position, Toast.LENGTH_LONG).show();
     			
     			if (DELETE_POS == position) {
     				
     				txtHistory.setVisibility(View.VISIBLE);
     				txtCall.setVisibility(View.VISIBLE);
     			} 
     			else
     			{
     				txtHistory.setVisibility(View.GONE);
     			    txtCall.setVisibility(View.GONE);
	                }*/
     			
     			         			    
     			ServiceData serviceData = getItem(position);
     			
     			name.setText(serviceData.getName());
     			//status.setText(serviceData.getStatus());

     			status.setText(Html.fromHtml(serviceData.getStatus()));
     			
     			if( (serviceData.getImageListView()!= null) && (serviceData.getImageListView().length()>0))
            		{
            		
     				  Picasso.with(getContext()).load(serviceData.getImageListView()).into(image);
     				
     				
     				/*Picasso picasso = new Picasso.Builder(getActivity()).listener(
     		                new Listener() {
     		                    @Override
     		                    public void onImageLoadFailed(Picasso picasso, Uri uri,
     		                            Exception exception) {
     		                        exception.printStackTrace();
     		                    }

     		                }).debugging(true).build();*/
     				//Picasso.load(serviceData.getImageListView()).resize(width, height).into(image);

     				//https://github.com/square/picasso/issues/500
		     			/*	OkHttpClient client = new OkHttpClient();
		     				client.setProtocols(Arrays.asList(Protocol.HTTP_11));
		     				Picasso picasso = new Picasso.Builder(context)
		     				    .downloader(new OkDownloader(client))
		     				    .build();*/
            		}
     			
     			return convertView;
   /*		
   	 if (convertView == null) {
         convertView = View.inflate(getApplicationContext(),R.layout.listview_main_map, null);
         new ViewHolder(convertView);
     }
   	 
   	 
     ViewHolder holder = (ViewHolder) convertView.getTag();
     
     ServiceData item = getItem(position);
     
    // holder.image.setImageDrawable(item.loadIcon(getPackageManager()));
     //holder.name.setText(item.loadLabel(getPackageManager()));
     return convertView;*/
     
   		
   	}

 
    
   		private Context getActivity() {
		// TODO Auto-generated method stub
		return null;
	}


		public Bitmap decodeFile(Context context,int resId) {
   						try {
   							// decode image size
   							mcontext=context;
   							BitmapFactory.Options o = new BitmapFactory.Options();
   							o.inJustDecodeBounds = true;
   							BitmapFactory.decodeResource(mcontext.getResources(), resId, o);
   							// Find the correct scale value. It should be the power of 2.
   							final int REQUIRED_SIZE = 200;
   							int width_tmp = o.outWidth, height_tmp = o.outHeight;
   							int scale = 1;
   							while (true)
   							{
   							 if (width_tmp / 2 < REQUIRED_SIZE
   							 || height_tmp / 2 < REQUIRED_SIZE)
   							 break;
   							 width_tmp /= 2;
   							 height_tmp /= 2;
   							 scale++;
   						}
   						// decode with inSampleSize
   						BitmapFactory.Options o2 = new BitmapFactory.Options();
   						o2.inSampleSize = scale;
   						return BitmapFactory.decodeResource(mcontext.getResources(), resId, o2);
   						} catch (Exception e) {
   				}
   				return null;
   		}



       public Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width) {
      	 // TODO Auto-generated method stub
      	 int targetWidth = width;
      	 int targetHeight = width;
      	 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
      	 targetHeight,Bitmap.Config.ARGB_8888);

      	 Canvas canvas = new Canvas(targetBitmap);
      	 Path path = new Path();
      	 path.addCircle(((float) targetWidth - 1) / 2,
      	 ((float) targetHeight - 1) / 2,
      	 (Math.min(((float) targetWidth),
      	 ((float) targetHeight)) / 2),
      	 Path.Direction.CCW);
      	 canvas.clipPath(path);
      	 Bitmap sourceBitmap = scaleBitmapImage;
      	 canvas.drawBitmap(sourceBitmap,
      	 new Rect(0, 0, sourceBitmap.getWidth(),
      	 sourceBitmap.getHeight()),
      	 new Rect(0, 0, targetWidth,
      	 targetHeight), null);
      	 return targetBitmap;
      	 }
      	
       

       public Bitmap decodeToBitmap(byte[] decodedByte) {
   		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
   	}
   }
  
  	public static class ViewHolderPattern {
		// I added a generic return type to reduce the casting noise in client
		// code
		@SuppressWarnings("unchecked")
		public static <T extends View> T get(View view, int id) {
			SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
			if (viewHolder == null) {
				viewHolder = new SparseArray<View>();
				view.setTag(viewHolder);
			}
			View childView = viewHolder.get(id);
			if (childView == null) {
				childView = view.findViewById(id);
				viewHolder.put(id, childView);
			}
			return (T) childView;
		}
	}
       
  	

 
}