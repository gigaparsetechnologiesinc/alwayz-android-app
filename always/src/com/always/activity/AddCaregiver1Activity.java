package com.always.activity;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.always.adapter.CaregiverArrayAdapter;
import com.always.adapter.UserArrayAdapter;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.example.always.R;
import com.example.always.R.id;
import com.example.always.R.layout;
import com.example.always.R.menu;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AddCaregiver1Activity extends Activity  {

	 private static final String TAG = "ListViewActivity";

	    private CaregiverArrayAdapter caregiverArrayAdapter;
		private ListView listView;

		private static int colorIndex;
		
		Map<String, Integer> mapIndex;
		
		// static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 
		 TextView txtBack;
		 ImageView  imgBack;	 
		 public static final String MY_EMP_PREFS = "MySharedPref";  
		 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_caregiver1);
		
		txtBack=(TextView) findViewById(R.id.txtBack);
		
		txtBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});

		
		imgBack=(ImageView) findViewById(R.id.imgBack);
	
		
		imgBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});
		
		
		
		
		 colorIndex = 0;
		listView = (ListView) findViewById(R.id.listView);
		
	//CAll service to fill listview data
		
			Utility utility=new Utility();
			
			SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
			String caregiver_id=  settings.getString("caregiver_id", "0");
	    	
			String userData=utility.serviceCall(111,caregiver_id);
		
		    List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	            
		   XMLPullParserHandler parser = new XMLPullParserHandler();
  	   onlineDataList = parser.parseJSON(userData);
  	            
  	   
  	   
	           Toast.makeText(getApplicationContext(), " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
	         
		   	             
	        caregiverArrayAdapter = new CaregiverArrayAdapter(getApplicationContext(), R.layout.list_row_layout_acaregiver);
		   	listView.setAdapter(caregiverArrayAdapter);
		           
		   	  for (OnlineData onlineData:onlineDataList)
		  		{
		       	
		       	     String image = onlineData.getName();
		             String name = onlineData.getName();
		         	
		             String text = "Status :" ;
		             
		             if (onlineData.getStatus().equalsIgnoreCase("online"))
		            	 text = text +"<font color='green'>"+ onlineData.getStatus()+"</font>";
		             else 
		            	 text = text +"<font color='red'>"+ onlineData.getStatus()+"</font>";
		             
		        	 text = text +"     Location : "+onlineData.getLocation();
		             String status =  text;
		             //"Status : "+ onlineData.getStatus()+"     Location : "+onlineData.getLocation();
		             
		             String test = onlineData.getStatus()+"$"+onlineData.getStatus();
		             
		             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
		             
		           ServiceData serviceData = new ServiceData(imgResId,name,status,test);//,hidData);
		           
		          // serviceData.setTest("manageUsers");
		           caregiverArrayAdapter.add(serviceData);
		             
		     	 //  array_sort.add(onlineData.getName());
		         // Toast.makeText(getApplicationContext(), " onlineData.getName() ="+ onlineData.getName(), Toast.LENGTH_LONG).show();
		     	   //array_sort1.add(onlineData.getLocation());
		  		}
		       
		
	}
	
	
	
	/*private void getIndexList(String[] fruits) {
		mapIndex = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < fruits.length; i++) {
			String fruit = fruits[i];
			String index = fruit.substring(0, 1);

			if (mapIndex.get(index) == null)
				mapIndex.put(index, i);
		}
	}

	private void displayIndex() {
		LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index);

		TextView textView;
		List<String> indexList = new ArrayList<String>(mapIndex.keySet());
		for (String index : indexList) {
			textView = (TextView) getLayoutInflater().inflate(R.layout.side_index_item, null);
			textView.setText(index);
			textView.setOnClickListener(this);
			indexLayout.addView(textView);
		}
	}*/

	public void onClick(View view) {
		TextView selectedIndex = (TextView) view;
		listView.setSelection(mapIndex.get(selectedIndex.getText()));
	}

	

private String getXmlFromUrl(String urlString) {

Toast.makeText(getApplicationContext(), "getXMLFRomURL", Toast.LENGTH_LONG);


StringBuffer output = new StringBuffer("");
 try {
     InputStream stream = null;
     URL url = new URL(urlString);
     URLConnection connection = url.openConnection();

     HttpURLConnection httpConnection = (HttpURLConnection) connection;
     httpConnection.setRequestMethod("GET");
     httpConnection.connect();

     if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
         stream = httpConnection.getInputStream();

         BufferedReader buffer = new BufferedReader(
                 new InputStreamReader(stream));
         String s = "";
     while ((s = buffer.readLine()) != null)
         output.append(s);
 }
  
} catch (Exception ex) {
 ex.printStackTrace();
}
 
return output.toString();

/* ---Using Apache DefaultHttpClient for applications targeting 
Froyo and previous versions --- */
/*String xml = null;

 try {
     DefaultHttpClient httpClient = new DefaultHttpClient();
     HttpGet httpGet = new HttpGet(url);

     HttpResponse httpResponse = httpClient.execute(httpGet);
     HttpEntity httpEntity = httpResponse.getEntity();
     xml = EntityUtils.toString(httpEntity);

 } catch (UnsupportedEncodingException e) {
     e.printStackTrace();
 } catch (ClientProtocolException e) {
     e.printStackTrace();
 } catch (IOException e) {
     e.printStackTrace();
 }
 return xml;*/
}  



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_users, menu);
		return true;
	}


}
