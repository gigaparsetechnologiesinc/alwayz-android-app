package com.always.activity;

public class UserHistoryAdapter {
	
	private String dtime;
	private String latitude;
	private String longitude;
	private String status;
	private String location;
	
	
	public UserHistoryAdapter(String dtime, String latitude,String longitude,String status, String location){
		super();
        this.setDtime(dtime);
		this.setLatitude(latitude);
		this.setLongitude(longitude);
        this.setStatus(status);
        this.setLocation(location);
      //  this.setHidData(hidData);
	}
	
	public String getDtime() {
		return dtime;
	}
	public void setDtime(String dtime) {
		this.dtime = dtime;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
}
