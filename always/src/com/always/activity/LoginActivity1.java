package com.always.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.always.common.Utility;
import com.always.model.OnlineData;
import com.example.always.R;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity1 extends Activity {

	EditText editTxtEmail;
	EditText editTxtPassword;
	TextView txtLogin;
	ProgressDialog prgDialog;
	TextView errorMsg;
	TextView txtLoginUser;
	TextView txtSignUp;
	TextView txtForgotPassword; 
	final Context context = this;
	
	String password;
	public static final String MY_EMP_PREFS = "MySharedPref";  
     boolean  DEVELOPER_MODE=true;
	
    @SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		if (android.os.Build.VERSION.SDK_INT==19)
		{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

			StrictMode.setThreadPolicy(policy);
		}
		
		     DisplayMetrics displaymetrics = new DisplayMetrics();
	  		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
	  		int height = displaymetrics.heightPixels;
	  		int width = displaymetrics.widthPixels;
	  		Toast.makeText(LoginActivity1.this, width+ " "+height, 0).show();
		
		//Toast.makeText(getApplicationContext(), "version =="+ android.os.Build.VERSION.SDK_INT, Toast.LENGTH_LONG).show();
		//StrictMode.enableDefaults(); 
		//StrictMode.enableDefaults();
		
		txtLogin=(TextView) findViewById(R.id.txtLogin);
		txtSignUp=(TextView) findViewById(R.id.txtSignUp);
		editTxtEmail=(EditText) findViewById(R.id.editTxtEmail);
		editTxtPassword=(EditText) findViewById(R.id.editTxtPassword);
		txtForgotPassword=(TextView) findViewById(R.id.forgotpassword);
		
		//Toast.makeText(getBaseContext(), "Login", Toast.LENGTH_LONG).show();
		
		txtLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Toast.makeText(getBaseContext(), "Login Click", Toast.LENGTH_LONG).show();
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Login", Toast.LENGTH_LONG).show();
				try {
					loginUser();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Exception===="+e.toString());
				}
			}
		});
		
		
		txtSignUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				    Intent intent =new Intent();
	                intent.setClass(getApplicationContext(),WhoAreYouActivity.class );
	                startActivity(intent);
			}
		});
		
		txtForgotPassword.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				forgotPassword();
			}
		});
	
	}

	
	    
    private void forgotPassword()
	{

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.alert_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set prompts.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);
        final EditText input = (EditText) promptView.findViewById(R.id.userInput);
        
        alertDialogBuilder.setTitle("Forgot Password");
        
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                               // editTextMainScreen.setText(input.getText());
                            	Utility utility=new Utility();
                				String userData=utility.serviceCall(114,input.getText().toString());
                            	
                				System.out.println("114 result "+userData);
                				
                				try {
                			  		
                			  		String message="";
                					JSONObject jsonResponse = new JSONObject(userData);
                					
                					System.out.println("jsonResponse message "+jsonResponse.optString("Message"));
                					message=jsonResponse.optString("Message");
                					
                					if (message.equalsIgnoreCase("Success"))
                					{
                						                						
                						Toast.makeText(getApplicationContext(), "Password Successfully changed.", Toast.LENGTH_LONG).show();
                					}
                					else
                					{
                						
                						Toast.makeText(getApplicationContext(), "Password Not changed.", Toast.LENGTH_LONG).show();;
                					}
                					
                				} catch (JSONException e) {
                					// TODO Auto-generated catch block
                					e.printStackTrace();
                				}
                				
                            }
                        })
		                        .setNegativeButton("Cancel",

                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                            }

                        });

        // create an alert dialog

        AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();
	}
	
	
public void loginUser() throws JSONException{
	
	Utility utility=new Utility();
	
	String serviceResponse="";
	
	 System.out.println("loginUserloginUser=");
	//Toast.makeText(getApplicationContext(), "Login2", Toast.LENGTH_LONG).show();
		        // Get Email Edit View Value
	        String email = editTxtEmail.getText().toString();
	        // Get Password Edit View Value
	         password = editTxtPassword.getText().toString();
	        
	        System.out.println("email="+email);
	        System.out.println("password="+password);
	        
	        // Instantiate Http Request Param Object
	        RequestParams params = new RequestParams();
	        // When Email Edit View and Password Edit View have values other than Null
	        if(Utility.isNotNull(email) && Utility.isNotNull(password)){
	            // When Email entered is Valid
	            if(Utility.validate(email)){
	                // Put Http parameter username with value of Email Edit View control
	                params.put("email", email);
	                // Put Http parameter password with value of Password Edit Value control
	                params.put("password", password);
	                // Invoke RESTful Web Service with Http parameters
	                
		               //Call WebService for login
	                try {
							
	                        serviceResponse=  invokeWS(email+"!"+password);
	                		System.out.println("invokeWS===");
	                		  
	            	    	//Utility utility1=new Utility();
	            	    	
	            			/*String userData=utility1.serviceCall(100,email+"!"+password);
	            		  	System.out.println("reader111=="+userData);*/
	            		  	
	            		  	//Toast.makeText(LoginActivity.this,"reader111=="+serviceResponse, 0).show();
							
		                	
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("e.printStackTrace()==");
						}
						

						JSONObject reader = new JSONObject(serviceResponse);
						
						
						String message  = reader.getString("Message");
					
		                System.out.println("messagemessage=="+message);
		             //  	String userType = editor.getString("userType", "No Type defined");
			          //  Toast.makeText(getApplicationContext(), "userType="+userType, Toast.LENGTH_LONG).show();
		             
			            if (message.equals("Success"))
			            {
					            //userType="2";
			            	
			            	  //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
			            	SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
			            	String userType=  settings.getString("userType", "0");
			            	  
			            	System.out.println("userTypeuserTypeuserType==="+userType);
			                     
				                if (userType.equals("1"))
				                {
					                Intent intent =new Intent();
					                intent.setClass(getApplicationContext(),MainListActivity.class );
					                startActivity(intent);
				                }
				                else if (userType.equals("2"))
				                {
				                	 /* Intent callIntent = new Intent(Intent.ACTION_CALL); 
				                	  callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
				                	  callIntent.setClass(getApplicationContext(), LocationActivity.class);
				                	  */
				                	 int size = settings.getInt("caregiverArr" + "_size", 0);
				                	 
				                	    String caregiverArray[] = new String[size];
				                	    /*
				                	    for(int i=0;i<size;i++)  
				                	        array[i] = prefs.getString(arrayName + "_" + i, null);*/  
				                	 
				                	    if (size==1)
				                	    {
						                	Intent intent =new Intent();
							                intent.setClass(getApplicationContext(),PatientHomeActivity.class );
							                startActivity(intent);
				                	    }
				                	    else if (size>1)
				                	    {
				                	    	Intent intent =new Intent();
							                intent.setClass(getApplicationContext(),PatientHomeAltActivity.class );
							                startActivity(intent);
				                	    }
					                
					                
				                }
			           }
			            else
			            {
			            	Toast.makeText(getApplicationContext(), "Invalid User ID and Password.", Toast.LENGTH_LONG).show();
			            }
		                	
	            } 
	            // When Email is invalid
	            else{
	                Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
	            }
	        } else{
	            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
	        }
	 
	    }
	 
	    /**
	     * Method that performs RESTful webservice invocations
	     * 
	     * @param params
	     * @throws JSONException 
	     */
	    public String invokeWS(String params) throws JSONException{
	    	
	    	String MY_EMP_PREFS1 = "MySharedPref";  
	    	System.out.println("invokeWS===");
	    	
	    	
	    	Utility utility=new Utility();
	    	
			String userData=utility.serviceCall(100,params);
		  	System.out.println("reader111=="+userData);
			JSONObject reader = new JSONObject(userData);
			
			String message  = reader.getString("Message");	
			
			
			 if (message.equals("Success"))
	            {
				
				 
				    JSONObject user  = reader.getJSONObject("user");
					String userType = user.getString("userType");
					
				
					System.out.println("serviceResponse =="+ userData);
					
					
					SharedPreferences settings = getSharedPreferences(MY_EMP_PREFS1,0); 
					//need an editor to edit and save values
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("id", user.getString("id"));
		            editor.putString("image", user.getString("image"));
		            editor.putString("fullName",user.getString("fullName"));
		            editor.putString("password", password);
		            editor.putString("phone", user.getString("phone"));
		            editor.putString("latitude", user.getString("latitude"));
		            editor.putString("longitude",user.getString("longitude"));
		            editor.putString("cardNumber", user.getString("cardNumber"));
		            editor.putString("userType", user.getString("userType"));
		            editor.putString("status", user.getString("status"));
		            editor.putString("caregiver_id",user.getString("caregiver_id"));
		            editor.putString("email",user.getString("email"));
		            editor.putString("password",params.split("!")[1]);
		            
		           // editor.putString("password", user.getString("password"));
		            
		            
		            System.out.println("reader.getString(cargiver) "+reader.getString("cargiver"));
		            
		            if ((reader.getString("cargiver") !=null) && (reader.getString("cargiver") !="null") && (reader.getString("cargiver") !=""))
		            {
		            	

			            JSONArray caregiverArr  = reader.getJSONArray("cargiver");
			            
			            editor.putInt("caregiverArr" +"_size", caregiverArr.length());  
			            
			            List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
			            
			            for(int i=0; i<caregiverArr.length(); i++){
			                JSONObject json_data = caregiverArr.getJSONObject(i);
	
			              /*  editor.putString("caregiverImage" + "_" + i, json_data.getString("image"));  
			                editor.putString("caregiverName" + "_" + i, json_data.getString("fullName"));
			                editor.putString("phoneNo" + "_" + i, json_data.getString("phone"));*/
			                
			               /* OnlineData  onlineData=new OnlineData();
			                
			                onlineData.setName( json_data.getString("fullName"));
			                onlineData.setImage( json_data.getString("image"));
			                onlineData.setPhone( json_data.getString("phone"));
			                
			                onlineDataList.add(onlineData);*/
			                
			                editor.putString("caregiverImage", json_data.getString("image"));
			                editor.putString("caregiverName", json_data.getString("fullName"));
			                editor.putString("phoneNo", json_data.getString("phone"));
			                
			           /* JSONArray caregiverArr  = reader.getJSONArray("cargiver");
			            
			            for(int i=0; i<caregiverArr.length(); i++){
			                JSONObject json_data = caregiverArr.getJSONObject(i);
	
			                editor.putString("caregiverImage", json_data.getString("image"));
			                editor.putString("caregiverName", json_data.getString("fullName"));
			                editor.putString("phoneNo", json_data.getString("phone"));
			                
			            }*/
			            
		            }
			            
		            }
		            
					editor.commit();
					
					 //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
			/*		SharedPreferences settings = getSharedPreferences("USER", MODE_PRIVATE);
					 Editor editor = settings.edit();
					
				
		             *//******* Fetch node values **********//*
		              
		             editor.putString("id", user.getString("id"));
		             editor.putString("image", user.getString("image"));
		             editor.putString("fullName",user.getString("fullName"));
		             editor.putString("email", user.getString("email"));
		             editor.putString("phone", user.getString("phone"));
		             editor.putString("latitude", user.getString("latitude"));
		             editor.putString("longitude",user.getString("longitude"));
		             editor.putString("cardNumber", user.getString("cardNumber"));
		             editor.putString("userType", user.getString("userType"));
		             editor.putString("status", user.getString("status"));
		             editor.putString("caregiver_id",user.getString("caregiver_id"));
		             editor.putString("password", user.getString("password"));
					 editor.commit();*/
					
	            }
			
			return userData;
	    }
	  
	    
	    
	    /**
	     * Method which navigates from Login Activity to Home Activity
	     */
	    public void navigatetoHomeActivity(){
	        /*Intent homeIntent = new Intent(getApplicationContext(),HomeActivity.class);
	        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(homeIntent);*/
	    }
	 
	    /**
	     * Method gets triggered when Register button is clicked
	     * 
	     * @param view
	     */
	    public void navigatetoRegisterActivity(View view){
	      /*  Intent loginIntent = new Intent(getApplicationContext(),RegisterActivity.class);
	        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(loginIntent);*/
	    }
	 
	    
	    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	
}
