package com.always.activity;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.always.adapter.CaregiverArrayAdapter;
import com.always.adapter.UserArrayAdapter;
import com.always.adapter.DataArrayAdapter1.DataViewHolder;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.Caregiver;
import com.always.model.CaregiverDetail;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.example.always.R;
import com.example.always.R.id;
import com.example.always.R.layout;


import android.text.TextWatcher;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AddCaregiver2Activity extends Activity implements OnClickListener{
	  // List view
    private ListView lv;
	Map<String, Integer> mapIndex;
    // Listview Adapter
    ArrayAdapter<String> adapter;
    private CaregiverArrayAdapter caregiverArrayAdapter;
    // Search EditText
    EditText inputSearch;
    
    TextView txtBackC;
    ImageView imgBackC;
    int textlength=0;
    String products[] = {"Amy Hicks", "Beth Hsu", "Name Here", "Name Here", "Name Here",
            "Name Here", "Name Here",
            "Name Here", "Name Here", "Name Here", "Name Here"};

    List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
    List<CaregiverDetail> searchDataList = new ArrayList<CaregiverDetail>();
    List<CaregiverDetail>  caregiverDataList=new ArrayList<CaregiverDetail>();
    
    public static final String MY_EMP_PREFS = "MySharedPref";  
    
    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_caregiver);
		
		// Listview Data

         
        lv = (ListView) findViewById(R.id.list_view_caregiver);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
         
        
      //CAll service to fill listview data
   		
			Utility utility=new Utility();
			
			SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
			String caregiver_id=  settings.getString("caregiver_id", "0");
	/*		
	    	
			String userData=utility.serviceCall(111,caregiver_id);
		
	        XMLPullParserHandler parser = new XMLPullParserHandler();
 	        onlineDataList = parser.parseJSON(userData);*/
 	       
 	       
 	       String caregiverData=utility.serviceCall(105,"1");
 			
		   XMLPullParserHandler parserCaregiver = new XMLPullParserHandler();
		   caregiverDataList = parserCaregiver.caregiverDetail(caregiverData);
 	            
 	   
		   Toast.makeText(getApplicationContext(), " Size ="+ caregiverDataList.size(), Toast.LENGTH_LONG).show();
	         
	             
		    caregiverArrayAdapter = new CaregiverArrayAdapter(getApplicationContext(), R.layout.list_row_layout_acaregiver);
		   	lv.setAdapter(caregiverArrayAdapter);
		   	
			 if (caregiverDataList.size() > 0) {
			   	    Collections.sort(caregiverDataList, new Comparator<CaregiverDetail>() {
			   	        @Override
			   	        public int compare(final CaregiverDetail object1, final CaregiverDetail object2) {
			   	        	//return Character.toUpperCase(object1.getFullName().charAt(0)) + object1.getFullName().substring(1).compareTo(Character.toUpperCase(object2.getFullName().charAt(0)) + object2.getFullName().substring(1));
			   	          return object1.getFullName().toLowerCase().compareTo(object2.getFullName().toLowerCase());
			   	        }
			   	       } );
			   	   }
		           
		   	  for (CaregiverDetail caregiverDetail:caregiverDataList)
		  		{
		   		  	 int id =caregiverDetail.getId(); 
		       	     String image = caregiverDetail.getFullName();
		             String name = Character.toUpperCase(caregiverDetail.getFullName().charAt(0)) + caregiverDetail.getFullName().substring(1);
		         	
		             String text =caregiverDetail.getFullName();
		             
		             String status =  text;
		         
		             String test =caregiverDetail.getId()+"!"+ caregiverDetail.getFullName()+"!"+caregiverDetail.getImage();
		             
		             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
		             
		             ServiceData serviceData = new ServiceData(id, imgResId,name,status,test);//,hidData);
		           
		          
		             caregiverArrayAdapter.add(serviceData);
		             
		     	   
		  		}
		       
		   	  
 	   
	      /* Toast.makeText(getApplicationContext(), " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
	         
	   	             
	    caregiverArrayAdapter = new CaregiverArrayAdapter(getApplicationContext(), R.layout.list_row_layout_acaregiver);
	   	lv.setAdapter(caregiverArrayAdapter);
	   	
		 if (onlineDataList.size() > 0) {
		   	    Collections.sort(onlineDataList, new Comparator<OnlineData>() {
		   	        @Override
		   	        public int compare(final OnlineData object1, final OnlineData object2) {
		   	            return object1.getName().compareTo(object2.getName());
		   	        }
		   	       } );
		   	   }
	           
	   	  for (OnlineData onlineData:onlineDataList)
	  		{
	       	
	       	     String image = onlineData.getName();
	             String name = Character.toUpperCase(onlineData.getName().charAt(0)) + onlineData.getName().substring(1);
	         	
	             String text =onlineData.getName();
	             
	             String status =  text;
	         
	             String test = onlineData.getStatus()+"$"+onlineData.getStatus();
	             
	             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
	             
	             ServiceData serviceData = new ServiceData(imgResId,name,status,test);//,hidData);
	           
	          
	             caregiverArrayAdapter.add(serviceData);
	             
	     	   
	  		}
	       */

	   	  
	   	  
	   	inputSearch.addTextChangedListener(new TextWatcher()
		{
					public void afterTextChanged(Editable s)
					{
					                                                                // Abstract Method of TextWatcher Interface.
					}
					public void beforeTextChanged(CharSequence s,
					int start, int count, int after)
					{
					// Abstract Method of TextWatcher Interface.
					}
					public void onTextChanged(CharSequence s,
					int start, int before, int count)
					{
							textlength = inputSearch.getText().length();
							searchDataList.clear();
							
							
							for (CaregiverDetail caregiverDetail:caregiverDataList)
					  		{
								if (textlength <= caregiverDetail.getFullName().length())
								{
										
									if(inputSearch.getText().toString().equalsIgnoreCase((String)caregiverDetail.getFullName().subSequence(0,textlength)))
									{
										searchDataList.add(caregiverDetail);
						             }
					          }
					  		}
							
							
						
							   caregiverArrayAdapter = new CaregiverArrayAdapter(getApplicationContext(), R.layout.list_row_layout_acaregiver);
							   	lv.setAdapter(caregiverArrayAdapter);
							   	
								 if (searchDataList.size() > 0) {
								   	    Collections.sort(searchDataList, new Comparator<CaregiverDetail>() {
								   	        @Override
								   	        public int compare(final CaregiverDetail object1, final CaregiverDetail object2) {
								   	            return object1.getFullName().toLowerCase().compareTo(object2.getFullName().toLowerCase());
								   	        }
								   	       } );
								   	   }
							           
								  for (CaregiverDetail caregiverDetail:searchDataList)
							  		{
							   		  	 int id =caregiverDetail.getId(); 
							       	     String image = caregiverDetail.getFullName();
							             String name = Character.toUpperCase(caregiverDetail.getFullName().charAt(0)) + caregiverDetail.getFullName().substring(1);
							         	
							             String text =caregiverDetail.getFullName();
							             
							             String status =  text;
							         
							             String test = caregiverDetail.getFullName()+"!"+caregiverDetail.getImage();
							             
							             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
							             
							             ServiceData serviceData = new ServiceData(id, imgResId,name,status,test);//,hidData);
							           
							          
							             caregiverArrayAdapter.add(serviceData);
							             
							     	   
							  		}
							       
							  			
					}
			
			
			
			});
	   	
	
	        
    	getIndexList(caregiverDataList);

		displayIndex();
		
		
       /* txtBackC=(TextView) findViewById(R.id.txtBack);
        
        txtBackC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				
				
			}
		});
        */
        imgBackC=(ImageView) findViewById(R.id.imgBack);
        
        imgBackC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				
				
			}
		});
        
       

        lv.setOnItemClickListener(new OnItemClickListener() {
     		
			  public void onItemClick(AdapterView<?> arg0,
			  View arg1, int position, long arg3)
			  {
				/*  DataViewHolder viewHolder;
				  
				  viewHolder=(DataViewHolder)arg1.getTag();*/
				  ServiceData serviceData = (ServiceData) caregiverArrayAdapter.getItem(position);
				  
				  
					/*  String name = ((TextView)arg1.findViewById(R.id.name)).getText().toString();
		          String status = ((TextView)arg1.findViewById(R.id.status)).getText().toString();
	*/
				  String name =serviceData.getName();
				  String test =serviceData.getTest();
				  
				//  Toast.makeText(getApplicationContext(), "name "+name+" test== "+test, Toast.LENGTH_LONG).show();
				  
		          Intent intent =new Intent();
				  intent.setClass(getApplicationContext(),AddConfirmActivity.class);
				  
				  intent.putExtra("name",name);
				  intent.putExtra("test",test);
				  //intent.putExtra("status",status);
				  
				  startActivity(intent);
				 				  	
			  }
        });
        
        
        
			/*   	 lv.setOnItemClickListener(new OnItemClickListener() {
			   		
					  public void onItemClick(AdapterView<?> arg0,
					  View arg1, int position, long arg3)
					  { 
						  DataViewHolder viewHolder;
						  
						  viewHolder=(DataViewHolder)arg1.getTag();
						  
						  
						  String name = ((TextView)arg1.findViewById(R.id.name)).getText().toString();
				          String test = ((TextView)arg1.findViewById(R.id.status)).getText().toString();
			
				          Intent intent =new Intent();
						  intent.setClass(getApplicationContext(),PatientDetailActivity.class);
						  
						  intent.putExtra("name",name);
						  intent.putExtra("LatLog",test);
						  
						  startActivity(intent);
					  }
			 });
			*/
			   	 
        
        
	}

	private void getIndexList(List<CaregiverDetail>  caregiverDataList) {
		
		mapIndex = new LinkedHashMap<String, Integer>();
		
		
		
          int i=0;
          
	   	  for (CaregiverDetail caregiverDetail:caregiverDataList)
	  		{
	       	    String name = caregiverDetail.getFullName();
	       	 	String index = name.substring(0, 1);
	       	 	
				//Toast.makeText(getApplicationContext(), index,Toast.LENGTH_LONG).show();
	       	 	index=index.toUpperCase();
				if (mapIndex.get(index) == null)
					mapIndex.put(index, i);
				
				i=i+1;
	  		}
	}

	
	private void displayIndex() {
		LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index);

		TextView textView;
		List<String> indexList = new ArrayList<String>(mapIndex.keySet());
		for (String index : indexList) {
			textView = (TextView) getLayoutInflater().inflate(R.layout.side_index_item, null);
			textView.setText(index);
			textView.setOnClickListener(this);
			indexLayout.addView(textView);
		}
	}
	
	
	public void onClick(View view) {
		TextView selectedIndex = (TextView) view;
		lv.setSelection(mapIndex.get(selectedIndex.getText()));
	}
	
}