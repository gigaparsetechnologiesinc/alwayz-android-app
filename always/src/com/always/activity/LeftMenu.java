package com.always.activity;

import javax.xml.datatype.Duration;

import com.example.always.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LeftMenu extends Activity {

	ImageView imgPerson;
	TextView txtManageUsers;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.left_menu);
		
		imgPerson=(ImageView)findViewById(R.id.imgPicture);
		  Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.baby);
          
		  imgPerson.setImageBitmap(icon);
		  
		  
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.left_menu, menu);
		return true;
	}

	
}
