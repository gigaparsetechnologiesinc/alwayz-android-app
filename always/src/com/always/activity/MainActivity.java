package com.always.activity;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import com.always.adapter.DataArrayAdapter1;
import com.always.adapter.DataArrayAdapter1.DataViewHolder;
import com.always.common.Utility;
import com.always.common.XMLPullParserHandler;
import com.always.model.OnlineData;
import com.always.model.ServiceData;
import com.always.model.UserData;
import com.example.always.R;

import com.navdrawer.SimpleSideDrawer;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;





import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;




public class MainActivity extends Activity {
	public  List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	SimpleSideDrawer slide_me;
	public static final String MY_EMP_PREFS = "MySharedPref";  
	    WebView web;
	
	    TextView txtUsers;
	
		private static final String TAG = "ListViewActivity";
		
		private DataArrayAdapter1 dataArrayAdapter;
		private ListView listView;
		
		private static int colorIndex;
		
		static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 
		private final static String SERVICE_URI = "http://www.gigaparse.com/clients/alwayz/API/APICloud.php";
		int height;
		int width ;
		DisplayMetrics displaymetrics;
		
	
		
		
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
        displaymetrics = new DisplayMetrics();
  		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
  		height = displaymetrics.heightPixels;
  		width = displaymetrics.widthPixels;
  		
  		setContentView(R.layout.activity_main_map);
           // setActivity();
          
       	   
      		
      	slide_me = new SimpleSideDrawer(this);
      //	runSlider();
      		
      	slide_me.setLeftBehindContentView(R.layout.left_menu);
      	
    		ImageView left_button; 
    	    left_button = (ImageView) findViewById(R.id.imgLeft);
       		
    	   	
       		left_button.setOnClickListener(new View.OnClickListener() {

       			@Override
       			public void onClick(View v) {
       				// TODO Auto-generated method stub
       				slide_me.toggleLeftDrawer();
       			
       			}
       		});
       		
        
       	//CAll service to fill listview data
		Utility utility=new Utility();
		
		SharedPreferences settings 	= getSharedPreferences(MY_EMP_PREFS, 0);
		
    	String caregiver_id=  settings.getString("caregiver_id", "0");
		String userData=utility.serviceCall(111,caregiver_id);
		
		System.out.println("caregiver_idcaregiver_idcaregiver_id"+caregiver_id);
		
		TextView txtCenter;
		
		txtCenter=(TextView)findViewById(R.id.txtCenter);
			 
		txtCenter.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Click",Toast.LENGTH_LONG).show();
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(), ManageUsersActivity.class);
				startActivity(intent);
			
			}
		});
		  
	    	 web = (WebView)findViewById(R.id.webview);
	         web.getSettings().setJavaScriptEnabled(true);
	         web.getSettings().setBuiltInZoomControls(true);

	         web.requestFocusFromTouch();

	         web.setWebViewClient(new WebViewClient());
	         web.setWebChromeClient(new WebChromeClient());  
	        
	         web.loadUrl("file:///android_asset/jsonmap.html");
	      		
   		
	         //Open MAnage Users window
   	       colorIndex = 0;
           listView = (ListView) findViewById(R.id.list1);
      	             
  	       XMLPullParserHandler parser = new XMLPullParserHandler();
      	   onlineDataList = parser.parseJSON(userData);
      	            
      	   //Toast.makeText(MainActivity.this, " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
      	         
    	             
      	    dataArrayAdapter = new DataArrayAdapter1(MainActivity.this, R.layout.listview_row_layout);
      		listView.setAdapter(dataArrayAdapter);
      		
      		int cntUser=0;
      		
      	  	  for (OnlineData onlineData:onlineDataList)
     		  {
      	  		  
      	  		int id=onlineData.getId();
          	    String image = onlineData.getName();
                String name = onlineData.getName();
                String text = "Status :" ;
	            
                cntUser=cntUser+1;
                
	             if (onlineData.getStatus().equalsIgnoreCase("online"))
	            	 text = text +"<font color='green'>"+ onlineData.getStatus()+"</font>";
	             else 
	            	 text = text +"<font color='red'>"+ onlineData.getStatus()+"</font>";
	             
	        	 text = text +"     Location : "+onlineData.getLocation();
	             String status =  text;
	             
             //   String status = "Status : "+ onlineData.getStatus()+"     Location : "+onlineData.getLocation();
                String  test =onlineData.getId()+"!"+onlineData.getLatitude()+"!"+onlineData.getLongitude();
                
                String imageListView=onlineData.getImage(); 
                
               int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
                
               ServiceData serviceData = new ServiceData(imgResId,name,status,test,imageListView);
           
               dataArrayAdapter.add(serviceData);
               
     		}
      	  	
      	  	TextView txtCircle;
      		//txtCircle=(TextView)findViewById(R.id.txtCircle);
      		
      	  	//txtCircle.setText(String.valueOf(cntUser));
      	 
        	String fullName=  settings.getString("fullName", "0");
        	TextView txtName=(TextView) findViewById(R.id.txtName);
        	TextView txtUser=(TextView) findViewById(R.id.txtUser);
        	
        	txtName.setText(fullName);
        	txtUser.setText("Caregiver for "+cntUser+" Users");
        	
        	
        	
           	 listView.setOnItemClickListener(new OnItemClickListener() {
          		
     			  public void onItemClick(AdapterView<?> arg0,
     			  View arg1, int position, long arg3)
     			  { 
     				  DataViewHolder viewHolder;
     				  
     				  viewHolder=(DataViewHolder)arg1.getTag();
     				  
     				  
     				  String name = ((TextView)arg1.findViewById(R.id.name)).getText().toString();
     		          String test = ((TextView)arg1.findViewById(R.id.test)).getText().toString();

     		          Intent intent =new Intent();
     				  intent.setClass(getApplicationContext(),PatientDetailActivity.class);
     				  
     				  intent.putExtra("name",name);
     				  intent.putExtra("LatLog",test);
     				  
     				  startActivity(intent);
     			  }
     	 });

           	setClickEvent();
    }
     
    
    private void setClickEvent()
    {
     	 
    	TextView  txtManageUsers;
       txtManageUsers=(TextView)slide_me.findViewById(R.id.txtUsers);
		txtManageUsers.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent=new Intent();
				intent.setClass(getApplicationContext(), ManageUsersActivity.class);
				startActivity(intent);
			
			}
		});
		  
		  //Open My Subscription window
		TextView  txtSub;
		txtSub=(TextView)slide_me.findViewById(R.id.txtSub);
		  
		 txtSub.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),SubscriptionActivity.class);
				startActivity(intent);
			}
		});
  		
		 
		 //open My Account window
		   TextView  txtAccount;
		  txtAccount=(TextView)slide_me.findViewById(R.id.txtAccount);
		  
		  txtAccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),MyAccountActivity.class);
				startActivity(intent);
			}
		});
		  
		  
		/*  //open 
			  TextView  txtAddCaregiver;
		  
		  txtAddCaregiver=(TextView)slide_me.findViewById(R.id.txtAddCaregiver);
		  
		  txtAddCaregiver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Click",Toast.LENGTH_SHORT).show();
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),AddCaregiverActivity.class);
				startActivity(intent);
			}
		});*/
		  
		/* for  Add Caregiver 
		  //open 
		  TextView  txtPatient;
		  
		  txtPatient=(TextView)slide_me.findViewById(R.id.txtPatient);
		  
		  txtPatient.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Click",Toast.LENGTH_SHORT).show();
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(),PatientHomeAltActivity.class);
				startActivity(intent);
			}
		});
		  */
    
		txtUsers=(TextView) findViewById(R.id.txtUsers);
     	
     	txtUsers.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent  intent=new Intent();
				intent.setClass(getApplicationContext(), ManageUsersActivity.class);
				startActivity(intent);
			}
		});
     	  
    }
    
  /*  private void setActivity()
    {
    		
    	Toast.makeText(getApplicationContext()," width="+width+ "Height = "+ height, Toast.LENGTH_LONG).show();
	
			if(width==240 && height==320)
			{
				setContentView(R.layout.activity_main_menu_240_320);
			}
			else if(width==240 && height==400)
			{
				setContentView(R.layout.activity_main_menu_240_400);
			}
			else if(width==240 && height==432)
			{
				setContentView(R.layout.activity_main_menu_240_432);
			}
			else if(width==280 && height==280)
			{
				setContentView(R.layout.activity_main_menu_280_280);
			}
			else if(width==320 && height==320)
			{
				setContentView(R.layout.activity_main_menu_320_320);
			}
			else if(width==320 && height==432)//480
			{
				setContentView(R.layout.activity_main_menu_320_480);
			}
			else if(width==320 && height==480)//480
			{
				setContentView(R.layout.activity_main_menu_320_480);
			}
			else if(width==480 && height==800)
			{
				setContentView(R.layout.activity_main_menu_480_800);
			}		
			else if(width==480 && height==854)
			{
				setContentView(R.layout.activity_main_menu_480_854);
			}		
			else if(width==720 && height==1280)
			{
				setContentView(R.layout.activity_main_menu_720_1280);
			}		
			else if(width==768 && height==1280)
			{
				setContentView(R.layout.activity_main_menu_768_1280);
			}	
			else if(width==720 && height==1184)
			{
				setContentView(R.layout.activity_main_menu_768_1280);
			}	
			else if(width==800 && height==1280)
			{
				setContentView(R.layout.activity_main_menu_800_1280);
			}		
			else if(width==1024 && height==600)
			{
				setContentView(R.layout.activity_main_menu_1024_600);
			}		
			else if(width==1080 && height==1920)
			{
				setContentView(R.layout.activity_main_menu_1080_1920);
			}		
			else if(width==1200 && height==1920)
			{
				setContentView(R.layout.activity_main_menu_1200_1920);
			}		
			else if(width==1280 && height==720)
			{
				setContentView(R.layout.activity_main_menu_1280_720);
			}		
			else if(width==1920 && height==1080)
			{
				setContentView(R.layout.activity_main_menu_280_280);
			}		
			else if(width==2560 && height==1600)
			{
				setContentView(R.layout.activity_main_menu_280_280);
			}
			else
			{
				 setContentView(R.layout.activity_main_menu_480_854);
			}

}*/
    
 /*
 public void runSlider()
    {
    	
 		if(width==240 && height==320)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_240_320);
		}
		else if(width==240 && height==400)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_240_400);
		}
		else if(width==240 && height==432)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_240_432);
		}
		else if(width==280 && height==280)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_280_280);
		}
		else if(width==320 && height==320)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_320_320);
		}
		else if(width==320 && height==480)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_320_480);
			}		
		else if(width==480 && height==800)
		{
			Toast.makeText(getApplicationContext(), "480*800  screen ",Toast.LENGTH_LONG).show();
			
			slide_me.setLeftBehindContentView(R.layout.left_menu_480_800);
		}		
		else if(width==480 && height==854)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_480_854);
		}		
		else if(width==720 && height==1280)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_720_1280);
		}		
		else if(width==768 && height==1280)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_768_1280);
		}
		else if(width==720 && height==1184)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_768_1280);
		}	
		else if(width==800 && height==1280)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_800_1280);
		}		
		else if(width==1024 && height==600)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_1024_600);
		}		
		else if(width==1080 && height==1920)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_1080_1920);
		}		
		else if(width==1200 && height==1920)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_1200_1920);
		}		
		else if(width==1280 && height==720)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_1280_720);
		}		
		else if(width==1920 && height==1080)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_1920_1080);
		}		
		else if(width==2560 && height==1600)
		{
			slide_me.setLeftBehindContentView(R.layout.left_menu_2560_1600);
		}
		else
		{   
			slide_me.setLeftBehindContentView(R.layout.left_menu_480_854);
		
		}
 
    }
      */
  
}
