package com.always.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.example.always.R;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AddCaregiverActivity extends Activity implements OnClickListener{
	  // List view
    private ListView lv;
	Map<String, Integer> mapIndex;
    // Listview Adapter
    ArrayAdapter<String> adapter;
     
    // Search EditText
    EditText inputSearch;
    
    TextView txtBackC;
    ImageView imgBackC;
    
    String products[] = {"Amy Hicks", "Beth Hsu", "Name Here", "Name Here", "Name Here",
            "Name Here", "Name Here",
            "Name Here", "Name Here", "Name Here", "Name Here"};
    
    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_caregiver);
		
		// Listview Data

         
        lv = (ListView) findViewById(R.id.list_view_caregiver);
       // inputSearch = (EditText) findViewById(R.id.inputSearch);
         
        Toast.makeText(getApplicationContext(), "List size  " , Toast.LENGTH_LONG).show();
        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.listview_row_layout_caregiver, products);

        //listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow,     	    R.id.rowTextView, planetList);  R.id.inputSearch,
        
        //lv.setAdapter(new ArrayAdapter<String> (AddCaregiverActivity.this,  R.layout.listview_row_layout_caregiver ,  R.id.inputSearch,products));

        lv.setAdapter(adapter);   
        
    	/*getIndexList(products);

		displayIndex();
		*/
		
       /* inputSearch.addTextChangedListener(new TextWatcher() {
            
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                AddCaregiverActivity.this.adapter.getFilter().filter(cs);   
            }
             
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
                 
            }
             
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub                          
            }
        });
        */
        txtBackC=(TextView) findViewById(R.id.txtBackC);
        
        txtBackC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				
				
			}
		});
        
        imgBackC=(ImageView) findViewById(R.id.imgBackC);
        
        imgBackC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				
				
			}
		});
        
       

        lv.setOnItemClickListener(new OnItemClickListener() {
     		
			  public void onItemClick(AdapterView<?> arg0,
			  View arg1, int position, long arg3)
			  {
				  	Toast.makeText(getApplicationContext(), "Click " +position , Toast.LENGTH_LONG).show();	  
				  	
				  	Intent intent=new Intent();
					intent.setClass(getApplicationContext(), AddConfirmActivity.class);
					startActivity(intent);
				  	
			  }
        });
        
        
        
	}

	
/*	private void getIndexList(String[] products) {
		mapIndex = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < products.length; i++) {
			String product = products[i];
			String index = product.substring(0, 1);

			//Toast.makeText(getApplicationContext(), index,Toast.LENGTH_LONG).show();
			
			if (mapIndex.get(index) == null)
				mapIndex.put(index, i);
		}
	}


	
	private void displayIndex() {
		LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index);

		TextView textView;
		List<String> indexList = new ArrayList<String>(mapIndex.keySet());
		for (String index : indexList) {
			textView = (TextView) getLayoutInflater().inflate(R.layout.side_index_item, null);
			textView.setText(index);
			textView.setOnClickListener(this);
			indexLayout.addView(textView);
		}
	}*/
	
	
	public void onClick(View view) {
		TextView selectedIndex = (TextView) view;
		lv.setSelection(mapIndex.get(selectedIndex.getText()));
	}
	



	/*@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}*/


	/*@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}*/
}
