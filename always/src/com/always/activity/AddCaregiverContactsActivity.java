package com.always.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Comparator;

import com.always.adapter.CaregiverArrayAdapter;
import com.always.model.CaregiverDetail;
import com.always.model.ServiceData;
import com.example.always.R;
import com.example.always.R.drawable;
import com.example.always.R.id;
import com.example.always.R.layout;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.widget.SimpleCursorAdapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class AddCaregiverContactsActivity extends Activity {
	SimpleCursorAdapter mAdapter;
	MatrixCursor mMatrixCursor;	
	EditText inputSearch;
	//private SearchView mSearchView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_caregiver_contacts);
        
        Intent i =getIntent();
        final String comeFrom = i.getStringExtra("comeFrom");
        
        // The contacts from the contacts content provider is stored in this cursor
        mMatrixCursor = new MatrixCursor(new String[] { "_id","name","photo","details"} );
        
        // Adapter to set data in the listview
        mAdapter = new SimpleCursorAdapter(getBaseContext(),
                R.layout.lv_layout,
                null,
                new String[] { "name","photo","details"},
                new int[] { R.id.tv_name,R.id.iv_photo,R.id.tv_details}, 0);
        
        // Getting reference to listview
        final ListView lstContacts = (ListView) findViewById(R.id.lst_contacts);
       // inputSearch = (EditText) findViewById(R.id.inputSearch);
        
        
        // Setting the adapter to listview
        lstContacts.setAdapter(mAdapter);        
        
        // Creating an AsyncTask object to retrieve and load listview with contacts
        ListViewContactsLoader listViewContactsLoader = new ListViewContactsLoader();
        
        // Starting the AsyncTask process to retrieve and load listview with contacts
        listViewContactsLoader.execute();        
        
        lstContacts.setOnItemClickListener(new OnItemClickListener() {
     		
		 public void onItemClick(AdapterView<?> arg0,
			  View arg1, int position, long arg3)
			  {
				  
				    TextView tv_name= (TextView) arg1.findViewById(R.id.tv_name);
				    String name =tv_name.getText().toString();
				    
			        TextView tv_details= (TextView) arg1.findViewById(R.id.tv_details);
				    String details = tv_details.getText().toString();
				    
				    ImageView iv_photo=(ImageView)arg1.findViewById(R.id.iv_photo);
				  //  String imagePath  = (String) iv_photo.getTag();
				    
				    iv_photo.buildDrawingCache();
				    Bitmap bitmap = iv_photo.getDrawingCache();

				   // Intent intent = new Intent(this, NewActivity.class);
	
				    
				    
				//    String imgUrl = contact_data.get( + i).getUrl();
				    //System.out.println("imagePathimagePath "+imagePath);
				    
				    int length=details.split("!").length;
			        
			        String phone;
			        String email = null;
			        
			        if (length==1)
			        {
			            phone=details.split("!")[0];
			            details=details+"!"+"";
			        }
			        else
			        {
			        	phone=details.split("!")[0];
			            email=details.split("!")[1];
			        }
				    

				    
				    Toast.makeText(getApplicationContext(), "name=="+name +" details "+details , Toast.LENGTH_LONG).show();
				 
				    if (comeFrom.equalsIgnoreCase("user"))
				    {
				    
  				      Intent intent =new Intent();
 					  intent.setClass(getApplicationContext(),AddConfirmActivity.class);
					  
					  intent.putExtra("name",name);
					  intent.putExtra("details",details);
					  intent.putExtra("BitmapImage", bitmap);
					  startActivity(intent);
				    }
				    else
				    {
				    	sendEmail(email);
				    }
			  }
        });
    }    
    
    
    protected void sendEmail(String toMail) {
		
  		Intent i  = new Intent(getApplicationContext(), SendEmailActivity.class);
  		i.putExtra("toMail",toMail);
  		startActivity(i);	
  	}
  	
    
    
    /** An AsyncTask class to retrieve and load listview with contacts */
    private class ListViewContactsLoader extends AsyncTask<Void, Void, Cursor>{   	

		@Override
		protected Cursor doInBackground(Void... params) {
			Uri contactsUri = ContactsContract.Contacts.CONTENT_URI;
			
			// Querying the table ContactsContract.Contacts to retrieve all the contacts
			Cursor contactsCursor = getContentResolver().query(contactsUri, null, null, null, 
									ContactsContract.Contacts.DISPLAY_NAME + " ASC ");
			
			if(contactsCursor.moveToFirst()){
				do{
					long contactId = contactsCursor.getLong(contactsCursor.getColumnIndex("_ID"));
					
					
					Uri dataUri = ContactsContract.Data.CONTENT_URI;
					
					// Querying the table ContactsContract.Data to retrieve individual items like
					// home phone, mobile phone, work email etc corresponding to each contact 
					Cursor dataCursor = getContentResolver().query(dataUri, null, 
											ContactsContract.Data.CONTACT_ID + "=" + contactId, 
											null, null);
					
					
					String displayName="";
					String nickName="";
					String homePhone="";
					String mobilePhone="";
					String workPhone="";
					String photoPath="" + R.drawable.blank;
					byte[] photoByte=null;
					String homeEmail="";
					String workEmail="";
					String companyName="";
					String title="";
					String otherEmail="";
					
					
					if(dataCursor.moveToFirst()){
						// Getting Display Name
						displayName = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME ));
						do{
												
							// Getting NickName
							if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE))
								nickName = dataCursor.getString(dataCursor.getColumnIndex("data1"));
							
							// Getting Phone numbers
							if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)){
								switch(dataCursor.getInt(dataCursor.getColumnIndex("data2"))){
									case ContactsContract.CommonDataKinds.Phone.TYPE_HOME : 
										homePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
										break;
									case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE : 
										mobilePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
										break;
									case ContactsContract.CommonDataKinds.Phone.TYPE_WORK : 
										workPhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
										break;	
								}
							}
							
							// Getting EMails
							if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE ) ) {									
								switch(dataCursor.getInt(dataCursor.getColumnIndex("data2"))){
									case ContactsContract.CommonDataKinds.Email.TYPE_HOME : 
										homeEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
										break;
									case ContactsContract.CommonDataKinds.Email.TYPE_WORK : 
										workEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
										break;		
									case ContactsContract.CommonDataKinds.Email.TYPE_OTHER : 
										otherEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
										break;		
								}
							}
							
							// Getting Organization details
							if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)){
								companyName = dataCursor.getString(dataCursor.getColumnIndex("data1"));
								title = dataCursor.getString(dataCursor.getColumnIndex("data4"));
							}
								
							// Getting Photo	
							if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)){								
								
								photoByte = dataCursor.getBlob(dataCursor.getColumnIndex("data15"));
								
								if(photoByte != null) {							
									Bitmap bitmap = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);
									
									// Getting Caching directory 
				                    File cacheDirectory = getBaseContext().getCacheDir();
	
				                    // Temporary file to store the contact image 
				                    File tmpFile = new File(cacheDirectory.getPath() + "/wpta_"+contactId+".png");
	
				                    // The FileOutputStream to the temporary file
				                    try {
										FileOutputStream fOutStream = new FileOutputStream(tmpFile);
										
										// Writing the bitmap to the temporary file as png file
					                    bitmap.compress(Bitmap.CompressFormat.PNG,100, fOutStream);
	
					                    // Flush the FileOutputStream
					                    fOutStream.flush();
	
					                    //Close the FileOutputStream
					                    fOutStream.close();
	
									} catch (Exception e) {
										e.printStackTrace();
									}
	
				                    photoPath = tmpFile.getPath();
								}
								
							}
							
						}while(dataCursor.moveToNext());					
						
						String details = "";
						
						/*// Concatenating various information to single string
						if(homePhone != null && !homePhone.equals("") )
							details = "HomePhone : " + homePhone + "\n";
						if(mobilePhone != null && !mobilePhone.equals("") )
							details += "MobilePhone : " + mobilePhone + "\n";
						if(workPhone != null && !workPhone.equals("") )
							details += "WorkPhone : " + workPhone + "\n";
						if(nickName != null && !nickName.equals("") )
							details += "NickName : " + nickName + "\n";
						if(homeEmail != null && !homeEmail.equals("") )
							details += "HomeEmail : " + homeEmail + "\n";
						if(workEmail != null && !workEmail.equals("") )
							details += "WorkEmail : " + workEmail + "\n";
						if(companyName != null && !companyName.equals("") )
							details += "CompanyName : " + companyName + "\n";
						if(title != null && !title.equals("") )
							details += "Title : " + title + "\n";*/
						
						System.out.println("photoPath "+photoPath);
						
						if(mobilePhone != null && !mobilePhone.equals("") )
							details =  mobilePhone + "!";
						else
							details =  homePhone + "!";
						
						//if(mobilePhone != null && !mobilePhone.equals("") )
							
						if(workEmail != null && !workEmail.equals("") )
							details +=  workEmail;
						else if(homeEmail != null && !homeEmail.equals("") )
							details +=  homeEmail;
						else if(otherEmail != null && !otherEmail.equals("") )
							details +=  otherEmail;
						
						
						// Adding id, display name, path to photo and other details to cursor
						mMatrixCursor.addRow(new Object[]{ Long.toString(contactId),displayName,photoPath,details});
					}
					
				}while(contactsCursor.moveToNext());
			}
			return mMatrixCursor;
		}
    	
		@Override
		protected void onPostExecute(Cursor result) {			
			// Setting the cursor containing contacts to listview
			mAdapter.swapCursor(result);
		}		
    }

    
}
