package com.example.model;

public class UserData {
	
	private int image;
	private String name;
    private String status;
    private String test;
    
    
    
    public UserData(int image, String name,String status, String test) {
		super();
        this.setImage(image);
		this.setName(name);
        this.setStatus(status);
        this.setTest(test);
      //  this.setHidData(hidData);
	}

    
	public int getImage() {
		return image;
	}
	public void setImage(int image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
    
    
}
