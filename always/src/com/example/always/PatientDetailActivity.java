package com.example.always;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.example.common.Utility;
import com.example.model.OnlineData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class PatientDetailActivity extends Activity {
	WebView web;
	public  List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
	
    private DataArrayAdapter arrayAdapter;
	private ListView listView;
	private static int colorIndex;
	
	 static final String urls = "http://api.androidhive.info/pizza/?format=xml";
	 
	 ImageView imgLeftH;
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_detail);
		
		
		Intent intent = getIntent();
        
        String latLog = intent.getStringExtra("LatLog");
        String getName = intent.getStringExtra("name");
        
        Toast.makeText(getApplicationContext(),"Name "+getName,Toast.LENGTH_LONG).show();
        
        Toast.makeText(getApplicationContext(),"LatLog "+latLog,Toast.LENGTH_LONG).show();
        
		imgLeftH=(ImageView) findViewById(R.id.imgLeftH);
		
		imgLeftH.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		 web = (WebView)findViewById(R.id.webviewH);
	     web.getSettings().setJavaScriptEnabled(true);
	     web.getSettings().setBuiltInZoomControls(true);

	     web.requestFocusFromTouch();

	     web.setWebViewClient(new WebViewClient());
	     web.setWebChromeClient(new WebChromeClient());  
	        
	     web.loadUrl("file:///android_asset/map_try.html");

	     
	     colorIndex = 0;
   		listView = (ListView) findViewById(R.id.lstHistory);
   		
   		        XMLPullParserHandler parser = new XMLPullParserHandler();
   	            
   	            String xml = null;
   	         	            
   	            xml = getXmlFromUrl(urls);
   	 
   	            
   	 		Utility utility=new Utility();
   			String userData=utility.serviceCall(108,"1");//For user detail
   			
   			
   	          /// InputStream stream = new ByteArrayInputStream(xml.getBytes());
   	        
   	          onlineDataList = parser.parseHistory(userData);
   	            
   	        //   Toast.makeText(getApplicationContext(), " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
   	         
   			/*          
   	        arrayAdapter = new DataArrayAdapter(getApplicationContext(), R.layout.listview_row_layout);
   		listView.setAdapter(arrayAdapter);
           
   	  for (OnlineData onlineData:onlineDataList)
  		{
       	
       	     String image = onlineData.getName();
             String name = onlineData.getName();
             String location = onlineData.getLocation();
             String status = onlineData.getStatus();
             String test=status+"$"+status; 
             
             //String latitude =onlineData.getLatitude();
           //  String longitude=onlineData.getLongitude();
             
             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
             
           ServiceData serviceData = new ServiceData(imgResId,name,status,test);//, latitude+"$"+longitude  );
            arrayAdapter.add(serviceData);
             
     	 //  array_sort.add(onlineData.getName());
          Toast.makeText(getApplicationContext(), " onlineData.getName() ="+ onlineData.getName(), Toast.LENGTH_LONG).show();
     	   //array_sort1.add(onlineData.getLocation());
  		}
       */
	     
	}

	


private String getXmlFromUrl(String urlString) {

Toast.makeText(getApplicationContext(), "getXMLFRomURL", Toast.LENGTH_LONG);


StringBuffer output = new StringBuffer("");
    try {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        HttpURLConnection httpConnection = (HttpURLConnection) connection;
        httpConnection.setRequestMethod("GET");
        httpConnection.connect();

        if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            stream = httpConnection.getInputStream();

            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(stream));
            String s = "";
        while ((s = buffer.readLine()) != null)
            output.append(s);
    }
     
} catch (Exception ex) {
    ex.printStackTrace();
}
    
return output.toString();
 
/* ---Using Apache DefaultHttpClient for applications targeting 
 Froyo and previous versions --- */
/*String xml = null;

    try {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        xml = EntityUtils.toString(httpEntity);

    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    } catch (ClientProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return xml;*/
}  


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
