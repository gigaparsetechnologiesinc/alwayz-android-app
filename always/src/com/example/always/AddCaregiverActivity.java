package com.example.always;

import java.util.ArrayList;
import java.util.HashMap;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AddCaregiverActivity extends Activity {
	  // List view
    private ListView lv;
     
    // Listview Adapter
    ArrayAdapter<String> adapter;
     
    // Search EditText
    EditText inputSearch;
    
    TextView txtBackC;
    ImageView imgBackC;
    
    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_caregiver);
		
		// Listview Data
        String products[] = {"Amy Hicks", "Beth Hsu", "Name Here", "Name Here", "Name Here",
                                "Name Here", "Name Here",
                                "Name Here", "Name Here", "Name Here", "Name Here"};
         
        lv = (ListView) findViewById(R.id.list_view_caregiver);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
         
        Toast.makeText(getApplicationContext(), "List size  " , Toast.LENGTH_LONG).show();
        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.listview_row_layout_caregiver, products);

        //listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow,     	    R.id.rowTextView, planetList);  R.id.inputSearch,
        
        //lv.setAdapter(new ArrayAdapter<String> (AddCaregiverActivity.this,  R.layout.listview_row_layout_caregiver ,  R.id.inputSearch,products));

        lv.setAdapter(adapter);   
        
        
        
        
        inputSearch.addTextChangedListener(new TextWatcher() {
            
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                AddCaregiverActivity.this.adapter.getFilter().filter(cs);   
            }
             
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
                 
            }
             
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub                          
            }
        });
        
        txtBackC=(TextView) findViewById(R.id.txtBackC);
        
        txtBackC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				
				
			}
		});
        
        imgBackC=(ImageView) findViewById(R.id.imgBackC);
        
        imgBackC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				
				
			}
		});
        
       

        lv.setOnItemClickListener(new OnItemClickListener() {
     		
			  public void onItemClick(AdapterView<?> arg0,
			  View arg1, int position, long arg3)
			  {
				  	Toast.makeText(getApplicationContext(), "Click " +position , Toast.LENGTH_LONG).show();	  
				  	
				  	Intent intent=new Intent();
					intent.setClass(getApplicationContext(), AddConfirmActivity.class);
					startActivity(intent);
				  	
			  }
        });
        
        
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_caregiver, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
