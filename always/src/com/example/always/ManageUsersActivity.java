package com.example.always;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.example.model.OnlineData;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ManageUsersActivity extends Activity {

	
	 private static final String TAG = "ListViewActivity";

	    private UserArrayAdapter userArrayAdapter;
		private ListView listView;

		private static int colorIndex;
		
		 static final String urls = "http://api.androidhive.info/pizza/?format=xml";
		 
		 TextView txtBack;
		 ImageView  imgBack;	 
		 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_users);
		
		txtBack=(TextView) findViewById(R.id.txtBack);
		
		txtBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});

		
		imgBack=(ImageView) findViewById(R.id.imgBack);
	
		
		imgBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				/*Intent intent=new Intent();
				intent.setClass(getApplicationContext(), MainActivity.class);
				startActivity(intent);*/
			}
		});
		
		
		
		
		 colorIndex = 0;
   		listView = (ListView) findViewById(R.id.listView);
   		
   		
   		
   		  List<OnlineData> onlineDataList = new ArrayList<OnlineData>();
   	        
   	            XMLPullParserHandler parser = new XMLPullParserHandler();
   	            
   	            String xml = null;
   	         	            
   	            xml = getXmlFromUrl(urls);
   	 
   	           InputStream stream = new ByteArrayInputStream(xml.getBytes());
   	         
   	           onlineDataList = parser.parse(stream);
   	            
   	           Toast.makeText(getApplicationContext(), " Size ="+ onlineDataList.size(), Toast.LENGTH_LONG).show();
   	         
		   	             
		   	userArrayAdapter = new UserArrayAdapter(getApplicationContext(), R.layout.listview_row_layout_musers);
		   		listView.setAdapter(userArrayAdapter);
		           
		   	  for (OnlineData onlineData:onlineDataList)
		  		{
		       	
		       	     String image = onlineData.getName();
		             String name = onlineData.getName();
		             String status = onlineData.getStatus();
		             String test = onlineData.getStatus()+"$"+onlineData.getStatus();
		             
		             int imgResId = getResources().getIdentifier(image, "drawable", "http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
		             
		           ServiceData serviceData = new ServiceData(imgResId,name,status,test);//,hidData);
		           
		          // serviceData.setTest("manageUsers");
		            userArrayAdapter.add(serviceData);
		             
		     	 //  array_sort.add(onlineData.getName());
		         // Toast.makeText(getApplicationContext(), " onlineData.getName() ="+ onlineData.getName(), Toast.LENGTH_LONG).show();
		     	   //array_sort1.add(onlineData.getLocation());
		  		}
		       
		   	  
		   	 listView.setOnItemClickListener(new OnItemClickListener() {
		     		
				  public void onItemClick(AdapterView<?> arg0,
				  View arg1, int position, long arg3)
				  {
					  //Toast.makeText(getApplicationContext(), "position ="+position,  Toast.LENGTH_SHORT).show();
				   
				   final CharSequence[] items = {
			                "Delete", "Details", "Cancel"
			        };

			      
				   AlertDialog.Builder builder3=new AlertDialog.Builder(ManageUsersActivity.this);
				   builder3.setTitle("Manage Users.").setItems(items, new DialogInterface.OnClickListener()
				   {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
					   // TODO Auto-generated method stub
					   Toast.makeText(getApplicationContext(), "U clicked "+items[which], Toast.LENGTH_LONG).show();
					   }
				   });
				   builder3.show();
				   				   
				  }
		 });
		   	  
	}
	
	

private String getXmlFromUrl(String urlString) {

Toast.makeText(getApplicationContext(), "getXMLFRomURL", Toast.LENGTH_LONG);


StringBuffer output = new StringBuffer("");
    try {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        HttpURLConnection httpConnection = (HttpURLConnection) connection;
        httpConnection.setRequestMethod("GET");
        httpConnection.connect();

        if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            stream = httpConnection.getInputStream();

            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(stream));
            String s = "";
        while ((s = buffer.readLine()) != null)
            output.append(s);
    }
     
} catch (Exception ex) {
    ex.printStackTrace();
}
    
return output.toString();
 
/* ---Using Apache DefaultHttpClient for applications targeting 
 Froyo and previous versions --- */
/*String xml = null;

    try {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        xml = EntityUtils.toString(httpEntity);

    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    } catch (ClientProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return xml;*/
}  



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_users, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
