package com.example.always;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class PatientArrayAdapter extends ArrayAdapter<ServiceData> {
	 static Context mcontext;
	    private static final String TAG = "DataArrayAdapter";
	    
		private List<ServiceData> dataList = new ArrayList<ServiceData>();

	    static class DataViewHolder {
	        ImageView image;
	        TextView name;
	       //TextView calories;
	        ImageView rightImg;
			
	    }

	    
	    
	    public PatientArrayAdapter(Context context, int textViewResourceId) {
	        super(context, textViewResourceId);
	    }

		@Override
		public void add(ServiceData object) {
			dataList.add(object);
			super.add(object);
		}

	    @Override
		public int getCount() {
			return this.dataList.size();
		}

	    @Override
		public ServiceData getItem(int index) {
			return this.dataList.get(index);
		}

	    
	    @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
	        DataViewHolder viewHolder;
	       // Activity cntx = new Activity();
	        
			if (row == null) {
				
				LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				
			
				row = inflater.inflate(R.layout.listview_row_layout, parent, false);
				
	            viewHolder = new DataViewHolder();
	            
	            
	            viewHolder.image = (ImageView) row.findViewById(R.id.image);
	            viewHolder.name = (TextView) row.findViewById(R.id.name);
	          //  viewHolder.calories = (TextView) row.findViewById(R.id.calories);
	            viewHolder.rightImg= (ImageView) row.findViewById(R.id.rightImage);
	            
	            row.setTag(viewHolder);
			} else {
				
	            viewHolder = (DataViewHolder)row.getTag();
	            
	        }
			
			ServiceData serviceData = getItem(position);
			
		//	Bitmap bMap = BitmapFactory.decodeFile("http://karanbalkar.com/wp-content/uploads/2012/09/jellybean2.jpg");
			// Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.baby);
			// viewHolder.fruitImg.setImageBitmap(bMap);
			 
			
			 
			 
			 
	    //    viewHolder.fruitImg.setImageResource(fruit.getFruitImg());
		    viewHolder.image.setImageResource(R.drawable.userpic);
		    
		    
		    viewHolder.rightImg.setImageResource(R.drawable.phone);
	        
			//viewHolder.fruitImg.setImageBitmap(getRoundedShape(decodeFile(mcontext,R.drawable.baby),200));
			//viewHolder.fruitImg.setImageBitmap(getRoundedShape(decodeFile(cntx,R.drawable.baby),200));
			
			 //im.setImageBitmap(getRoundedShape(decodeFile(cntx, listview_images[position]),200));
	        
	        viewHolder.name.setText(serviceData.getName());
	     //   viewHolder.calories.setText(fruit.getCalories());
	        
	        
			return row;
		}


			public static Bitmap decodeFile(Context context,int resId) {
							try {
								// decode image size
								mcontext=context;
								BitmapFactory.Options o = new BitmapFactory.Options();
								o.inJustDecodeBounds = true;
								BitmapFactory.decodeResource(mcontext.getResources(), resId, o);
								// Find the correct scale value. It should be the power of 2.
								final int REQUIRED_SIZE = 200;
								int width_tmp = o.outWidth, height_tmp = o.outHeight;
								int scale = 1;
								while (true)
								{
								 if (width_tmp / 2 < REQUIRED_SIZE
								 || height_tmp / 2 < REQUIRED_SIZE)
								 break;
								 width_tmp /= 2;
								 height_tmp /= 2;
								 scale++;
							}
							// decode with inSampleSize
							BitmapFactory.Options o2 = new BitmapFactory.Options();
							o2.inSampleSize = scale;
							return BitmapFactory.decodeResource(mcontext.getResources(), resId, o2);
							} catch (Exception e) {
					}
					return null;
			}



	    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width) {
	   	 // TODO Auto-generated method stub
	   	 int targetWidth = width;
	   	 int targetHeight = width;
	   	 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
	   	 targetHeight,Bitmap.Config.ARGB_8888);

	   	 Canvas canvas = new Canvas(targetBitmap);
	   	 Path path = new Path();
	   	 path.addCircle(((float) targetWidth - 1) / 2,
	   	 ((float) targetHeight - 1) / 2,
	   	 (Math.min(((float) targetWidth),
	   	 ((float) targetHeight)) / 2),
	   	 Path.Direction.CCW);
	   	 canvas.clipPath(path);
	   	 Bitmap sourceBitmap = scaleBitmapImage;
	   	 canvas.drawBitmap(sourceBitmap,
	   	 new Rect(0, 0, sourceBitmap.getWidth(),
	   	 sourceBitmap.getHeight()),
	   	 new Rect(0, 0, targetWidth,
	   	 targetHeight), null);
	   	 return targetBitmap;
	   	 }
	   	
	    

	    public Bitmap decodeToBitmap(byte[] decodedByte) {
			return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
		}
}
