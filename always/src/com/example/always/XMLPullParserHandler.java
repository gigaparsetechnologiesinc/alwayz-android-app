package com.example.always;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.example.model.OnlineData;
import com.example.model.User;
import com.example.model.UserHistory;
 

public class XMLPullParserHandler {
    List<OnlineData> onlineDataList;
    
    List<User> userList;
    
    private OnlineData onlineData;
    
    private String text;
    
    private User user;
    
    private UserHistory userHistory;
    
    public XMLPullParserHandler() {
    	onlineDataList = new ArrayList<OnlineData>();
    }
 
    public List<OnlineData> getOnlineData() {
        return onlineDataList;
    }
 
    
    public List<OnlineData> parseJSON(String  strJson)
    {
    	  String OutputData = "";
          JSONObject jsonResponse;
                
          try {
                
               /****** Creates a new JSONObject with name/value mappings from the JSON string. ********/
               jsonResponse = new JSONObject(strJson);
                
               /***** Returns the value mapped by name if it exists and is a JSONArray. ***/
               /*******  Returns null otherwise.  *******/
               JSONArray jsonMainNode = jsonResponse.optJSONArray("user");
                
               /*********** Process each JSON Node ************/

               int lengthJsonArr = jsonMainNode.length();  

               for(int i=0; i < lengthJsonArr; i++) 
               {
                   /****** Get Object for each JSON node.***********/
                   JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    
                   /******* Fetch node values **********/
                   String  fullName   = jsonChildNode.optString("fullName").toString();
                   String latitude   = jsonChildNode.optString("latitude").toString();
                   String longitude = jsonChildNode.optString("longitude").toString();
                   String status=jsonChildNode.optString("status").toString();
                   String location=jsonChildNode.optString("location").toString();
                   
                   onlineData = new OnlineData();
                   
                   String str=" \"test";
                   System.out.println("fullName="+fullName);
                   System.out.println("status="+status);
                   System.out.println("location="+location);
                		   
                   onlineData.setName(fullName.toString());
                   onlineData.setLatitude(latitude.toString());
                   onlineData.setLongitude(longitude.toString());
                   onlineData.setStatus(status.toString());
                   onlineData.setLocation(location.toString());
                   
                   
                   onlineDataList.add(onlineData);
                   
                   OutputData += "Node : \n\n     "+ fullName +" | "
                                                   + latitude +" | "
                                                   + longitude +" | "
                                                   + status +" | "
                                                   + location +" \n\n ";
                   //Log.i("JSON parse", song_name);
              }
                
               /************ Show Output on screen/activity **********/

           //    output.setText( OutputData );
                
           } catch (JSONException e) {
    
               e.printStackTrace();
           }
    
          return onlineDataList;
    
    }
    
    
    
    public List<OnlineData> parseHistory(String  strJson)
    {
    	  String OutputData = "";
          JSONObject jsonResponse;
                
          try {
                
               /****** Creates a new JSONObject with name/value mappings from the JSON string. ********/
               jsonResponse = new JSONObject(strJson);
                
               /***** Returns the value mapped by name if it exists and is a JSONArray. ***/
               /*******  Returns null otherwise.  *******/
               JSONArray jsonMainNode = jsonResponse.optJSONArray("inout");
                
               /*********** Process each JSON Node ************/
               System.out.println("jsonMainNode ==="+jsonMainNode);
               
                int lengthJParams = jsonMainNode.length();  
                
                System.out.println("lengthJParams==="+lengthJParams);
           
                String userName = jsonResponse.getString("userName");
                String image = jsonResponse.getString("image");
                String message = jsonResponse.getString("Message");
                String status= jsonResponse.getString("Status");
                
                
                System.out.println("userName===="+userName);
                System.out.println("image="+image);
                System.out.println("Message="+message);
                System.out.println("Status="+status);
                

                user = new User();
                user.setUserName(userName);
                user.setImage(image);
                user.setMessage(message);
                user.setStatus(status);

               for(int i=0; i < lengthJParams; i++) 
               {
                   
            	 /*   JSONObject jo = new JSONObject(jsonString);
            	   JSONObject joParams = jo.getJSONObject("params");
            	   String username = joParams.getString("username");*/
            	   
            	   
            	   
            	   /****** Get Object for each JSON node.***********/
                   JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    
                   /******* Fetch node values **********/
                   String  longitude   = jsonChildNode.optString("longitude").toString();
                   String latitude   = jsonChildNode.optString("latitude").toString();
                   String location = jsonChildNode.optString("location").toString();
                   String hStatus=jsonChildNode.optString("status").toString();
                   String dtime=jsonChildNode.optString("dtime").toString();
                   
                   userHistory = new UserHistory();
                   
                  
                   
                   String str=" \"test";
                   System.out.println("longitude="+longitude);
                   System.out.println("latitude="+latitude);
                   System.out.println("location="+location);
                   System.out.println("status="+status);
                   System.out.println("dtime="+dtime);
                   
                   
                   /*onlineData.setName(fullName.toString());
                   onlineData.setLatitude(latitude.toString());
                   onlineData.setLongitude(longitude.toString());
                   onlineData.setStatus(status.toString());
                   onlineData.setLocation(location.toString());
                   
                   
                   onlineDataList.add(onlineData);
                   */
                   OutputData += "Node second : \n\n     "+ dtime +" | "
                                                   + latitude +" | "
                                                   + longitude +" | "
                                                   + status +" | "
                                                   + location +" \n\n ";
                   //Log.i("JSON parse", song_name);
              }
                
               
           
               
               /************ Show Output on screen/activity **********/

           //    output.setText( OutputData );
                
           } catch (JSONException e) {
    
               e.printStackTrace();
           }
    
          return onlineDataList;
    
    }
    
    
    
    
    public List<OnlineData> parse(InputStream is) {
        
    	XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();
 
            parser.setInput(is, null);
 
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagname.equalsIgnoreCase("item")) {
                        // create a new instance of employee
                        onlineData = new OnlineData();
                    }
                    break;
 
                case XmlPullParser.TEXT:
                    text = parser.getText();
                    break;
 
                case XmlPullParser.END_TAG:
                    if (tagname.equalsIgnoreCase("item")) {
                        // add employee object to list
                    	onlineDataList.add(onlineData);
                    } else if (tagname.equalsIgnoreCase("id")) {
                    	onlineData.setId(Integer.parseInt(text));
                    } else if (tagname.equalsIgnoreCase("name")) {
                    	onlineData.setName(text);
                    } else if (tagname.equalsIgnoreCase("description")) {
                    	onlineData.setLocation(text);
                    }
                    else if (tagname.equalsIgnoreCase("cost")) {
                    	onlineData.setStatus(text);
                     //  	onlineData.setLatitude(text);
                     //  	onlineData.setLatitude(text);
                    }
                   
                    
                    break;
                    
                default:
                    break;
                    /*} else if (tagname.equalsIgnoreCase("type")) {
                    	onlineData.setType(text);
                    }*/
                    
 
               
                }
                eventType = parser.next();
            }
 
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        return onlineDataList;
    }
    
    
  
  
}