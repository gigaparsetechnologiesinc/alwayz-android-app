package com.example.always;

public class ServiceData {
   

	private static final String TAG = "Fruit";

    private int image;
	private String name;
    private String status;
    private String test;
    //private String hidData; 
  

	public ServiceData(int image, String name,String status,String test){//, String hidData) {
		super();
        this.setImage(image);
		this.setName(name);
        this.setStatus(status);
        this.setTest(test);
      //  this.setHidData(hidData);
	}

	
	  
	public String getTest() {
		return test;
	}



	public void setTest(String test) {
		this.test = test;
	}



	/*public String getHidData() {
		return hidData;
	}
	
	public void setHidData(String hidData) {
		this.hidData = hidData;
	}
*/
	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



   
}